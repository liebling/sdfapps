#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <bbhutil.h>
#include <sdf_priv.h>

/*    mysdfinfo.c   Adapted from sdfinfo.c in rnpl distro */

double dmin(int size, double *data)
{
  double dm=0.0;
  int i;
  
  if(size>0)
    for(dm=data[0],i=1;i<size;i++)
      if(data[i]<dm)
        dm=data[i];
  return dm;
}

double dmax(int size, double *data)
{
  double dm=0.0;
  int i;
  
  if(size>0)
    for(dm=data[0],i=1;i<size;i++)
      if(data[i]>dm)
        dm=data[i];
  return dm;
}

main(int argc, char **argv)
{
  int rank,csize,dsize,version,*shape=NULL;
  int i;
  double time,*data=NULL,*coords=NULL,*bbox=NULL;
  char *name=NULL,*cnames=NULL,*tag=NULL;
  char fname[256];
  gft_sdf_file_data *gp;
  
  if(argc!=2){
    printf("usage: sdfinfo file_name\n");
    exit(0);
  }
  strcpy(fname,argv[1]);
  gft_set_single(fname);
  gp=gft_open_sdf_stream(fname);
  if(gp==NULL){
    gft_set_multi();
    gft_set_single(gft_make_sdf_name(fname));
    fprintf(stderr,"gft_make_sdf_name(%s) returns '%s'\n",fname,
            gft_make_sdf_name(fname));
  }
  i=1;
  while(mid_read_sdf_stream(1,fname,&time,&version,&rank,&dsize,&csize,&name,
                          &cnames,&tag,&shape,&bbox,&coords,&data)){

    printf("%4d  ",i);
    printf("%7.3g  ",time);
    printf("%7.3g  ",(bbox[1]-bbox[0]) / (shape[0]-1) );
/*  printf("%7.3g  ",(coords[1]-coords[0]) / (shape[0]-1) ); */

/*    printf("Data set %d\n",i);                  */
/*    printf("name=<%s>\n",name);                  */
/*    printf("version=%d\n",version);                  */
/*    printf("time=%g\n",time);                  */
/*    printf("rank=%d\n",rank);                  */

      switch(rank){
        case 1:
          printf("(%d)   ",shape[0]);
          printf("(%7.3g)   ",bbox[0]);
          printf("(%7.3g)   ",bbox[1]);
          break;
        case 2:
          printf("(%d,%d)   ",shape[0],shape[1]);
          printf("(%7.3g,%7.3g)   ",bbox[0],bbox[2]);
          printf("(%7.3g,%7.3g)   ",bbox[1],bbox[3]);
          break;
        case 3:
          printf("(%4d,%4d,%4d)   ",shape[0],shape[1],shape[2]);
          printf("(%7.3g,%7.3g,%7.3g)   ",bbox[0],bbox[2],bbox[4]);
          printf("(%7.3g,%7.3g,%7.3g)   ",bbox[1],bbox[3],bbox[5]);
          break;
        default:
          break;
      }
    printf("\n");
    i++;
    if(shape)free(shape);
    if(data)free(data);
    if(coords)free(coords);
    if(bbox)free(bbox);
    if(name)free(name);
    if(cnames)free(cnames);
    if(tag)free(tag);
  }
  gft_close_all_sdf();
}
