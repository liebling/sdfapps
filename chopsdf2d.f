c     program chopsdf
      implicit none

      character*18   APPEND2, tmp_chr8, tmp2_chr8

      character*32  name,   nameout, tcnames(3)
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 100000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, trank,
     .              x0,y0,z0
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 1000,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 35 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS),
     .              xval, yval, zval, userx, usery
      real*8        r8arg
      external      r8arg
      real*8        FUZZ
      parameter   ( FUZZ = 1.0d-10)
      logical       usingcoordvalues
      logical       double_equal
      external      double_equal

      logical       first_dataset

      logical       trace,           trace2
      parameter   ( trace = .false.  )
      parameter   ( trace2= .false. )

c      ### usage statement
       if (trace) write(*,*) 'chopsdf2d: numargs = ',iargc()
       if (iargc() .lt. 3) then
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       chopsdf2d  <sdf filename> <outputvector>'
          write(*,*) '               ',
     .                         '<1 index=-1> [ <2 index=-1> '
          write(*,*) '               ',
     .                         '[<1 value=-1.0><2 value=-1.0>]'
          write(*,*) ''
          write(*,*) '  Notes: Cuts 2d sdf data into 1d slices based'
          write(*,*) '         on user input. User simply inputs a  '
          write(*,*) '         single non-negative value for either'
          write(*,*) '         the first, second, or third axis.'
          write(*,*) '         If that value,say i, is non-zero,chopsdf'
          write(*,*) '         chooses the plane defined by x=x(i)'
          write(*,*) '         where x=x(i) is defined on the *first*'
          write(*,*) '         data set in the sdf file. Hence, this '
          write(*,*) '         differs from procsdf in that the slicing'
          write(*,*) '         is based on the actual coordinate value'
          write(*,*) '         and not simply on index. Some data sets'
          write(*,*) '         may not contain data for x=x(i) and so'
          write(*,*) '         wont be included.'
          write(*,*) ''
          stop
       end if

      !
      ! Read in first data set, then for all others
      ! slice along the coordinate of the z-index chosen:
      !
      first_dataset = .true.

      xval = 0.d0
      yval = 0.d0

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
      end do

      !
      ! Read in arguments:
      !
      name   = sarg(1,'2d_Phi_3.sdf')
      cline  = sarg(2,'*-*')
      x0     = i4arg(3,-1)
      y0     = i4arg(4,-1)
      ! Seems to be a problem with default value compiling with gfortran:
      userx = -1.d0
      usery = -1.d0
      if (iargc() .gt. 4) userx  = r8arg(5,-1.0)
      if (iargc() .gt. 5) usery  = r8arg(6,-1.0)
      stride = 1

      !
      ! Setup what will be appended to file name:
      !   (e.g. chi.sdf  ---> chi_i=11.sdf )
      !
      call sload(APPEND2,' ')
      if (.not.double_equal(userx,-1.d0)) then
         if (trace) write(*,*) 'diff: ',abs(userx+1.0)
         if (trace) write(*,*) 'de: ',double_equal(userx,-1.d0)
         usingcoordvalues = .true.
         xval = userx
         call getarg(5,tmp_chr8)
         tmp2_chr8   = '_x='//tmp_chr8
         APPEND2(1:) = tmp2_chr8
      else if (.not.double_equal(usery,-1.d0)) then
      !else if (usery .ne. -1.0) then
         usingcoordvalues = .true.
         yval = usery
         call getarg(6,tmp_chr8)
         tmp2_chr8   = '_y='//tmp_chr8
         APPEND2(1:) = tmp2_chr8
      else
         usingcoordvalues = .false.
         if (x0.ge.0) then
            call getarg(3,tmp_chr8)
            tmp2_chr8   = '_i='//tmp_chr8
            APPEND2(1:) = tmp2_chr8
         end if
         if (y0.ge.0) then
            call getarg(4,tmp_chr8)
            tmp2_chr8                     = '_j='//tmp_chr8
            APPEND2(index(APPEND2,' '):)  = tmp2_chr8
         end if
      end if

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'chopsdf: Reading in file ',name
         write(*,*) 'chopsdf: Outputting times ', cline
         write(*,*) 'chopsdf: Outputting w/ : ',APPEND2
         write(*,*) 'chopsdf: x=',x0
         write(*,*) 'chopsdf: y=',y0
         write(*,*) 'chopsdf: userx=',userx
         write(*,*) 'chopsdf: usery=',usery
         write(*,*) '*******************************'
      end if

      !
      ! Setup output vector:
      !
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

      !
      ! Get rank:
      !
      if (trace2) then
          write(*,*) 'chopsdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step   = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'chopsdf: Unable to read ', name
         write(*,*) 'chopsdf: gf3_rc = ', gf3_rc
         write(*,*) 'chopsdf: Quitting....'
         goto 800
      end if
      if (rank.ne.2) then
          write(*,*) 'chopsdf2d:  Rank not equal to 2'
          write(*,*) 'chopsdf2d:  Quitting...'
          stop
      end if

      !
      ! Create output file name
      ! based on input filename and user option:
      !
      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND2
      if (trace2) write(*,*) ' outputing to: ', nameout
      if (.true.) write(*,*) ' outputing to: ', nameout

      !
      ! Step through all data sets:
      !
      step = 0
 88   do while (.true.)
         step   = step + 1
         gf3_rc = gft_read_full( name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then
            if ( ((shape(1)*shape(2)         .gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)+shape(2)         ).gt.MAXCOORDS))
     .          ) then
                write(*,*) '2d data set too big for mem. allocated'
                stop
            end if
            !
            ! Breakup "cnames" as returned by gft_read_full
            ! into constituent names (delineated by "|"):
            !
            k              = INDEX(cnames, '|')
            tcnames(1)     = cnames(:k-1)
            tcnames_len(1) = k-1
            j              = k+1
            do i = 2, rank-1
               k              = INDEX(cnames(j:), '|') + j - 1
               tcnames(i)     = cnames(j:k-1)
               tcnames_len(i) = k-j 
               j              = k+1
            end do
            tcnames(rank)     = cnames(j:)
            tcnames_len(rank) = INDEX(tcnames(rank),' ') - 1
            !
            ! With first dataset, need to initialize the
            ! explicit value of the coordinate on which
            ! we'll be slicing:
            !
            if (first_dataset.and. .not.usingcoordvalues) then
               if (y0.gt.0) then
                  j    = y0
                  yval = coords( shape(1) +            j )
                  if (trace) write(*,*) 'chopsdf: Search: yval = ',yval
                  write(*,*) 'chopsdf: Search: yval = ',yval
               else if (y0.eq.0) then
                  j    = nint(shape(2)/2.0d0)
                  yval = coords( shape(1) +            j )
                  yval = 0.d0
                  if (trace) write(*,*) 'chopsdf: Search: yval = ',yval
                  write(*,*) 'chopsdf: Search: yval = ',yval
               end if
               if (x0.gt.0) then
                  i    = x0
                  xval = coords(                       i )
                  if (trace) write(*,*) 'chopsdf: Search: xval = ',xval
                  write(*,*) 'chopsdf: Search: xval = ',xval
               else if (x0.eq.0) then
                  i    = nint(shape(1)/2.0d0)
                  xval = coords(                       i )
                  xval = 0.d0
                  if (trace) write(*,*) 'chopsdf: Search: xval = ',xval
                  write(*,*) 'chopsdf: Search: xval = ',xval
               end if
               first_dataset = .false.
            end if
            !
            ! Only output/process data sets chosen by index vector:
            !
            if ( ivindx(iv,liv,step) .gt. 0 ) then
               !
               !   Output (x = constant)  1D slice
               !
               if(((x0.ge.0).and.(y0.lt.0)).or.userx.ne.-1.0) then
                  if (trace)
     .            write(*,*) 'Making x=constant                1D slice'
                  !
                  ! Find index i of "x()" for which "x(i)" = xval
                  !
                  i = 0 
   16             i = i + 1
                  if ( i .gt. shape(1) ) then
                     if (trace) then
                        write(*,*) 'chopsdf: not using dataset: ',step
                     end if
                     goto 88
                  end if
c                 if ( coords(i) .ne. xval ) then
                  if ( abs(coords(i)-xval) .gt. FUZZ ) then
                     goto 16
                  end if
                  if (trace) then
                     write(*,*) 'chopsdf: Looking at data set: ',step
                     write(*,*) 'chopsdf: i = ',i, ' of ',shape(1)
                     write(*,*) 'chopsdf: coords(i) = ',
     *                             coords(i)
                     write(*,*) 'chopsdf: coords(k) = ',
     *                             coords(shape(1))
                  end if
                   do j = 1, shape(2)
                         tdata( j ) = 
     .                  data((j-1)*shape(1)+i)
                       tcoords( j ) = coords( shape(1) + j)
                   end do
                   trank      = 1
                   tshape(1)  = shape(2)
                   tnames     = tcnames(2)(:tcnames_len(2))
                   gf3_rc = gft_out_full(nameout, time,tshape,tnames,
     .                                     trank, tcoords, tdata)
                   if (trace2) then
		                if (gf3_rc .eq. 1) then
                         write(*,*) "         successful writing"
		                else
                         write(*,*) "         unsuccessful writing",
     .                                " file: ",nameout
                      end if
                   end if
               !
               !   Output (y = constant)                   1D slice
               !
               else if(((x0.lt.0).and.(y0.ge.0)).or.usery.ne.-1.0) then
               !else if ( (x0.lt.0).and.(y0.ge.0)               ) then
                  if (trace)
     .            write(*,*) 'Making y=constant                1D slice'
                  !
                  ! Find index j of "y()" for which "y(j)" = yval
                  !
                  j = 0 
   18             j = j + 1
                  if ( j .gt. shape(2) ) then
                     if (trace) then
                        write(*,*) 'chopsdf: not using dataset: ',step
                     end if
                     goto 88
                  end if
                  if ( abs(coords(shape(1)+j)-yval) .gt. FUZZ ) then
                     goto 18
                  end if
                  if (trace) then
                     write(*,*) 'chopsdf: Looking at data set: ',step
                     write(*,*) 'chopsdf: j = ',j, ' of ',shape(2)
                     write(*,*) 'chopsdf: coords(j) = ',
     *                             coords(shape(1)+j)
                  end if
                   do i = 1, shape(1)
                       tdata( i ) = 
     .                  data((j-1)*shape(1)+i)
                       tcoords( i ) = coords( i )
                   end do
                   trank      = 1
                   tshape(1)  = shape(1)
                   tnames     = tcnames(1)(:tcnames_len(1))
                   gf3_rc = gft_out_full(nameout, time,tshape,tnames,
     .                                     trank, tcoords, tdata)
                   if (trace2) then
		                if (gf3_rc .eq. 1) then
                         write(*,*) "         successful writing"
		                else
                         write(*,*) "         unsuccessful writing",
     .                                " file: ",nameout
                      end if
                   end if
               else
                  write(*,*) 'chopsdf: Invalid input options'
                  stop
               end if
            else
              if (trace2) write(*,*) 'procsdf: not in index vector',step
            end if
 	      else 
             if (trace2) write(*,*) "procsdf: end of file ", name
             goto 800
         end if       ! end if gf_rc>0
      end do
		
 800  continue
      end





