c     program reshape
      implicit none

      character*18   APPEND2, tmp_chr8, tmp2_chr8

      character*32  name,   nameout, tcnames(3)
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 10000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, trank,
     .              x0,y0,z0, xf,yf,zf, ax,ay,az
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 5000000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS),
     .              xval, yval, zval

      logical       first_dataset

      logical       trace,           trace2
      parameter   ( trace = .false.  )
      parameter   ( trace2= .false. )

c      ### usage statement
       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       reshape  <sdf filename> <outputvector>'
          write(*,*) '               ',
     .                         '[<1 index=-1><2 index=-1><3 index=-1> ',
     .                         '[<1 index=-1><2 index=-1><3 index=-1>]'
          write(*,*) ''
          write(*,*) '  Notes: Extracts data from SDF and puts into '
          write(*,*) '         a re-shaped output. Only for 3d so far'
          write(*,*) ''
          stop
       end if

      !
      ! Read in first data set, then for all others
      ! slice along the coordinate of the z-index chosen:
      !
      first_dataset = .true.

      xval = 0.d0
      yval = 0.d0
      zval = 0.d0

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
      end do

      !
      ! Read in arguments:
      !
      name   = sarg(1,'2d_Phi_3.sdf')
      cline  = sarg(2,'*-*')
      x0     = i4arg(3,-1)
      y0     = i4arg(4,-1)
      z0     = i4arg(5,-1)
      xf     = i4arg(6,-1)
      yf     = i4arg(7,-1)
      zf     = i4arg(8,-1)
      stride = 1

      !
      ! Setup what will be appended to file name:
      !   (e.g. chi.sdf  ---> chi_i=11.sdf )
      !
      call sload(APPEND2,' ')
      if (x0.ge.0) then
         call getarg(3,tmp_chr8)
         tmp2_chr8   = '_is='//tmp_chr8
         APPEND2(1:) = tmp2_chr8
      end if
      if (y0.ge.0) then
         call getarg(4,tmp_chr8)
         tmp2_chr8                     = '_js='//tmp_chr8
         APPEND2(index(APPEND2,' '):)  = tmp2_chr8
      end if
      if (z0.ge.0) then
         call getarg(5,tmp_chr8)
         tmp2_chr8                     = '_ks='//tmp_chr8
         APPEND2(index(APPEND2,' '):)  = tmp2_chr8
      end if
      if (xf.ge.0) then
         call getarg(6,tmp_chr8)
         tmp2_chr8                     = '_if='//tmp_chr8
         APPEND2(index(APPEND2,' '):)  = tmp2_chr8
      end if
      if (yf.ge.0) then
         call getarg(7,tmp_chr8)
         tmp2_chr8                     = '_jf='//tmp_chr8
         APPEND2(index(APPEND2,' '):)  = tmp2_chr8
      end if
      if (zf.ge.0) then
         call getarg(8,tmp_chr8)
         tmp2_chr8                     = '_kf='//tmp_chr8
         APPEND2(index(APPEND2,' '):)  = tmp2_chr8
      end if

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'chopsdf: Reading in file ',name
         write(*,*) 'chopsdf: Outputting times ', cline
         write(*,*) 'chopsdf: Outputting w/ : ',APPEND2
         write(*,*) 'chopsdf: x=',x0
         write(*,*) 'chopsdf: y=',y0
         write(*,*) 'chopsdf: z=',z0
         write(*,*) '*******************************'
      end if

      !
      ! Setup output vector:
      !
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

      !
      ! Get rank:
      !
      if (trace2) then
          write(*,*) 'chopsdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step   = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'chopsdf: Unable to read ', name
         write(*,*) 'chopsdf: gf3_rc = ', gf3_rc
         write(*,*) 'chopsdf: Quitting....'
         goto 800
      end if
      if (rank.ne.3) then
          write(*,*) 'chopsdf:  Rank not equal to 3'
          write(*,*) 'chopsdf:  Quitting...'
          stop
      end if

      !
      ! Create output file name
      ! based on input filename and user option:
      !
      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND2
      if (trace2) write(*,*) ' outputing to: ', nameout
      if (.true.) write(*,*) ' outputing to: ', nameout

      !
      ! Step through all data sets:
      !
      step = 0
 88   do while (.true.)
         step   = step + 1
         gf3_rc = gft_read_full( name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then
            if ( ((shape(1)*shape(2)*shape(3).gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)+shape(2)+shape(3)).gt.MAXCOORDS))
     .          ) then
                write(*,*) '3d data set too big for mem. allocated'
                stop
            end if
            !
            ! Only output/process data sets chosen by index vector:
            !
            if ( ivindx(iv,liv,step) .gt. 0 ) then
                   if (x0.lt.0) then
                      ax = 1
                   else if (x0.le.shape(1)) then
                      ax = x0
                   else 
                      write(*,*) 'x0 greater than shape(1):',x0,shape(1)
                      goto 88
                   end if
                   if (xf.lt.0) then
                      tshape(1) = shape(1) - ax + 1
                   else if (xf.le.shape(1)) then
                      tshape(1) = xf - ax + 1
                   else
                      write(*,*) 'xf greater than shape(1):',xf,shape(1)
                      goto 88
                   end if
                   !
                   if (y0.lt.0) then
                      ay = 1
                   else if (y0.le.shape(2)) then
                      ay = y0
                   else 
                      write(*,*) 'y0 greater than shape(2):',y0,shape(2)
                      goto 88
                   end if
                   if (yf.lt.0) then
                      tshape(2) = shape(2) - ay + 1
                   else if (yf.le.shape(2)) then
                      tshape(2) = yf - ay + 1
                   else
                      write(*,*) 'yf greater than shape(2):',yf,shape(2)
                      goto 88
                   end if
                   !
                   if (z0.lt.0) then
                      az = 1
                   else if (z0.le.shape(3)) then
                      az = z0
                   else 
                      write(*,*) 'z0 greater than shape(3):',z0,shape(3)
                      goto 88
                   end if
                   if (zf.lt.0) then
                      tshape(3) = shape(3) - az + 1
                   else if (zf.le.shape(3)) then
                      tshape(3) = zf - az + 1
                   else
                      write(*,*) 'zf greater than shape(3):',zf,shape(3)
                      goto 88
                   end if

                   call copyreshaped(  tdata,   data,
     .                                 tcoords, coords,
     .                                 ax,ay,az,
     .                                 tshape(1), tshape(2), tshape(3),
     .                                  shape(1),  shape(2),  shape(3))
                   gf3_rc = gft_out_full(nameout, time,tshape,cnames,
     .                                     rank, tcoords, tdata)
            else
              if (trace2) write(*,*) 'reshape: not in index vector',step
            end if
 	      else 
             if (trace2) write(*,*) "reshape: end of file ", name
             goto 800
         end if       ! end if gf_rc>0
      end do
		
 800  continue
      end

      subroutine copyreshaped(  fieldout,   fieldin,
     *                          coordsout,  coordsin,
     *                          ax,   ay,   az,
     *                          nxout,nyout,nzout,
     *                          nxin, nyin, nzin)
      implicit none
      integer  nxout,nyout,nzout, nxin, nyin, nzin,
     *         ax,ay,az
      real*8   fieldout(nxout,nyout,nzout),
     *         fieldin(nxin,nyin,nzin),
     *         coordsout(nxout+nyout+nzout),
     *         coordsin(nxin+nyin+nzin)
      integer  i,j,k

      do k = 1, nzout
         coordsout(nxout+nyout+k) = coordsin(nxin+nyin+az-1+k)
      end do
      do j = 1, nyout
         coordsout(nxout      +j) = coordsin(nxin     +ay-1+j)
      end do
      do i = 1, nxout
         coordsout(            i) = coordsin(          ax-1+i)
      end do

      do k = 1, nzout
      do j = 1, nyout
      do i = 1, nxout
         fieldout(i,j,k) = fieldin(ax-1+i,ay-1+j,az-1+k)
      end do
      end do
      end do

      return
      end



