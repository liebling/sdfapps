c     program logsdf
      implicit none

c     ## name of output file
      character*2   APPEND
		parameter   ( APPEND = '_l')

      character*32  name,   nameout, tcnames(3)
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 50000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,gft_read_shape,
     .              iv(mxliv), liv, key, x,y,z, trank,vsrc,vsxynt,
     .              x0,y0,z0, jj,ii,kk
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 25 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS)

      real*8        l2norm, maxdata, mindata,temp, 
     .                      maxdata_rho, maxdata_z,
     .                      mindata_rho, mindata_z
      logical       trace,           trace2
      parameter   ( trace = .false. )
c     parameter   ( trace = .true.  )
      parameter   ( trace2= .false. )
c     parameter   ( trace2= .true. )

      ! Initiailize:
      shape(1) = 1
      shape(2) = 1
      shape(3) = 1

c      ### usage statement
       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* logsdf:     Simply take log10 of the field.   *'
          write(*,*) '*************************************************'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       logsdf  <sdf filename> <outputvector>'
          write(*,*) ''
          write(*,*) ''
          stop
       end if

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
      end do

c      ### read in arguments
      name   = sarg(1,'2d_Phi_3.sdf')
      cline  = sarg(2,'*-*')
      x0     = i4arg(3,-1)
      y0     = i4arg(4,-1)
      z0     = i4arg(5,-1)
      stride = 1

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'logsdf: Reading in file ',name
         write(*,*) 'logsdf: Outputting times ', cline  
         write(*,*) 'logsdf: x=',x0
         write(*,*) 'logsdf: y=',y0
         write(*,*) 'logsdf: z=',z0
         write(*,*) '*******************************'
      end if
c      ### set up output vector
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
c         write(*,*) 'symmetrize: using *-* as index vector'
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

c     if (trace2) call ivdump(iv,liv,'index vector',6)


c     ### get rank
      if (trace2) then
          write(*,*) 'logsdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'logsdf: Unable to read ', name
         write(*,*) 'logsdf: gf3_rc = ', gf3_rc
         write(*,*) 'logsdf: Quitting....'
         goto 800
      end if
      if (trace2) then
          write(*,*) 'logsdf:  Finished reading in sdf file...'
          write(*,*) 'logsdf:  rank = ', rank
      end if

      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      if (trace2) write(*,*) '    junk = ', junk
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND
      if (trace2) write(*,*) ' outputing to: ', nameout

      step = 0
      do while (.true.)
         step = step + 1
 		   if (trace2) then
            write(*,*) '..........step ', step
            write(*,*) '..........liv  ', liv 
            write(*,*) '..........iv(1)', iv(1) 
            if ( ivindx(iv,liv,step) .gt. 0 ) 
     .               write(*,*) '          step in index vector'
            write(*,*) '       reading in sdf'
         end if
         gf3_rc = gft_read_shape( name,     step,   shape)
         call checkmem(shape(1),shape(2),shape(3),MAXPOINTS,MAXCOORDS)
         gf3_rc = gft_read_full(  name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then

            k              = INDEX(cnames, '|')
            tcnames(1)     = cnames(:k-1)
            tcnames_len(1) = k-1
            j          = k+1
            do i = 2, rank-1
               k              = INDEX(cnames(j:), '|') + j - 1
               tcnames(i)     = cnames(j:k-1)
               tcnames_len(i) = k-j 
               j              = k+1
            end do
            tcnames(rank)     = cnames(j:)
            tcnames_len(rank) = INDEX(tcnames(rank),' ') - 1

            if (trace2) then
               do i = 1, rank
                  write(*,*) tcnames(i), tcnames_len(i),i
               end do
            end if

            if (trace2) then
               write(*,*) '..........successfully read'
               write(*,*) '..........liv  ', liv 
               write(*,*) '..........iv(1)', iv(1) 
               write(*,*) '           step:',step
               write(*,*) '           time:',time
               write(*,*) '           rank:',rank
               write(*,*) '           shape    coord name:'
               do i = 1, rank
				       write(*,*) '                 ',shape(i),
     .                  '   ',tcnames(i)
               end do
            end if
            if ( ivindx(iv,liv,step) .gt. 0 ) then

		          if (trace2) 
     .               write(*,*) '..........step in index vector'
c               ################################
c               ###  2 Dimensional sdf files ###
c               ################################
                if (rank.eq.2) then
                   do j = 1, shape(2)
                      jj = shape(2) - j + 1
                      do i = 1, shape(1)
                         ! assume domain is symmetric about 0
                         ! (won't work for distributed data sets)
                         ii = shape(1) - i + 1
                         tdata((j-1)*shape(1) + i) =
     .                           log10(data(( j-1)*shape(1)+i ))
                      end do
                   end do
                   gf3_rc = gft_out_full(nameout, time, shape, cnames, 
     .                                     rank, coords, tdata)
                      if (trace) then
		                   if (gf3_rc .eq. 1) then
                            write(*,*) "logsdf: successful"
		                   else
                           write(*,*) "logsdf: unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if

c               ################################
c               ###  3 Dimensional sdf files ###
c               ################################
                else if (rank.eq.3) then
                   do k = 1, shape(3)
                   kk = shape(3) - k + 1
                   do j = 1, shape(2)
                      jj = shape(2) - j + 1
                      do i = 1, shape(1)
                        ! assume domain is symmetric about 0
                        ! (won't work for distributed data sets)
                        ii = shape(1) - i + 1
                        tdata((k-1)*shape(2)*shape(1)+(j-1)*shape(1)+i)=
     .          log10(data((k -1)*shape(2)*shape(1)+( j-1)*shape(1)+i ))
                      end do
                   end do
                   end do
                   gf3_rc = gft_out_full(nameout, time, shape, cnames, 
     .                                     rank, coords, tdata)
                      if (trace) then
                         if (gf3_rc .eq. 1) then
                            write(*,*) "logsdf: successful"
                         else
                           write(*,*) "logsdf: unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c               ################################
c               ###  1 Dimensional sdf files ###
c               ################################
                else if (rank.eq.1) then
                      do i = 1, shape(1)
                        ! assume domain is symmetric about 0
                        ! (won't work for distributed data sets)
                        ii = shape(1) - i + 1
                        tdata(                                       i)=
     .          log10(data(                                         i ))
                      end do
                   gf3_rc = gft_out_full(nameout, time, shape, cnames, 
     .                                     rank, coords, tdata)
                      if (trace) then
                         if (gf3_rc .eq. 1) then
                            write(*,*) "logsdf: successful"
                         else
                           write(*,*) "logsdf: unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
                else
                    write(*,*) 'logsdf: cannot handle rank',rank
                end if  
            else
          if (trace) write(*,*) 'logsdf: not in index vector',step
            end if
         else 
             if (trace2) 
     .           write(*,*) "logsdf: end of file ",
     .                       name
             goto 800
         end if
      end do

 800  continue
      end





