.IGNORE:

SHELL = /bin/sh

.IGNORE =
.SUFFIXES : .F90 .f90 .F .f


#LOCALIB = -L/u/ac/liebling/local/lib

#
#   Need to set:
#           LSV        =  -lsv      (if installed)
#           F77        =  ifc, f77, pgf77
#           F77FLAGS   =  -O3
#           LOCALIB    =  -L/usr/local/lib
#
#

# For debugging:
#F77FLAGS  = -g -80 -cm -w90 -w95 -Vaxlib  -mcmodel=medium -shared-intel -i_dynamic -r8 
#LOCALIB   = -L/usr/local/inteldebug/lib -L/usr/local/intel/lib


#F77_LOAD  = $(F77)    $(F77FLAGS) $(LOCALIB)
F77_LOAD  = $(F77)    $(F77FLAGS) $(LOCALIB)  -mcmodel=medium
F77_LOADM = $(MPIF77) $(F77FLAGS) $(LOCALIB) 
F90_LOAD  = $(F90)    $(LOCALIB) $(F90_FIXEDFORM_FLAG)




#F77_      = $(F77) $(F77FLAGS) $(LOCALINC)
F77_      = $(F77) $(F77FLAGS) $(LOCALINC) -mcmodel=medium
#F77_      = $(F77) $(F77FLAGS) -I/opt/intel/mkl/$(MKL_VER)/include/fftw
#F77_      = $(F77) $(F77FLAGS)

CC_       = $(CC) $(CCFLAGS) $(LOCALINC)
CC_LOAD   = $(CC) $(CCFLAGS) $(LOCALIB)

#
# For Intel 7:
#
FLIBS = -lbbhutil -lvs -lutilio -lvutil -lv2util -lv3util  $(LSV) -lm
# For Intel8:
#LIBS      =    -lbbhutil  $(LSV) -lsvml -lm -lv3util -lv2util -lvutil
#LIBS      =    -lvutil  -lv2util  -lv3util  -lbbhutil  $(LSV) -lsvml -lm
# For Intel7:
#LIBS      =    -lvutil  -lv2util  -lv3util  -lbbhutil  $(LSV) -lCEPCF90 -lF90 -lintrins -lsvml -lm
#
# For Intel 8:
#
#LIBS      =  -L/opt/intel_fce_80/lib  -lvutil  -lv2util  -lv3util  -lbbhutil  $(LSV) -lm -limf -lifcore -ldl
#FLIBS = -lbbhutil -lvs -lutilio -lvutil -lv2util -lv3util $(LSV) -lm -limf -lifcore -ldl -lifcore -lifport -lirc -lompstub -lunwind -lcprts -lifcoremt -lguide -lcxaguard -lcxa -ldl -lpthread


.c.o:
	$(CC_) -c $*.c

.f.o:
	$(F77_) -c $*.f

.F90.o:
	$(F90)  -c $*.F90

.f90.o:
	$(F90) -c $*.f90


EXECUTABLES = cleansdf procsdf copytsdf intsdf sumsdf intsdfrunavg intrhosdf intspheresdf axispin sdfdump csdf chopsdf chopsdf2d interpcircle sdfgnudump ascii2sdf mergesdf joinsdf testsdf mysdfinfo reshape mycvtestsdf sphereharm testsdfmpi symmetrize logsdf logxsdf antisymmetrize subtractddsdf deriv fallback fftpower uniform deriv1d integrate1d triggen seg2sdf tractopdb

all: $(EXECUTABLES)

fix: Makefile
	sed "s@prochdf@prochdf@g" < Makefile > .Makefile 
	mv .Makefile Makefile

test: test.o 
	$(F77_LOAD) test.o $(FLIBS) -o test

ascii2sdf: ascii2sdf.o 
	$(F77_LOAD) ascii2sdf.o $(FLIBS) -o ascii2sdf

intrhosdf: intrhosdf.o 
	$(F77_LOAD) intrhosdf.o $(FLIBS) -o intrhosdf

intspheresdf: intspheresdf.o util.o
	$(F77_LOAD) intspheresdf.o util.o $(FLIBS) -o intspheresdf

sumsdf: sumsdf.o 
	$(F77_LOAD) sumsdf.o util.o $(FLIBS) -o sumsdf

intsdf: intsdf.o 
	$(F77_LOAD) intsdf.o util.o $(FLIBS) -o intsdf

intsdfrunavg: intsdfrunavg.o 
	$(F77_LOAD) intsdfrunavg.o util.o $(FLIBS) -o intsdfrunavg

monosdf: monosdf.o 
	$(F77_LOAD) monosdf.o $(FLIBS) -o monosdf

sdfdump: sdfdump.o 
	$(F77_LOAD) sdfdump.o $(FLIBS) -o sdfdump

sdfgnudump: sdfgnudump.o 
	$(F77_LOAD) sdfgnudump.o $(FLIBS) -o sdfgnudump

csdf: csdf.o 
	$(F77_LOAD) csdf.o $(FLIBS) -o csdf

copytsdf: copytsdf.o util.o
	$(F77_LOAD) copytsdf.o util.o $(FLIBS) -o copytsdf

cleansdf: cleansdf.o util.o
	$(F77_LOAD) cleansdf.o util.o $(FLIBS) -o cleansdf

procsdf: procsdf.o util.o
	$(F77_LOAD) procsdf.o util.o $(FLIBS) -o procsdf

symmetrize: symmetrize.o util.o
	$(F77_LOAD) symmetrize.o util.o $(FLIBS) -o symmetrize

logsdf: logsdf.o util.o
	$(F77_LOAD) logsdf.o util.o $(FLIBS) -o logsdf

logxsdf: logxsdf.o util.o
	$(F77_LOAD) logxsdf.o util.o $(FLIBS) -o logxsdf

antisymmetrize: antisymmetrize.o util.o
	$(F77_LOAD) antisymmetrize.o util.o $(FLIBS) -o antisymmetrize

chopsdf: chopsdf.o 
	$(F77_LOAD) chopsdf.o $(FLIBS) -o chopsdf

chopsdf2d: chopsdf2d.o 
	$(F77_LOAD) chopsdf2d.o util.o $(FLIBS) -o chopsdf2d

fallback: fallback.o util.o
	$(F77_LOAD) fallback.o util.o $(FLIBS) -o fallback
j: j.o util.o
	$(F77_LOAD) j.o util.o $(FLIBS) -o j

interpcircle: interpcircle.o util.o
	$(F77_LOAD) interpcircle.o util.o $(FLIBS) -o interpcircle

deriv: deriv.o util.o
	$(F77_LOAD) deriv.o util.o $(FLIBS) -o deriv

axispin: axispin.o 
	$(F77_LOAD) axispin.o $(FLIBS) -o axispin

mergesdf: mergesdf.o  util.o
	$(F77_LOAD) mergesdf.o util.o $(FLIBS) -o mergesdf

subtractddsdf: subtractddsdf.o  util.o
	$(F77_LOAD) subtractddsdf.o util.o $(FLIBS) -o subtractddsdf

joinsdf: joinsdf.o  util.o
	$(F77_LOAD) joinsdf.o util.o $(FLIBS) -o joinsdf

testsdf: testsdf.o  util.o
	$(F77_LOAD) testsdf.o util.o $(FLIBS) -o testsdf

testsdfmpi: testsdfmpi.o  util.o
	$(F77_LOADM) testsdfmpi.o util.o $(FLIBS) -o testsdfmpi

mysdfinfo: mysdfinfo.o
	$(CC_LOAD) mysdfinfo.o $(FLIBS) -o mysdfinfo

reshape: reshape.o
	$(F77_LOAD) reshape.o $(FLIBS) -o reshape

mycvtestsdf: mycvtestsdf.o
	$(F77_LOAD) mycvtestsdf.o $(FLIBS) -lm -o mycvtestsdf
	#$(F77_LOAD) mycvtestsdf.o $(FLIBS) -o mycvtestsdf -nofor_main
	#$(CC_LOAD) mycvtestsdf.o $(FLIBS) -lm -o mycvtestsdf
	#$(CC_LOAD) mycvtestsdf.o $(FLIBS) -L/opt/intel/fce/9.1.036/lib -limf -lifcore -o mycvtestsdf

sphereharm: sphereharm.o
	$(F77_LOAD) sphereharm.o $(FLIBS) -o sphereharm

seg2sdf: seg2sdf.o
	$(F77_LOAD) seg2sdf.o $(FLIBS) -o seg2sdf

hdftosdf: hdftosdf.f90
	$(F90) -I/usr/local/hdf5/include -c hdftosdf.f90
	$(F90_LOAD) hdftosdf.o $(FLIBS) -L/usr/local/hdf5/lib -lhdf5_fortran -o hdftosdf
#
# Using Intel's included FFTW libraries
#          (first line is for gfortran, second line for intel)
#
fftpower: fftpower.o
	$(F77_LOAD) -I/usr/include fftpower.o $(FLIBS) -L/usr/lib/x86_64-linux-gnu -lfftw3 -o fftpower
	#$(F77_LOAD) -mkl fftpower.o $(FLIBS) -lfftw3 -o fftpower

uniform: uniform.o
	$(F77_LOAD) uniform.o $(FLIBS) -o uniform
deriv1d: deriv1d.o
	$(F77_LOAD) deriv1d.o $(FLIBS) -o deriv1d
integrate1d: integrate1d.o
	$(F77_LOAD) integrate1d.o $(FLIBS) -o integrate1d
triggen: triggen.o
	$(F77_LOAD) triggen.o $(FLIBS) -o triggen

tractopdb: tractopdb.o util.o
	$(F77_LOAD) tractopdb.o util.o $(FLIBS) -lsilo -o tractopdb



install: $(EXECUTABLES)
	if test ! -d $(HOME)/bin ; then\
		mkdir $(HOME)/bin ;\
   fi
	/bin/cp procsdf     $(HOME)/bin
	/bin/cp cleansdf    $(HOME)/bin
	/bin/cp intsdf      $(HOME)/bin
	/bin/cp intsdfrunavg $(HOME)/bin
	/bin/cp intrhosdf   $(HOME)/bin
	/bin/cp intspheresdf   $(HOME)/bin
	/bin/cp axispin     $(HOME)/bin
	/bin/cp sdfdump     $(HOME)/bin
	/bin/cp sdfgnudump  $(HOME)/bin
	/bin/cp chopsdf     $(HOME)/bin
	/bin/cp chopsdf2d   $(HOME)/bin
	/bin/cp interpcircle   $(HOME)/bin
	/bin/cp deriv          $(HOME)/bin
	/bin/cp ascii2sdf   $(HOME)/bin
	/bin/cp mergesdf    $(HOME)/bin
	/bin/cp subtractddsdf    $(HOME)/bin
	/bin/cp joinsdf     $(HOME)/bin
	/bin/cp mysdfinfo   $(HOME)/bin
	/bin/cp reshape     $(HOME)/bin
	/bin/cp mycvtestsdf $(HOME)/bin
	/bin/cp sphereharm  $(HOME)/bin
	/bin/cp symmetrize  $(HOME)/bin
	/bin/cp logsdf   $(HOME)/bin
	/bin/cp logxsdf  $(HOME)/bin
	/bin/cp antisymmetrize  $(HOME)/bin
	/bin/cp fftpower        $(HOME)/bin
	/bin/cp uniform         $(HOME)/bin
	/bin/cp deriv1d         $(HOME)/bin
	/bin/cp triggen         $(HOME)/bin
	/bin/cp seg2sdf         $(HOME)/bin
	/bin/cp copytsdf        $(HOME)/bin
	/bin/cp tractopdb       $(HOME)/bin

export: clean
	(cd ..; tar cvzf SDFapps.tar.gz --exclude CVS SDFapps)
	scp ../SDFapps.tar.gz relativity.liu.edu:~/public_html/export

clean: 
	 rm ifc?????? *.sdf .rn*  > /dev/null 2>&1
	 rm testsdf testsdfmpi cleansdf procsdf intsdf intsdfrunavg joinsdf mergesdf intrhosdf intspheresdf axispin sdfdump csdf chopsdf chopsdf2d deriv interpcircle sdfgnudump ascii2sdf reshape sphereharm subtractddsdf fallback antisymmetrize mysdfinfo mycvtestsdf symmetrize deriv1d triggen logsdf logxsdf copytsdf tractopdb
	 rm work.pc work.pcl *.o > /dev/null 2>&1
