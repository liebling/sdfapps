c     program procsdf
      implicit none

c     ## name of output file
      character*2   APPEND
		parameter   ( APPEND = '_p')

      character*32  name,   nameout, tcnames(3), nameout2
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 500 000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,gft_read_shape,
     .              iv(mxliv), liv, key, x,y,z, trank,vsrc,vsxynt,
     .              x0,y0,z0
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 50002,  MAXPOINTS = 654 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS)
      real*8        tdata2(MAXPOINTS)
      real*8        leftfdiff,  rightfdiff
      real*8        frontfdiff, backfdiff
      real*8        topfdiff,   bottomfdiff
      ! Threshold for when a point is replaced by an average:
      real*8        THRESH
      parameter   ( THRESH = 0.5d0)
      integer       numreplaced

      real*8        l2norm, maxdata, mindata,temp, 
     .                      maxdata_rho, maxdata_z,
     .                      mindata_rho, mindata_z
      logical       trace,           trace2, ltrace
      parameter   ( trace = .true. )
c     parameter   ( trace = .true.  )
      parameter   ( trace2= .false. )
c     parameter   ( trace2= .true. )

c      ### usage statement
       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* Identifies points that standout from their    *'
          write(*,*) '* environments, and then smoothes them out.     *'
          write(*,*) '*************************************************'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       cleansdf  <sdf filename> <outputvector>'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Output:                                       *'
          write(*,*) '*************************************************'
          write(*,*) '       sdf file <sdf filename>_p.sdf'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Sample usage:                                 *'
          write(*,*) '*************************************************'
          write(*,*) ''
          write(*,*) '   cleansdf 2d_Phi_2 . 1' 
          write(*,*) ''
          write(*,*) '                *****************'
          stop
       end if

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
      end do

c      ### read in arguments
      name   = sarg(1,'2d_Phi_3.sdf')
      cline  = sarg(2,'*-*')
      x0     = i4arg(3,-1)
      y0     = i4arg(4,-1)
      z0     = i4arg(5,-1)
      stride = 1

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'cleansdf: Reading in file ',name
         write(*,*) 'cleansdf: Outputting times ', cline
         write(*,*) '*******************************'
      end if
c      ### set up output vector
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
c         write(*,*) 'procsdf: using *-* as index vector'
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if
c     ### get rank
      if (trace2) then
          write(*,*) 'cleansdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'cleansdf: Unable to read ', name
         write(*,*) 'cleansdf: gf3_rc = ', gf3_rc
         write(*,*) 'cleansdf: Quitting....'
         goto 800
      end if
      if (trace2) then
          write(*,*) 'cleansdf:  Finished reading in sdf file...'
          write(*,*) 'cleansdf:  rank = ', rank
      end if
      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      if (trace2) write(*,*) '    junk = ', junk
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND
      if (trace2) write(*,*) ' outputing to: ', nameout

      step = 0
      do while (.true.)
         step = step + 1
         gf3_rc = gft_read_shape( name,     step,   shape)
         !if (rank.ne.3) exit
         ! important for call to checkmem:
         if (rank.lt.3) shape(3) = 1
         if (rank.lt.2) shape(2) = 1
         call checkmem(shape(1),shape(2),shape(3),MAXPOINTS,MAXCOORDS)
         gf3_rc = gft_read_full(  name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
         if (gf3_rc .eq. 1) then
            k              = INDEX(cnames, '|')
            tcnames(1)     = cnames(:k-1)
            tcnames_len(1) = k-1
            j          = k+1
            do i = 2, rank-1
               k              = INDEX(cnames(j:), '|') + j - 1
               tcnames(i)     = cnames(j:k-1)
               tcnames_len(i) = k-j 
               j              = k+1
            end do
            tcnames(rank)     = cnames(j:)
            tcnames_len(rank) = INDEX(tcnames(rank),' ') - 1
            if (trace2) then
               do i = 1, rank
                  write(*,*) tcnames(i), tcnames_len(i),i
               end do
            end if
            if (trace2) then
               write(*,*) '..........successfully read'
               write(*,*) '..........liv  ', liv 
               write(*,*) '..........iv(1)', iv(1) 
               write(*,*) '           step:',step
               write(*,*) '           time:',time
               write(*,*) '           rank:',rank
               write(*,*) '           shape    coord name:'
               do i = 1, rank
                 write(*,*) ' ',shape(i),'   ',tcnames(i)
               end do
            end if
            if ( ivindx(iv,liv,step) .gt. 0 ) then
                numreplaced = 0
                do i = 1, shape(1)
                do j = 1, shape(2)
                do k = 1, shape(3)
                   if (i.eq.201.and.j.eq.214.and.k.eq.233) then
                      ltrace=.true.
                   else
                      ltrace=.false.
                   end if
                   ! X-direction:
                   if (i.gt.1) then
                    leftfdiff =  2.d0*(
     .              (  data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i-1)
     .                -data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .              )/(data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i-1)
     .                +data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                ))
                   else
                    leftfdiff =  0.d0
                   endif
                   if (i.lt.shape(1)) then
                    rightfdiff =  2.d0*(
     .              (  data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                -data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i+1)
     .              )/(data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                +data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i+1)
     .                ))
                   else
                    rightfdiff =  0.d0
                   endif
                   ! Y-direction:
                   if (j.gt.1) then
                    frontfdiff =  2.d0*(
     .              (  data((k-1)*shape(2)*shape(1) +(j-2)*shape(1)+i)
     .                -data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .              )/(data((k-1)*shape(2)*shape(1) +(j-2)*shape(1)+i)
     .                +data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                ))
                   else
                    frontfdiff =  0.d0
                   endif
                   if (j.lt.shape(2)) then
                    backfdiff =  2.d0*(
     .              (  data((k-1)*shape(2)*shape(1) +(j  )*shape(1)+i)
     .                -data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .              )/(data((k-1)*shape(2)*shape(1) +(j  )*shape(1)+i)
     .                +data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                ))
                   else
                    backfdiff =  0.d0
                   endif
                   ! Z-direction:
                   if (k.gt.1) then
                    topfdiff =  2.d0*(
     .              (  data((k-2)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                -data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .              )/(data((k-2)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                +data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                ))
                   else
                    topfdiff =  0.d0
                   endif
                   if (k.lt.shape(3)) then
                    bottomfdiff =  2.d0*(
     .              (  data((k  )*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                -data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .              )/(data((k  )*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                +data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                ))
                   else
                    bottomfdiff =  0.d0
                   endif
                   !
                   ! Copy over data point to diagnostic SDF:
                   !    (if point is fine, we'll write this to 0)
                   tdata2((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)=
     .               data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
                   !
                   ! The anomolies should have the following properties:
                   !    1) point extends above its neighbors, in which
                   !       case the diffs should have opposite sign   
                   !    2) the fractional diffs should be roughly the same
                   !       magnitude...because they have opposite sign,
                   !       the abs() of their sum should be small
                   !    3) the fractional diffs should both be large
                   !
                   !
                   if (ltrace) then
                      write(*,*) 'x:',leftfdiff,   rightfdiff
                      write(*,*) '  ',leftfdiff*rightfdiff,
     .                                leftfdiff+rightfdiff
                      write(*,*) 'y:',frontfdiff,  backfdiff
                      write(*,*) 'z:',bottomfdiff, topfdiff
                   end if
                   if ( (leftfdiff*rightfdiff     .lt.0)        .and.
     .                  (abs(leftfdiff+rightfdiff)    .lt.0.5)  .and.
     .                  (abs(rightfdiff)     .gt.THRESH)        .and.
     .                  (abs(leftfdiff)      .gt.THRESH)          ) then
                      if(ltrace)write(*,*)'smoothing x',i,j,k
                    tdata((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)=
     .               0.5d0*(
     .               data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i-1)
     .              +data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i+1)
     .                      )
                    numreplaced = numreplaced + 1
                    !
                   elseif ( (frontfdiff*backfdiff     .lt.0)       .and.
     .                      (abs(frontfdiff+backfdiff)    .lt.0.5) .and.
     .                      (abs(backfdiff)        .gt.THRESH)     .and.
     .                      (abs(frontfdiff)      .gt.THRESH)      )then
                      if(ltrace)write(*,*)'smoothing y',i,j,k
                    tdata((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)=
     .               0.5d0*(
     .               data((k-1)*shape(2)*shape(1) +(j-2)*shape(1)+i)
     .              +data((k-1)*shape(2)*shape(1) +(j  )*shape(1)+i)
     .                      )
                    numreplaced = numreplaced + 1
                    !
                   elseif ( (topfdiff*bottomfdiff     .lt.0)       .and.
     .                      (abs(topfdiff+bottomfdiff)    .lt.0.5) .and.
     .                      (abs(bottomfdiff)        .gt.THRESH)   .and.
     .                      (abs(topfdiff)      .gt.THRESH)        )then
                      if(ltrace)write(*,*)'smoothing z',i,j,k
                    tdata((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)=
     .               0.5d0*(
     .               data((k-2)*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .              +data((k  )*shape(2)*shape(1) +(j-1)*shape(1)+i)
     .                      )
                    numreplaced = numreplaced + 1
                    !
                   else
                    ! No issues with this point
                    ! Direct copy:
                    tdata((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)=
     .               data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
                   tdata2((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)=
     .               0.d0
                   end if
                end do
                end do
                end do
                gf3_rc = gft_out_full(nameout, time, shape,cnames,
     .                                     rank, coords, tdata)
                nameout2 = "diagnostic"
                gf3_rc = gft_out_full(nameout2,time, shape,cnames,
     .                                     rank, coords, tdata2)
                write(*,*)"cleansdf: nureplaced =",numreplaced,
     .                       shape(1)*shape(2)*shape(3),
     .            100.0*numreplaced/shape(1)/shape(2)/shape(3)
            end if
         else 
            if(trace2)write(*,*) "cleansdf: end of file ", name
            goto 800
         end if
      end do
 800  continue
      end



