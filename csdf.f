c     program csdf
      implicit none

      character*32  name
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 10000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, x,y,z, trank, vsrc,vsxynt
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 3000000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS)

      real*8        l2norm, maxdata, mindata,temp, 
     .                      maxdata_rho, maxdata_z,
     .                      mindata_rho, mindata_z
      logical       trace,           trace2
      parameter   ( trace = .false. )
c     parameter   ( trace = .true.  )
      parameter   ( trace2= .false. )
c     parameter   ( trace2= .true. )

c      ### usage statement
       if (iargc() .lt. 1) then
          write(*,*) '*************************************************'
          write(*,*) '*  Takes 2D data slice in (x,y) and exchanges:  *'
          write(*,*) '*                y <---> t                      *'
          write(*,*) '*************************************************'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       csdf  <sdf filename>'
          stop
       end if

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
      end do

c      ### read in arguments
      name   = sarg(1,'2d_Phi_3.sdf')

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'csdf: Reading in file ',name
         write(*,*) '*******************************'
      end if

c     ### get rank
      step = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'procsdf: Unable to read ', name
         write(*,*) 'procsdf: gf3_rc = ', gf3_rc
         write(*,*) 'procsdf: Quitting....'
         goto 800
      end if

      if (rank.ne.2) then
          write(*,*) 'csdf:  Rank is not equal to 2, Quitting...'
          stop
      end if

      step = 0
      do while (.true.)
         step = step + 1
         gf3_rc = gft_read_full( name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then
            if (
     .              ((shape(1)*shape(2).gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)+shape(2)).gt.MAXCOORDS))
     .          ) then
                write(*,*) '2d data set too big for mem. allocated'
                write(*,*) 'Maxpoints ',MAXPOINTS, shape(1)*shape(2)
                write(*,*) 'Maxcoords ',MAXCOORDS, shape(1)+shape(2)
                stop
            end if
c               ################################
c               ###  2 Dimensional sdf files ###
c               ################################
c
c                  Output 1d slice y = constant
c
                   do y=1,shape(2)
                      vsrc = vsxynt(name,coords(shape(1)+y), coords(1),
     .                          data( (y-1)*shape(1)+1 ), shape(1)  )
                   end do
 	      else 
             if (trace2) 
     .           write(*,*) "procsdf: end of file ",
     .                       name
             goto 800
         end if
      end do
		
 800  continue
      end





