      program main
      implicit none
      !From: 
      !http://www.fftw.org/fftw2_doc/fftw_3.html#SEC16
      ! Note also that we use the standard "in-order" output ordering--the k-th output corresponds to the frequency k/n (or k/T, where T is your total sampling period). For those who like to think in terms of positive and negative frequencies, this means that the positive frequencies are stored in the first half of the output and the negative frequencies are stored in backwards order in the second half of the output. (The frequency -k/n is the same as the frequency (n-k)/n.)
      !include 'fftw3.f'
      include '/usr/include/fftw3.f'

      integer     N, i, numpoints
      parameter ( N = 200000 )
      !parameter ( N = 10001 )
      !parameter ( N = 5001 )
      !parameter ( N = 1001 )
      !parameter ( N = 101 )
      real*8     PIE
      parameter (PIE=3.1415926535897932385d0)
      ! For Testing:
      real*8      omega,           phase
      parameter ( omega = 77.d0,   phase = 10.d0 )
      real*8      minx,      maxx
      real*8      tempx, tempy
      !parameter ( minx=5.d0, maxx  = 15.d0 )
            double precision in
             dimension in(N)
             double complex out
             dimension out(N/2 + 1)
             integer*8 plan
      real*8 power(N/2+1)
      real*8 f_out, omega_out, x_in, x_prev
      real*8 deltax, newdeltax
      character*128 name, sarg
      integer myfile, getu
      ! 
      ! Normalize:  such that fft of sin(omega*t) gives a power of 1
      ! 
      logical    normalize
      parameter (normalize = .true. )
      real*8     factor_norm
      ! Just for testing the code:
      logical    test
      parameter (test = .false. )
      logical    ltrace
      parameter (ltrace = .false. )
     

       if (iargc() .lt. 1) then
          write(*,*) '*************************************************'
          write(*,*) '*                                               *'
          write(*,*) '*  fftpower:                                    *'
          write(*,*) '*            Computes the power spectrum        *'
          write(*,*) '*            of a signal stored in a file.      *'
          write(*,*) '*            Data is required to be in          *'
          write(*,*) '*            the form:                          *'
          write(*,*) '*                 <x_i> <data_i>                *'
          write(*,*) '*            and needs to be uniform in x_i.    *'
          write(*,*) '*                                               *'
          write(*,*) '*            Outputs:                           *'
          write(*,*) '*                 <omega_i> <|phi|^2>           *'
          write(*,*) '*              where |phi|^2 is the square      *'
          write(*,*) '*              of the complex FFT output.       *'
          write(*,*) '*                                               *'
          write(*,*) '*  Usage:                                       *'
          write(*,*) '*    fftpower <filename>                        *'
          write(*,*) '*                                               *'
          write(*,*) '*************************************************'
          stop
       end if

      ! initialize to blanks
      call sload(name,' ')
      ! read in argument
      name   = sarg(1,'test.dat')

      if(ltrace)write(*,*)'powerfft: Going to read filename = ',name

      if (.not.test) then
         myfile = getu()
         open(myfile,file=name,form='formatted',status='old',err=800)
         if(ltrace)write(*,*)'powerfft: File opened, myfile=',myfile
         i = 0
         minx = +9d90
         maxx = -9d90
  8      read(myfile,*,end=10,err=801) x_in, tempy
!77      format(2G21.14)
         if(ltrace)write(*,*)'powerfft: Read one line:',x_in,tempy
         i = i + 1
         if (i.gt.N) then
            write(*,*)'powerfft: Storage exceeded: ',N
            stop
            !write(*,*)'powerfft: only processing some data'
            !goto 10
         end if
         in(i) = tempy
         if(ltrace)write(*,*)'powerfft: i, in(i): ',i,in(i)
         if (x_in.gt.maxx) maxx=x_in
         if (x_in.lt.minx) minx=x_in
         if (i.eq.2) then
            deltax = x_in-x_prev
         else if (i.gt.2) then
            newdeltax = x_in-x_prev
            if (abs(newdeltax-deltax).gt.1.d-6) then
            write(*,*)'powerfft: Input data not uniform in x'
            write(*,*)'powerfft: x_in/prev=',x_in,x_prev
            write(*,*)'powerfft: deltax=',deltax,newdeltax
            stop
            end if
         end if
         x_prev = x_in
         goto 8
 10      continue
         if (i.eq.0) write(*,*) 'powerfft: problem reading file'
         close(myfile)
         numpoints = i
      else
         !   Just for testing:
          minx = 0.d0
          maxx = 8.d0
          do i = 1, N
            x_in  = 1.d0*(i-1) * (maxx-minx) / (1.d0*N)
            !in(i) = 1.d0 * dsin( omega*x_in + phase )
            in(i) = 1.d0 * dsin( omega*x_in + phase )
     .             +2.d0 * dcos( 2.d0*omega*x_in )
            !write(*,*) x_in,in(i)
          end do
          !stop
          !
          numpoints = N
      end if

      !write(*,*) 'fftpower: Using numpoints = ', numpoints
      call dfftw_plan_dft_r2c_1d(plan,numpoints,in,out,FFTW_ESTIMATE)
      call dfftw_execute_dft_r2c(plan, in, out)
      call dfftw_destroy_plan(plan)

      if (normalize) then
         ! For each DFT output:
         factor_norm = 1.d0/(0.5d0*numpoints)
      else
         factor_norm = 1.d0
      end if
      do i = 1, numpoints/2+1
         !power(i)  = out(i)*out(i)
         power(i)  = abs(out(i)*out(i))
         f_out     = (1.d0*(i-1))/(maxx-minx)
         omega_out = 2.d0*PIE*f_out
         write(*,*) omega_out,power(i)*factor_norm**2
      end do
      stop
 800  continue
      write(*,*) 'fftpower: file: ',name,' not found.'
      stop
 801  continue
      write(*,*) 'fftpower: Problem reading file: ',name(1:12)
      stop

      end
