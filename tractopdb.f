c     program tractopdb
      implicit none

      include 'silo.inc'
      character*18  APPEND2
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d38)
      integer       mystringlength
      external      mystringlength
      integer       MAXFILES,         numfiles
      parameter (   MAXFILES = 1025 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 13000000 )

      real*8        times(MAXFILES), mintime
      character*32  name,   nameout, fnames(MAXFILES), siloname
      integer                                         lsiloname
      character*5   tmps

      character*128 sarg,         cnames
      integer       junk,         shape(3),
     .              mintime_i,    steps(MAXFILES),
     .              gf3_rc,       gf3_rct,
     .              rank,         nx, ny, nz,
     .              x0,y0,z0,     step,
     .              iargc,        i,j,k,l,
     .              maxi,maxj,maxk, mini,minj,mink, ax,ay,az
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              xval, yval, zval, lasttime,
     .              minx, maxx,
     .              miny, maxy,
     .              minz, maxz, dx,dy,dz, dxt, dyt, dzt

      logical       first_datagrp, first_dataset

      !
      ! SDF Utilities found in libbbhutil.a
      !    (see bbhutil.ps for Matt's documentation)
      !
      integer       gft_out_full,    gft_read_rank,
     .              gft_read_full,   gft_read_shape
      external      gft_out_full,    gft_read_rank,
     .              gft_read_full,   gft_read_shape

      logical       trace,           trace2
      parameter   ( trace = .false.  )
      parameter   ( trace2= .false. )

      integer err, ierr, ndims, NPTS, numparticles
      parameter (NPTS = 1000)
      real a(NPTS), x(NPTS), y(NPTS), z(NPTS), t, angle
      integer dbfile

       !## usage statement
       numfiles = iargc()
       !
       ! Even if one file, still do it (so that batch scripts 
       ! don't have to consider such cases specially):
       !
       if (numfiles .lt. 3 .or. numfiles .gt. MAXFILES) then
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       tractopdb  <sdf filename-x> <sdf fname-y>',
     *                ' <sdf filename-z> [<fname4> <finame5> ....]'
          write(*,*) ''
          write(*,*) '         Produces a silo file that assumes each'
          write(*,*) '         index into these 1D arrays correspond'
          write(*,*) '         to a tracer particle. The first three'
          write(*,*) '         SDF files are assumed to be the x, y, '
          write(*,*) '         z coordinates of the particles, and'
          write(*,*) '         subsequent files denote properties of'
          write(*,*) '         the particles, such as density, etc'  
          write(*,*) ''
          write(*,*) ''
          write(*,*) '  Notes: Maximum number of files: ',MAXFILES
          write(*,*) ''
          stop
       end if

      !
      ! Read in first data set, then for all others
      ! slice along the coordinate of the z-index chosen:
      !
      first_datagrp = .true.
      first_dataset = .true.
      mintime       = LARGENUMBER
      shape(1)      = 1
      shape(2)      = 1
      shape(3)      = 1

      call sload(cnames,' ')
      do i = 1 , numfiles
         steps(i)    = 0
         call sload(fnames(i),' ')
         fnames(i)   = sarg(i,'2d_Phi_3.sdf')
      end do


      !
      ! Setup what will be appended to file name:
      !   (e.g. chi.sdf  ---> chi_i=11.sdf )
      !
      call sload(APPEND2,' ')
      APPEND2(1:) = '_joined'

      !
      ! Create output file name
      ! based on input filename and user option:
      !
      name = fnames(1)
      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND2

      if (trace) then
         write(*,*) '*******************************'
         do i = 1, numfiles
            write(*,*) 'tractopdb: Reading in file ',fnames(i)
         end do
         write(*,*) 'tractopdb: Outputting w/ :   ',APPEND2
         write(*,*) '          Outputing to:     ',nameout
         write(*,*) '*******************************'
      end if
      write(*,*) '          Outputing to:     ',nameout

      !
      ! Get rank:
      !
      if (trace2) then
          write(*,*) 'tractopdb:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step   = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'tractopdb: Unable to read ', name
         write(*,*) 'tractopdb: gf3_rc = ', gf3_rc
         write(*,*) 'tractopdb: Quitting....'
         stop
      end if

      !####################################################################
      !
      ! Step into each file once:
      !
      !####################################################################
 20   continue
      i        = 1
      gf3_rct  = gft_read_shape( fnames(i),    steps(i)+1,   shape)
      call checkmem( shape(1),shape(2),shape(3),MAXPOINTS,MAXCOORDS)
      if (gf3_rct.eq.1) then
         do i = 1 , numfiles
            steps(i) = steps(i) + 1
            gf3_rct  = gft_read_full(  fnames(i),    steps(i),   shape,
     .                           cnames,   rank,   times(i),
     .                           coords,   data           )
            if (trace2) write(*,*) '  gf3_rct:  ',gf3_rct
            !
            numparticles = shape(1)
            if (numparticles .gt. NPTS) numparticles=NPTS
            if(.true.)write(*,*)'i step numparticles=',
     .                         i,steps(i),numparticles
            if (i.eq.1) then
               do j = 1, numparticles
                  x(j) = data(j)
               end do
            else if (i.eq.2) then
               do j = 1, numparticles
                  y(j) = data(j)
               end do
            else if (i.eq.3) then
               do j = 1, numparticles
                  z(j) = data(j)
               end do
            else 
               do j = 1, numparticles
                  !other(j) = data(j)
               end do
            end if
            if (trace2) write(*,*) '  Read for: ',i,steps(1),fnames(1)
         end do
         !
         call int2str(steps(1),tmps)
         siloname               = 'my'
         lsiloname              = mystringlength(siloname)
         if      (steps(1).lt.10) then
            siloname(lsiloname+1:) = '0000'
            siloname(lsiloname+5:) = tmps
         else if (steps(1).lt.100) then
            siloname(lsiloname+1:) = '000'
            siloname(lsiloname+4:) = tmps
         else if (steps(1).lt.1000) then
            siloname(lsiloname+1:) = '00'
            siloname(lsiloname+3:) = tmps
         else if (steps(1).lt.10000) then
            siloname(lsiloname+1:) = '0'
            siloname(lsiloname+2:) = tmps
         end if
         lsiloname              = mystringlength(siloname)
         siloname(lsiloname+1:) = '.silo'
         write(*,*) 'tractopdb: siloname = ',siloname
         ierr = dbcreate(siloname, 20, DB_CLOBBER, DB_LOCAL,
     .             "trialplot", 9, DB_PDB, dbfile)
         ndims = 3
         err = dbputpm (dbfile, "pointmesh", 9, ndims, x, y,
     . z, NPTS, DB_DOUBLE, DB_F77NULL, ierr)
         ! The integers below are apparently just the number of characters!
         err = dbputpv1(dbfile, "junk1", 5,"pointmesh", 9, a, NPTS,
     .             DB_DOUBLE,  DB_F77NULL, ierr)
         ierr = dbclose(dbfile)
         !
         goto 20
      else
         if(.true.)write(*,*)'tractopdb: Total steps:',steps(1)
         if(.true.)write(*,*)'tractopdb: numparticles:',numparticles
         if(.true.)write(*,*)'tractopdb:No more data to be read',gf3_rct
      end if
      !
      if (trace) then
         write(*,*) '                      mintime = ',mintime
         write(*,*) '                    mintime_i = ',mintime_i
         write(*,*) '                    steps()   = ',steps(mintime_i)
         write(*,*) '                    fnames()  = ',fnames(mintime_i)
         write(*,*) '................................'
      end if


      end
