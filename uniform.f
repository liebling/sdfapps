      program uniform
      implicit none

      integer     maxN, N, i
      parameter ( maxN = 10 000 000 )
      real*8     PIE
      parameter (PIE=3.1415926535897932385d0)
      real*8     SMALLNUMBER
      parameter (SMALLNUMBER=1.d-13)
      real*8      omega,           phase
      parameter ( omega = 77.d0,   phase = 10.d0 )
      real*8      minx,      maxx
      real*8      tempx, tempy
      real*8 x_in(MaxN), x_out(MaxN)
      real*8   in(MaxN),   out(MaxN)
      real*8 f_out, omega_out, x_prev
      real*8 deltax, newdeltax
      character*128 name, sarg
      integer myfile, getu, i4arg
     

       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '*                                               *'
          write(*,*) '*  uniform:                                     *'
          write(*,*) '*            Takes as input a data file in      *'
          write(*,*) '*            the form:                          *'
          write(*,*) '*                 <x_i> <data_i>                *'
          write(*,*) '*            and interpolates them onto a       *'
          write(*,*) '*            uniform in x_i grid.               *'
          write(*,*) '*                                               *'
          write(*,*) '*            Outputs:                           *'
          write(*,*) '*                 <x_i> <interp. data>          *'
          write(*,*) '*                                               *'
          write(*,*) '*  Usage:                                       *'
          write(*,*) '*    uniform  <filename> <num points>           *'
          write(*,*) '*                                               *'
          write(*,*) '*************************************************'
          stop
       end if

      ! initialize to blanks
      call sload(name,' ')
      ! read in argument
      name   = sarg(1,'test.dat')
      N      = i4arg(2,1001)
      if (N.gt.maxN) then
         write(*,*)'uniform: Not enough memory'
         stop
      end if

      myfile = getu()
      open(myfile,file=name,form='formatted',status='old',err=800)
      i = 0
      minx = +9d99
      maxx = -9d99
  8   read(myfile,*,end=10) tempx, tempy
      i = i + 1
      x_in(i) = tempx
      in(i)   = tempy
      if (x_in(i).gt.maxx) maxx=x_in(i)
      if (x_in(i).lt.minx) minx=x_in(i)
      if (i.eq.2) then
         deltax = x_in(i)-x_prev
      else if (i.gt.2) then
         newdeltax = x_in(i)-x_prev
         if (x_in(i).lt.x_prev) then
            write(*,*)'uniform: Input data not ordered in x'
            write(*,*)'uniform: x_in/prev=',x_in(i),x_prev
            stop
         end if
         if (abs(newdeltax).lt.SMALLNUMBER) then
            if (.false.) then
               write(*,*)'uniform: Repeated x-value:'
               write(*,*)'uniform: x_in/prev=',x_in(i),x_prev
               write(*,*)'uniform: This messes up the interpolation.'
               stop
            else
               !write(*,*)'uniform: We could just skip it'
               !write(*,*)'uniform: Ignoring'
               i = i - 1
            end if
         end if
      end if
      if (i.eq.maxN) then
            write(*,*)'uniform: Too much data: ',maxN
            !stop
            goto 10
      end if
      x_prev = x_in(i)
      goto 8
 10   close(myfile)

      if (.false.) then
         write(*,*)'uniform: minx/maxx ',minx,maxx
         write(*,*)'uniform: read in data i=',i
         write(*,*)'uniform: Interpolate to grid w/ N=',N
      end if

      call load_ramp(x_out,maxx,minx,N)
      call interpolate_uniform(out,x_out,in,x_in,N,i)

      do i = 1, N
         write(*,*) x_out(i),out(i)
      end do

      stop
 800  continue
      write(*,*) 'uniform: file: ',name,' not found.'

      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine load_ramp(x,maxx,minx,n)
      implicit none
      integer n
      real*8 x(n), maxx, minx
      !
      integer i
      real*8  dx

      dx = (maxx-minx) / (n-1)
      do i = 1, n
         x(i) = minx + (i-1)*dx
      end do

      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine interpolate_uniform(yo,xo,yi,xi,no,ni)
      implicit none
      integer no, ni
      real*8  xi(ni), yi(ni)
      real*8  xo(no), yo(no)
      !
      real*8  interp_cubic
      integer o
      integer l2, l1, r1, r2

      l2 = 1
      l1 = 2
      r1 = 3
      r2 = 4
      do o = 1, no
         !
         do while (xo(o).gt.xi(r1) .and. r2.lt.ni) 
            l2 = l2 + 1
            l1 = l1 + 1
            r1 = r1 + 1
            r2 = r2 + 1
         end do
         !write(*,*)'interp_cubic: l2,l1,r1,r2: ',l2,l1,r1,r2
         !write(*,*)'interp_cubic: x:',xi(l2),xi(l1),xi(r1),xi(r2)
         !write(*,*)'interp_cubic: y:',yi(l2),yi(l1),yi(r1),yi(r2)
         yo(o) = interp_cubic(yi(l2),yi(l1),yi(r1),yi(r2), xo(o),
     .                        xi(l2),xi(l1),xi(r1),xi(r2)         )
         !write(*,*)'interp_cubic: output: ',xo(o),yo(o)
         !
      end do

      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  interp_cubic:                                                             cc
cc                                                                            cc
cc    Finds cubic interpolated point at x based on points 1,2,3,4             cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function interp_cubic(y1,y2,y3,y4, x,x1,x2,x3,x4)
      implicit    none
      real(kind=8)      y1, y2, y3, y4, x, x1, x2, x3, x4
      real(kind=8)      xx1, xx2, xx3, xx4


      real(kind=8)      l12,l13,l14
      real(kind=8)          l23,l24
      real(kind=8)              l34
      real(kind=8)              lll
      logical     ltrace
      parameter ( ltrace = .false. )
      real*8     VERYSMALLNUMBER
      parameter (VERYSMALLNUMBER=1.d-26)
      ! Basically for testing and understanding noise:
      logical     justlinear
      parameter ( justlinear = .false. )

      xx1  = x  - x1
      xx2  = x  - x2
      xx3  = x  - x3
      xx4  = x  - x4

      l12  = x1 - x2
      l13  = x1 - x3
      l14  = x1 - x4
      l23  = x2 - x3
      l24  = x2 - x4
      l34  = x3 - x4
      lll  = abs(l12*l13*l14*l23*l24*l34)

      if (.not.justlinear .and.
     .     lll.gt.VERYSMALLNUMBER ) then
         !
         if(ltrace)write(*,*) 'interp_cubic: cubic interpolation'
         interp_cubic = xx2    *xx3    *xx4     * y1
!    *                / ( (x1-x2)*(x1-x3)*(x1-x4) )
     *                / ( l12*l13*l14 )
     *
     *              +   xx1    *xx3    *xx4     * y2
!    *                / ( (x2-x1)*(x2-x3)*(x2-x4) )
     *                / ( -l12*l23*l24 )
     *
     *              +   xx1    *xx2    *xx4     * y3
!    *                / ( (x3-x1)*(x3-x2)*(x3-x4) )
     *                / (  l13*l23*l34 )
     *
     *              +   xx1    *xx2    *xx3     * y4
!    *                / ( (x4-x1)*(x4-x2)*(x4-x3) )
     *                / ( -l14*l24*l34 )
         !
      else if (abs(l23).gt.VERYSMALLNUMBER) then
      !if (justlinear) then
         if(ltrace)write(*,*) 'interp_cubic: linear on y2/y3'
         interp_cubic = (x3-x ) * y2
!    *                / (x3-x2)
     *                / (-l23 )
     *
     *              +   ( x-x2) * y3
!    *                / (x3-x2)
     *                / (-l23 )
      else 
         ! simple average:
         if(ltrace)write(*,*) 'interp_cubic: just averaging:'
         interp_cubic = 0.25d0*(y1+y2+y3+y4)
      end if
      if (ltrace) then
         if(ltrace)write(*,*) 'interp_cubic: y1 = ', y1
         write(*,*) 'interp_cubic: y2 = ', y2
         write(*,*) 'interp_cubic: y3 = ', y3
         write(*,*) 'interp_cubic: y4 = ', y4
         write(*,*) 'interp_cubic: x1 = ', x1
         write(*,*) 'interp_cubic: x2 = ', x2
         write(*,*) 'interp_cubic: x3 = ', x3
         write(*,*) 'interp_cubic: x4 = ', x4
         write(*,*) 'interp_cubic: lll = ',lll
c        write(*,*) 'interp_cubic: xx1 = ', xx1
c        write(*,*) 'interp_cubic: xx2 = ', xx2
c        write(*,*) 'interp_cubic: xx3 = ', xx3
c        write(*,*) 'interp_cubic: xx4 = ', xx4
         write(*,*) 'interp_cubic: x  = ', x
         write(*,*) 'interp_cubic: y  = ', interp_cubic
      end if


      return
      end    ! END function interp_cubic
