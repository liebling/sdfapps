c     program subtractddsdf
      implicit none

      character*18   APPEND2, tmp_chr8, tmp2_chr8

      real*8        FUZZ
      parameter   ( FUZZ = 1.D-6 )

      character*32  name,   nameout, tcnames(3)
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
c     parameter   ( mxliv = 10000 )
      parameter   ( mxliv = 1000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),tshape(3),
     .              gf3_rc,rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, trank,
     .              x0,y0,z0, numgrids
      integer       myindex
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,j
     .              maxi,maxj,maxk, mini,minj,mink, ax,ay,az
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
c     parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 8000000 )
c     parameter   ( MAXCOORDS = 6000,  MAXPOINTS =17000000 )
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS =35000000 )
c     parameter   ( MAXCOORDS = 2000,  MAXPOINTS =136000000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
c     real*8         data(9 000 000), coords(MAXCOORDS), time,
     .              tdata(MAXPOINTS), chr(MAXPOINTS),
c    .              tcoords(MAXCOORDS),
     .              bbox(6),
     .              xval, yval, zval, lasttime,
     .              minx, maxx,
     .              miny, maxy,
     .              minz, maxz, dx,dy,dz, dxt, dyt, dzt
      real*8        myl2norm3d
      external      myl2norm3d

      !
      ! SDF Utilities found in libbbhutil.a
      !    (see bbhutil.ps for Matt's documentation)
      !
      integer       gft_out_full,    gft_read_rank,
     .              gft_read_full,   gft_read_shape,
     .              gft_out_bbox
      external      gft_out_full,    gft_read_rank,
     .              gft_read_full,   gft_read_shape
     .              gft_out_bbox

      logical       first_datagrp, first_dataset, nomore

      logical       AVERAGE
      !parameter   ( AVERAGE = .fal.  )

      logical       trace,           trace2
      parameter   ( trace = .false.  )
      parameter   ( trace2= .false. )

      if (iargc() .lt. 1) then
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '   subtractddsdf  <sdf filename> [<outputvector>]'
          write(*,*) ''
          write(*,*) '  Notes: Subtracts grids:'
          write(*,*) '         1) that have identical resolution'
          write(*,*) '         2) in regions where they overlap'
          write(*,*) '         3) are defined at the same time'
          write(*,*) ''
          write(*,*) ' trace  = ',trace
          write(*,*) ' trace2 = ',trace2
          stop
      end if

      minx =  LARGENUMBER
      miny =  LARGENUMBER
      minz =  LARGENUMBER
      maxx = -LARGENUMBER
      maxy = -LARGENUMBER
      maxz = -LARGENUMBER
      dx   = 0.d0
      dy   = 0.d0
      dz   = 0.d0
      dxt  = 0.d0
      dyt  = 0.d0
      dzt  = 0.d0
      call load_values1d(tdata,-13.d0,MAXPOINTS)
      call load_values1d(chr,   0.0d0,MAXPOINTS)

      !
      ! Read in first data set, then for all others
      ! slice along the coordinate of the z-index chosen:
      !
      first_datagrp = .true.
      first_dataset = .true.

      !
      ! When done reading in data, set nomore to true,
      ! and then output final data set:
      !
      nomore        = .false.

      !
      ! Initialize these strings to blanks:
      !
      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
      end do

      !
      ! Read in arguments:
      !
      name   = sarg(1,'2d_Phi_3.sdf')
      cline  = sarg(2,'*-*')
      i      = i4arg(3,0)
      if (i.eq.0) then
         AVERAGE = .true.
      else
         AVERAGE = .false.
      end if
      !write(*,*) 'AVERAGE = ',AVERAGE

      !
      ! Setup what will be appended to file name:
      !   (e.g. chi.sdf  ---> chi_i=11.sdf )
      !
      call sload(APPEND2,' ')
      APPEND2(1:) = '_subtract'

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'subtractddsdf: Reading in file ',name
         write(*,*) 'subtractddsdf: Outputting times ', cline
         write(*,*) 'subtractddsdf: Outputting w/ : ',APPEND2
         write(*,*) '*******************************'
      end if

      !
      ! Setup output vector:
      !
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

      !
      ! Get rank:
      !
      if (trace2) then
          write(*,*) 'subtractddsdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step   = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'subtractddsdf: Unable to read ', name
         write(*,*) 'subtractddsdf: gf3_rc = ', gf3_rc
         write(*,*) 'subtractddsdf: Quitting....'
         goto 800
      end if
c     if (rank.ne.3) then
c         write(*,*) 'subtractddsdf:  Rank not equal to 3'
c         write(*,*) 'subtractddsdf:  Quitting...'
c         stop
c     end if

      !
      ! Create output file name
      ! based on input filename and user option:
      !
      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND2
      if (trace2) write(*,*) ' outputing to: ', nameout
      if (.true.) write(*,*) ' outputing to: ', nameout

      !####################################################################
      !
      ! Step through all data sets:
      !
      !####################################################################
      step = 0
      do while (.true.)
 99      step   = step + 1
         gf3_rc = gft_read_shape( name,     step,   shape)
         if (rank.eq.2) then
             shape(3) = 1
         else if (rank.eq.1) then
             shape(2) = 1
             shape(3) = 1
         end if
         call checkmem(shape(1),shape(2),shape(3),MAXPOINTS, MAXCOORDS)
         gf3_rc = gft_read_full(  name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
         if (trace) write(*,*) 'rank = ',rank
         if (rank.eq.2) then
            coords(shape(2)+shape(1)+1) = 0.d0
            coords(shape(2)+shape(1)+2) = 0.d0
         else if (rank.eq.1) then
            coords(shape(1)+1) = 0.d0
            coords(shape(1)+2) = 0.d0
            coords(shape(2)+shape(1)+1) = 0.d0
            coords(shape(2)+shape(1)+2) = 0.d0
            if (trace) then
               write(*,*) coords(1), coords(2)
               write(*,*) coords(shape(1)+1), coords(shape(1)+2)
       write(*,*)coords(shape(2)+shape(1)+1),coords(shape(2)+shape(1)+2)
            end if
         end if
         if (trace) write(*,*) '  Read in data, time = ', time,step
         if (gf3_rc .eq. 1) then
            if (first_dataset) then
               dx            =   coords(                  2)
     *                         - coords(                  1)
               dy            =   coords(         shape(1)+2)
     *                         - coords(         shape(1)+1)
               dz            =   coords(shape(2)+shape(1)+2)
     *                         - coords(shape(2)+shape(1)+1)
               if (trace) then
                   write(*,*) '*** Collecting first dataset:'
                   write(*,*) '      dx = ',dx
                   write(*,*) '      dy = ',dy
                   write(*,*) '      dz = ',dz
               end if
               lasttime      = time
               first_dataset = .false.
            else 
               dxt           =   coords(                  2)
     *                         - coords(                  1)
               dyt           =   coords(         shape(1)+2)
     *                         - coords(         shape(1)+1)
               dzt           =   coords(shape(2)+shape(1)+2)
     *                         - coords(shape(2)+shape(1)+1)
               if (abs(dx-dxt).gt.FUZZ) then
                  write(*,*) 'Not equal resolutions in x: ',dx,dxt
                  stop
               end if
               if (abs(dy-dyt).gt.FUZZ) then
                  write(*,*) 'Not equal resolutions in y: ',dy,dyt
                  stop
               end if
               if (abs(dz-dzt).gt.FUZZ) then
                  write(*,*) 'Not equal resolutions in z: ',dz,dzt
                  stop
               end if
            end if
            
            if (time .eq. lasttime) then
               if (trace2) write(*,*)'Adding grid to grouping:',lasttime
               if (first_datagrp) then
                 !
                 ! Get bounds of union of all grids at initial time:
                 !
                 if (minx.gt.coords(                  1))
     *                         minx = coords(                  1)
                 if (miny.gt.coords(         shape(1)+1))
     *                         miny = coords(         shape(1)+1)
                 if (minz.gt.coords(shape(2)+shape(1)+1))
     *                         minz = coords(shape(2)+shape(1)+1)
                 if (maxx.lt.coords(                  shape(1)))
     *                         maxx = coords(                  shape(1))
                 if (maxy.lt.coords(         shape(2)+shape(1)))
     *                         maxy = coords(         shape(2)+shape(1))
                 if (maxz.lt.coords(shape(3)+shape(2)+shape(1)))
     *                         maxz = coords(shape(3)+shape(2)+shape(1))
                 if (trace2) then
                    write(*,*) '               min/maxX = ', minx,maxx
                    write(*,*) '               min/maxY = ', miny,maxy
                    write(*,*) '               min/maxZ = ', minz,maxz
                 end if
                 goto 99
               end if

  20           continue
               !
               ! copy grid data into "merged" grid
               !
               if (dz.gt.0) then
                  az = NINT( (coords(shape(2)+shape(1)+1)-minz) / dz)+1
               else
                  az = 1
               end if
               if (dy.gt.0) then
                  ay = NINT( (coords(         shape(1)+1)-miny) / dy)+1
               else
                  ay = 1
               end if
               ax = NINT( (coords(                  1)-minx) / dx) + 1
               if (trace2) then
                   write(*,*) '*** Copying data into main grid'
                   write(*,*) '               step     = ', step
                   write(*,*) '               ax       = ', ax
                   write(*,*) '               ay       = ', ay
                   write(*,*) '               az       = ', az
               end if
               !if (AVERAGE) then
               if (trace) write(*,*) 'norm of chr: ',
     *             myl2norm3d(chr,tshape(1),tshape(2),tshape(3) )
               ! For subtractive averaging, not regular averaging
               call copy_field3dchranti(data,tdata,chr,
     *                            ax,       ay,       az,
     *                            shape(1), shape(2), shape(3),
     *                           tshape(1),tshape(2),tshape(3) )

!              else
!              call copy_field3d(data,tdata,
!    *                            ax,       ay,       az,
!    *                            shape(1), shape(2), shape(3),
!    *                           tshape(1),tshape(2),tshape(3) )
!              end if
            else
               if (trace) write(*,*) '  Outputting at time ',lasttime
   80          if (first_datagrp) then
                  first_datagrp = .false.
                  tshape(1) = NINT( ( maxx - minx ) / dx) + 1
                  if (dy.gt.0) then
                     tshape(2) = NINT( ( maxy - miny ) / dy) + 1
                  else
                     tshape(2) = 1
                  end if
                  if (dz.gt.0) then
                     tshape(3) = NINT( ( maxz - minz ) / dz) + 1
                  else
                     tshape(3) = 1
                  end if
                  if (trace) then
                    write(*,*) '*** Output grid:'
                    write(*,*) ' Bounds:       min/maxX = ', minx,maxx
                    write(*,*) '               min/maxY = ', miny,maxy
                    write(*,*) '               min/maxZ = ', minz,maxz
                    write(*,*) '               tshape(1)= ', tshape(1)
                    write(*,*) '               tshape(2)= ', tshape(2)
                    write(*,*) '               tshape(3)= ', tshape(3)
                    write(*,*) '*** Checking if we have space:'
                  end if
                  call checkmem(tshape(1),tshape(2),tshape(3),
     .                    MAXPOINTS, MAXCOORDS)
                  if (trace) write(*,*) '*** Setting up coordinates'
                  bbox(1) = minx
                  bbox(2) = maxx
                  bbox(3) = miny
                  bbox(4) = maxy
                  bbox(5) = minz
                  bbox(6) = maxz
                  ! Need to go back and re-read all the data since
                  ! this is the first data grouping, we needed to
                  ! get bounds of "merged" grid before copying data
                  if (trace) write(*,*) '  Rereading in first data'
                  step = 0
                  goto 99
               end if
               !
  88           continue
               !
               ! Zero out any entries that didn't get subtracted:
               !
               do k = 1, tshape(3)
               do j = 1, tshape(2)
               do i = 1, tshape(1)
                  myindex = (tshape(1)*tshape(2))*(k-1)
     .                     +(tshape(1)          )*(j-1)
     .                     +                       i
                  if (mod(nint(chr(myindex)),2).eq.1) then
                     ! an odd number of data points:
                     tdata(myindex) = 0.d0
                  end if
               end do
               end do
               end do
               !
               ! output merged grid:
               !
               gf3_rc = gft_out_bbox(nameout, lasttime,tshape,
! 88           gf3_rc = gft_out_bbox(nameout, lasttime,tshape,
     .                                     rank, bbox, tdata)

               if (nomore) goto 800
               !
               ! Reset for next merged grid:
               !
               lasttime = time
               call load_values3d(chr,0.0d0,tshape(1),
     .             tshape(2),tshape(3))
               call load_values3d(tdata,-0.1d0,tshape(1),
     .             tshape(2),tshape(3))
               goto 20
            end if
 	      else 
             if (trace2) write(*,*) "subtractddsdf: end of file ", name
             nomore = .true.
             if (first_datagrp) goto 80
             goto 88
         end if       ! end if gf_rc>0
      end do
		
 800  continue
      end

