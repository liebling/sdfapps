               subroutine load_values1d(field,value,nx)
               implicit none
               integer  nx
               real*8   value
               real*8   field(nx)
               integer  i

               do i = 1, nx
                  field(i) = value
               end do

               return
               end

               subroutine load_values3d(field,value,nx,ny,nz)
               implicit none
               integer  nx,ny,nz
               real*8   value
               real*8   field(nx,ny,nz)
               integer  i,j,k

               do k = 1, nz
               do j = 1, ny
               do i = 1, nx
                  field(i,j,k) = value
               end do
               end do
               end do

               return
               end

c-------------------------------------------------------------------------------
      double precision function myl2norm3d(u, nx, ny, nz)
c-------------------------------------------------------------------------------
      implicit none
      integer  nx,        ny,   nz
      real(kind=8)   u(nx,ny,nz)
      integer  i,         j,    k


      myl2norm3d = 0.d0

      if ((nx.le.0).or.(ny.le.0).or.(nz.le.0)) then
         write(*,*) 'myl2norm3d: ERROR'
      end if

      do k = 1, nz
         do j = 1, ny
            do i = 1, nx
               myl2norm3d = myl2norm3d + u(i,j,k)**2
            end do
         end do
      end do

      myl2norm3d = sqrt(myl2norm3d/nx/ny/nz)
      return
      end

               subroutine copy_field3dchr(field,tdata,chr,
     *                            ax,       ay,       az,
     *                            nxf,nyf,nzf,
     *                            nxd,nyd,nzd)
               implicit none
               integer  ax, ay, az, nxf,nyf,nzf, nxd,nyd,nzd
               real*8   field(nxf,nyf,nzf), tdata(nxd,nyd,nzd)
               real*8   chr(nxd,nyd,nzd), n
               integer  i,j,k, ii,jj,kk

               do k = 1, nzf
                  kk = k + az - 1
               do j = 1, nyf
                  jj = j + ay - 1
               do i = 1, nxf
                  ii = i + ax - 1
                  n = chr(ii,jj,kk)
                  tdata(ii,jj,kk) = (   n * tdata(ii,jj,kk)
     *                                + field(i,j,k)
     *                                ) / (n + 1.d0)
c                 if (chr(ii,jj,kk).gt. 1.d0) 
c    *              write(*,*) i,j,k,chr(ii,jj,kk)
                  chr(ii,jj,kk) = n + 1.d0
               end do
               end do
               end do

               return
               end
!............................................................
!              same as copy_field3dchr() except that
!              it adds every odd iteration with a negative sign
!              so that one achieves not the average, but instead
!              the subtraction.
!
!............................................................
               subroutine copy_field3dchranti(field,tdata,chr,
     *                            ax,       ay,       az,
     *                            nxf,nyf,nzf,
     *                            nxd,nyd,nzd)
               implicit none
               integer  ax, ay, az, nxf,nyf,nzf, nxd,nyd,nzd
               real*8   field(nxf,nyf,nzf), tdata(nxd,nyd,nzd)
               real*8   chr(nxd,nyd,nzd), n, tmp
               integer  i,j,k, ii,jj,kk

               do k = 1, nzf
                  kk = k + az - 1
               do j = 1, nyf
                  jj = j + ay - 1
               do i = 1, nxf
                  ii = i + ax - 1
                  n = chr(ii,jj,kk)
                  if (mod(NINT(n),2).eq.1) then
                    tmp = -1.d0
                  else
                    tmp = +1.d0
                  end if
                  tdata(ii,jj,kk) = (   tmp* n * tdata(ii,jj,kk)
     *                                + field(i,j,k)
     *                                ) / (n + 1.d0)
c                 if (chr(ii,jj,kk).gt. 1.d0) 
c    *              write(*,*) i,j,k,chr(ii,jj,kk)
                  chr(ii,jj,kk) = n + 1.d0
               end do
               end do
               end do

               return
               end

               subroutine copy_field3d(field,tdata,
     *                            ax,       ay,       az,
     *                            nxf,nyf,nzf,
     *                            nxd,nyd,nzd)
               implicit none
               integer  ax, ay, az, nxf,nyf,nzf, nxd,nyd,nzd
               real*8   field(nxf,nyf,nzf), tdata(nxd,nyd,nzd)
               integer  i,j,k

               do k = 1, nzf
               do j = 1, nyf
               do i = 1, nxf
                  tdata(i+ax-1, j+ay-1, k+az-1) = field(i,j,k)
               end do
               end do
               end do

               return
               end

               subroutine checkmem(nx,ny,nz,numpoints,numcoords)
               implicit none
               integer nx,ny,nz,numpoints,numcoords

               ! check if bad argument passed in
               !    (can happen with rank<3 if calling code is not careful)
               if (nx.lt.0) nx = 1
               if (ny.lt.0) ny = 1
               if (nz.lt.0) nz = 1
               if ( ((nx*ny*nz.gt.numpoints)
     .                        .or.
     .               ((nx+ny+nz).gt.numcoords))
     .             ) then
                   write(*,*) '3d data set too big for mem. allocated'
                   write(*,*) 'nx,ny,nz = ',nx,ny,nz
                   write(*,*) 'Need numpoints >= ',nx*ny*nz,numpoints
                   write(*,*) 'Need numcoords >= ',nx+ny+nz,numcoords
                   stop
                end if

               return
               end

            subroutine findmin(min,min_i,data,num)
            implicit none
            integer  min_i, num
            real*8   min, data(num)
            real*8   tmp
            integer  i

            min_i = 1
            min   = data(min_i)
            do i = 2, num
               tmp = data(i)
               if (tmp.lt.min) then
                  min_i = i
                  min   = tmp
               end if
            end do

            return
            end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  gaussian3d:                                                               cc
cc                   Initializes field to a Gaussian pulse.                   cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine gaussian3d( field, h, minx, miny, minz,nx,ny,nz,
     *           a,ex,ey,r,d,xc,yc,zc)
      implicit    none
      integer    nx, ny, nz
      real*8     field(nx,ny,nz), h, a, ex, ey, r, d,
     *           minx, miny, minz, xc,yc,zc
      real*8     x,y,z
      integer    i,j,k

      logical     ltrace
      parameter ( ltrace = .false. )

      if (ltrace) then
         write(*,*) 'gaussian3d:   h = ',h
         write(*,*) 'gaussian3d: minx = ',minx
         write(*,*) 'gaussian3d: miny = ',miny
         write(*,*) 'gaussian3d: minz = ',minz
         write(*,*) 'gaussian3d:    a = ',a
         write(*,*) 'gaussian3d:   ex = ',ex
         write(*,*) 'gaussian3d:   ey = ',ey
         write(*,*) 'gaussian3d:    r = ',r
         write(*,*) 'gaussian3d:    d = ',d
         write(*,*) 'gaussian3d:   xc = ',xc
         write(*,*) 'gaussian3d:   yc = ',yc
         write(*,*) 'gaussian3d:   zc = ',zc
      end if

      do k = 1, nz
         z = minz + h*(k-1.d0)
         do j = 1, ny
            y = miny + h*(j-1.d0)
            do i = 1, nx
               x = minx + h*(i-1.d0)
               field(i,j,k) = a*exp( -(sqrt(
     *             ex*(x-xc)**2 + ey*(y-yc)**2 + (z-zc)**2)-r)**2/d**2 )
            end do
         end do
      end do

      if (ltrace) then
         write(*,*) 'gaussian3d: maxx = ',x
         write(*,*) 'gaussian3d: maxy = ',y
         write(*,*) 'gaussian3d: maxz = ',z
      end if

      return
      end    ! END: gaussian3d

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc int2str:                                                                   cc
cc           Converts a non-negative integer to a string.                     cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine int2str( myint, mystring )
      implicit none
      character*(*) mystring
      integer       myint
      integer       tmp, numdigits, the_int, poweroften, digit
      character     dig2char
      external      dig2char

      mystring = ''
      if (myint .lt. 0) then
         write(*,*) 'int2str: Cannot convert negative integer.'
         return
      else if (myint .lt. 10) then
         mystring = dig2char(myint)
         return
      end if

      tmp       = 10
      numdigits = 1
 10   if (myint .ge. tmp) then
         tmp       = 10 * tmp
         numdigits = numdigits + 1
         goto 10
      end if

      tmp               = 1
      the_int           = myint
      poweroften        = 10**(numdigits-1)

 20   digit             = the_int / poweroften
      mystring(tmp:tmp) = dig2char(digit)
      tmp               = tmp + 1
      the_int           = the_int - digit * poweroften
      poweroften        =  poweroften / 10
      if (tmp.le.numdigits) goto 20

      mystring(tmp:tmp) = ''

      return
      end        ! END: subroutine int2str

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc dig2char:                                                                  cc
cc           Converts a single digit [0..9] to a character.                   cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      character function dig2char( digit )
      implicit none
      integer       digit

      if      (digit .eq. 0) then
              dig2char = '0'
      else if (digit .eq. 1) then
              dig2char = '1'
      else if (digit .eq. 2) then
              dig2char = '2'
      else if (digit .eq. 3) then
              dig2char = '3'
      else if (digit .eq. 4) then
              dig2char = '4'
      else if (digit .eq. 5) then
              dig2char = '5'
      else if (digit .eq. 6) then
              dig2char = '6' 
      else if (digit .eq. 7) then
              dig2char = '7' 
      else if (digit .eq. 8) then
              dig2char = '8' 
      else if (digit .eq. 9) then
              dig2char = '9'
      end if
      
      return     
      end        ! END: function dig2char



cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  double_equal:                                                             cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      logical function double_equal( first, second )
      implicit    none
      real(kind=8)      first, second
      real(kind=8)      SMALLNUMBER
      parameter       ( SMALLNUMBER = 1.0d-14)

      double_equal = abs(first-second) .lt. SMALLNUMBER

      return
      end    ! END: double_equal


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  interp2d:                                                                 cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function interp2d( field, fx, fy,
     *                                  i,j,nx,ny)
      implicit     none
      integer      i,j,nx, ny
      real(kind=8) field(nx,ny),
     *             fx, fy

      logical     ltrace
      parameter ( ltrace = .false. )

      if (i .eq. nx .or. j .eq. ny) then
         interp2d =
     *                                field(i,  j  )
      else if (i .gt. nx .or. j .gt. ny .or.
     *    i .lt. 1    .or. j .lt. 1         ) then
         return
      else
         interp2d =
     *      + (1.d0-fx) * (1.d0-fy) * field(i,  j  )
     *      +       fx  *       fy  * field(i+1,j+1)
     *      +       fx  * (1.d0-fy) * field(i+1,j  )
     *      + (1.d0-fx) *       fy  * field(i,  j+1)
      end if

      return
      end    ! END: interp2d

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  interp:                                                                   cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function interp3d( field, fx, fy, fz,
     *                                  i,j,k, nx,ny,nz)
      implicit     none
      integer      i,j,k, nx, ny, nz
      real(kind=8) field(nx,ny,nz),
     *             fx, fy, fz

      logical     ltrace
      parameter ( ltrace = .false. )

      if (i .gt. nx-1 .or. j .gt. ny-1 .or. k .gt. nz-1 .or.
     *    i .lt. 1    .or. j .lt. 1    .or. k .lt. 1         ) then
         if (ltrace) then
            write(*,79) 'interp3d: returning...',i,j,k,nx,ny,nz
         end if
         return
      end if
      !
      interp3d =
     *      + (1.d0-fx) * (1.d0-fy) * (1.d0-fz) * field(i,  j,  k  )
      !
     *      + (1.d0-fx) *       fy  *       fz  * field(i,  j+1,k+1)
     *      +       fx  * (1.d0-fy) *       fz  * field(i+1,j,  k+1)
     *      +       fx  *       fy  * (1.d0-fz) * field(i+1,j+1,k  )
      !
     *      +       fx  * (1.d0-fy) * (1.d0-fz) * field(i+1,j,  k  )
     *      + (1.d0-fx) *       fy  * (1.d0-fz) * field(i,  j+1,k  )
     *      + (1.d0-fx) * (1.d0-fy) *       fz  * field(i,  j,  k+1)
      !
     *      +       fx  *       fy  *       fz  * field(i+1,j+1,k+1)

        if (ltrace) then
           write(*,79) 'interp3d: ',i,j,k
           write(*,78) 'interp3d:f',fx,fy,fz
           write(*,78) 'interp3d: ',field(i,j,k),    field(i+1,j,k),
     .                              field(i,j+1,k),  field(i,j,k+1)
           write(*,78) 'interp3d: ',field(i+1,j+1,k),field(i+1,j,k+1),
     .                              field(i,j+1,k+1),field(i+1,j+1,k+1)
           write(*,78) 'interp3d: ',interp3d
           write(*,78) 'interp3d:A',
     *      + (1.d0-fx) * (1.d0-fy) * (1.d0-fz) * field(i,  j,  k  ),
      !
     *      + (1.d0-fx) *       fy  *       fz  * field(i,  j+1,k+1),
     *      +       fx  * (1.d0-fy) *       fz  * field(i+1,j,  k+1),
     *      +       fx  *       fy  * (1.d0-fz) * field(i+1,j+1,k  )
      !
           write(*,78) 'interp3d:B',
     *      +       fx  * (1.d0-fy) * (1.d0-fz) * field(i+1,j,  k  ),
     *      + (1.d0-fx) *       fy  * (1.d0-fz) * field(i,  j+1,k  ),
     *      + (1.d0-fx) * (1.d0-fy) *       fz  * field(i,  j,  k+1),
      !
     *      +       fx  *       fy  *       fz  * field(i+1,j+1,k+1)
 78        format(A,4G15.8)
 79        format(A,6I5)
        end if

      return
      end    ! END: interp3d


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc mystringlength:                                                            cc
cc                 Returns length of beginning non-space characters.          cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function mystringlength( the_string )
      implicit none
      character*(*) the_string
      integer       the_string_length

      the_string_length = len(the_string)

      mystringlength    = index(the_string,' ') - 1

      if (mystringlength .lt. 0) mystringlength = the_string_length

      return
      end        ! END: function mystringlength

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc                                                                            cc
cc  interp_cubic:                                                             cc
cc                                                                            cc
cc    Finds cubic interpolated point at x based on points 1,2,3,4             cc
cc    (Taken from had/trunk/src/amr/util.f 05/12/12)                          cc
cc                                                                            cc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      double precision function interp_cubic(y1,y2,y3,y4, x,x1,x2,x3,x4)
      implicit    none
      real(kind=8)      y1, y2, y3, y4, x, x1, x2, x3, x4
      real(kind=8)      xx1, xx2, xx3, xx4


      logical     ltrace
      parameter ( ltrace = .false. )

      xx1  = x  - x1
      xx2  = x  - x2
      xx3  = x  - x3
      xx4  = x  - x4

      interp_cubic =    xx2    *xx3    *xx4     * y1
     *                / ( (x1-x2)*(x1-x3)*(x1-x4) )
     *
     *              +   xx1    *xx3    *xx4     * y2
     *                / ( (x2-x1)*(x2-x3)*(x2-x4) )
     *
     *              +   xx1    *xx2    *xx4     * y3
     *                / ( (x3-x1)*(x3-x2)*(x3-x4) )
     *
     *              +   xx1    *xx2    *xx3     * y4
     *                / ( (x4-x1)*(x4-x2)*(x4-x3) )
      if (ltrace) then
         write(*,*) 'interp_cubic: y1 = ', y1
         write(*,*) 'interp_cubic: y2 = ', y2
         write(*,*) 'interp_cubic: y3 = ', y3
         write(*,*) 'interp_cubic: y4 = ', y4
         write(*,*) 'interp_cubic: x1 = ', x1
         write(*,*) 'interp_cubic: x2 = ', x2
         write(*,*) 'interp_cubic: x3 = ', x3
         write(*,*) 'interp_cubic: x4 = ', x4
c        write(*,*) 'interp_cubic: xx1 = ', xx1
c        write(*,*) 'interp_cubic: xx2 = ', xx2
c        write(*,*) 'interp_cubic: xx3 = ', xx3
c        write(*,*) 'interp_cubic: xx4 = ', xx4
         write(*,*) 'interp_cubic: x  = ', x
         write(*,*) 'interp_cubic: y  = ', interp_cubic
      end if


      return
      end    ! END function interp_cubic
