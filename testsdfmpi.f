c     program testsdf
      implicit none
      include 'mpif.h'
      integer  myid, numprocs, ierr

      character*18  APPEND2
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       MAXFILES,         numfiles
      parameter (   MAXFILES = 30 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 3000000 )

      real*8        times(MAXFILES), mintime
      character*32  name,   nameout, fnames(MAXFILES)

      character*128 sarg,         cnames
      integer       junk,         shape(3),
     .              mintime_i,    steps(MAXFILES),
     .              gf3_rc,       gf3_rct,
     .              rank,         nx, ny, nz,
     .              x0,y0,z0,     step,
     .              iargc,        i,j,k,l,
     .              maxi,maxj,maxk, mini,minj,mink, ax,ay,az
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              xval, yval, zval, lasttime,
     .              minx, maxx,
     .              miny, maxy, coordsbb(6),
     .              minz, maxz, dx,dy,dz, dxt, dyt, dzt,
     .              a, r, d, ex, ey, xc, yc, zc

      logical       first_datagrp, first_dataset

      !
      ! SDF Utilities found in libbbhutil.a
      !    (see bbhutil.ps for Matt's documentation)
      !
      integer       gft_out_full,    gft_read_rank,
     .              gft_read_full,   gft_read_shape,
     .              gft_out,         gft_out_brief, 
     .              gft_out_bbox
      external      gft_out_full,    gft_read_rank,
     .              gft_read_full,   gft_read_shape,
     .              gft_out,         gft_out_brief, 
     .              gft_out_bbox
      character(9) tmps, fname
      integer      mylen


      logical       trace,           trace2
      parameter   ( trace = .false.  )
      parameter   ( trace2= .false. )

      call MPI_INIT( ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, myid,     ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )

      nx            = 33
      ny            = 33
      nz            = 33

      shape(1)      = nx
      shape(2)      = ny
      shape(3)      = nz

      minx          = -10.d0
      miny          = -10.d0
      minz          = -10.d0

      maxx          = +10.d0
      maxy          = +10.d0
      maxz          = +10.d0

      a             = 1.d0
      ex            = 1.d0
      ey            = 1.d0
      ey            = 0.d0
      r             = 3.d0
      d             = 3.d0
      xc            = 0.d0
      yc            = 0.d0
      zc            = 0.d0

      dx            = (maxx-minx) / ( shape(1) - 1)
      dy            = (maxy-miny) / ( shape(2) - 1)
      dz            = (maxz-minz) / ( shape(3) - 1)

      do k = 1, shape(3)
      do j = 1, shape(2)
      do i = 1, shape(1)
         coords(                      i) = minx + dx*(i-1)
         coords(           j + shape(1)) = miny + dy*(j-1)
         coords(k + shape(2) + shape(1)) = minz + dz*(k-1)
      end do
      end do
      end do
      coordsbb(1) = minx
      coordsbb(2) = maxx
      coordsbb(3) = miny
      coordsbb(4) = maxy
      coordsbb(5) = minz
      coordsbb(6) = maxz

      cnames  = 'x|y|z'
      rank    = 3

      call  gaussian3d( data, dx, minx, miny, minz,nx,ny,nz,
     *           a,ex,ey,r,d,xc,yc,zc)



         call int2str(myid,tmps)
         fname           = tmps
         mylen           = len_trim(fname)

      do i = 1, 30
         time = 0.1d0*(i-1)
        fname(mylen+1:) = 'test_out'
      gf3_rc = gft_out(      fname,       time,shape,
     .                                  rank,           data)
c        fname(mylen+1:) = 'test_brief'
c     gf3_rc = gft_out_brief(fname,      ,time,shape,
c    .                                  rank,           data)
         fname(mylen+1:) = 'test_bbox'
      gf3_rc = gft_out_bbox( fname,       time,shape,
     .                                  rank, coordsbb, data)
         fname(mylen+1:) = 'test_full'
      gf3_rc = gft_out_full( fname,      time,shape,cnames,
     .                                  rank, coords,   data)

      end do
		
      call MPI_FINALIZE(ierr)
      end
c
c     This is to see the difference between the various calls.
c         -rw-rw-r--    1 steve    steve     8630880 Jul 23 14:15 test_bbox.sdf
c         -rw-rw-r--    1 steve    steve     8630910 Jul 23 14:15 test_brief.sdf
c         -rw-rw-r--    1 steve    steve     8653200 Jul 23 14:15 test_full.sdf
c         -rw-rw-r--    1 steve    steve     8630850 Jul 23 14:15 test_out.sdf
c
c     Size difference is pretty small, not sure if that's because the call to gft_out_full()
c     recognizes that the coords are uniform and thus only stores the bounding box or what.   
c     Not sure about speed, but seems gft_out() and gft_out_brief() are the same, while
c     gft_out_full() and gft_out_bbox() are effectively the same in this context.
c
