c     program joinsdf
      implicit none

      character*18  APPEND2
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d38)
      integer       MAXFILES,         numfiles
      parameter (   MAXFILES = 1025 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 13000000 )

      real*8        times(MAXFILES), mintime
      character*32  name,   nameout, fnames(MAXFILES)

      character*128 sarg,         cnames
      integer       junk,         shape(3),
     .              mintime_i,    steps(MAXFILES),
     .              gf3_rc,       gf3_rct,
     .              rank,         nx, ny, nz,
     .              x0,y0,z0,     step,
     .              iargc,        i,j,k,l,
     .              maxi,maxj,maxk, mini,minj,mink, ax,ay,az
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              xval, yval, zval, lasttime,
     .              minx, maxx,
     .              miny, maxy,
     .              minz, maxz, dx,dy,dz, dxt, dyt, dzt

      logical       first_datagrp, first_dataset

      !
      ! SDF Utilities found in libbbhutil.a
      !    (see bbhutil.ps for Matt's documentation)
      !
      integer       gft_out_full,    gft_read_rank,
     .              gft_read_full,   gft_read_shape
      external      gft_out_full,    gft_read_rank,
     .              gft_read_full,   gft_read_shape

      logical       trace,           trace2
      parameter   ( trace = .false.  )
      parameter   ( trace2= .false. )

       !## usage statement
       numfiles = iargc()
       !
       ! Even if one file, still do it (so that batch scripts 
       ! don't have to consider such cases specially):
       !
       !if (numfiles .lt. 2 .or. numfiles .gt. MAXFILES) then
       if (numfiles .lt. 1 .or. numfiles .gt. MAXFILES) then
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       joinsdf  <sdf filename1> <sdf fname2>',
     *                ' [<fname3> <finame4> ....]'
          write(*,*) ''
          write(*,*) '         Produces a new SDF file with all data'
          write(*,*) '         from the input files so that the data'
          write(*,*) '         occur in chronological order.'
          write(*,*) ''
          write(*,*) '  Notes: Assumes input SDF files are so ordered.'
          write(*,*) ''
          write(*,*) '  Notes: SDF files can apparently be catenated '
          write(*,*) '         together, but this utility keeps them '
          write(*,*) '         in chrono order which is important'
          write(*,*) '         for certain utilities.'
          write(*,*) ''
          write(*,*) '  Notes: Maximum number of files: ',MAXFILES
          write(*,*) ''
          stop
       end if

      !
      ! Read in first data set, then for all others
      ! slice along the coordinate of the z-index chosen:
      !
      first_datagrp = .true.
      first_dataset = .true.
      mintime       = LARGENUMBER
      shape(1)      = 1
      shape(2)      = 1
      shape(3)      = 1

      call sload(cnames,' ')
      do i = 1 , numfiles
         steps(i)    = 0
         call sload(fnames(i),' ')
         fnames(i)   = sarg(i,'2d_Phi_3.sdf')
      end do


      !
      ! Setup what will be appended to file name:
      !   (e.g. chi.sdf  ---> chi_i=11.sdf )
      !
      call sload(APPEND2,' ')
      APPEND2(1:) = '_joined'

      !
      ! Create output file name
      ! based on input filename and user option:
      !
      name = fnames(1)
      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND2

      if (trace) then
         write(*,*) '*******************************'
         do i = 1, numfiles
            write(*,*) 'joinsdf: Reading in file ',fnames(i)
         end do
         write(*,*) 'joinsdf: Outputting w/ :   ',APPEND2
         write(*,*) '          Outputing to:     ',nameout
         write(*,*) '*******************************'
      end if
      write(*,*) '          Outputing to:     ',nameout

      !
      ! Get rank:
      !
      if (trace2) then
          write(*,*) 'chopsdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step   = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'chopsdf: Unable to read ', name
         write(*,*) 'chopsdf: gf3_rc = ', gf3_rc
         write(*,*) 'chopsdf: Quitting....'
         stop
      end if

      !####################################################################
      !
      ! Step into each file once:
      !
      !####################################################################
      gf3_rc = 2147483647
      !gf3_rc = int(LARGENUMBER)
      do i = 1 , numfiles
         steps(i) = steps(i) + 1
         gf3_rct  = gft_read_shape( fnames(i),    steps(i),   shape)
         call checkmem( shape(1),shape(2),shape(3),MAXPOINTS,MAXCOORDS)
         gf3_rct  = gft_read_full(  fnames(i),    steps(i),   shape,
     .                           cnames,   rank,   times(i),
     .                           coords,   data           )
         if (trace2) write(*,*) '  gf3_rct:  ',gf3_rct
         !
         ! As long as one file still has data to be "join"ed:
         !
         if (gf3_rct.eq.1) gf3_rc = 1
         if (times(i) .lt. mintime) then
             mintime   = times(i)
             mintime_i = i
         end if
         if (trace2) write(*,*) '  Read for: ',i,times(i),fnames(i)
      end do
      if (trace) then
         write(*,*) ' Starting to join files together:'
         write(*,*) '                      mintime = ',mintime
         write(*,*) '                    mintime_i = ',mintime_i
         write(*,*) '                    steps()   = ',steps(mintime_i)
         write(*,*) '                    fnames()  = ',fnames(mintime_i)
         write(*,*) '................................'
      end if

      !---------------------------
      ! Loop for all data:
      !---------------------------
c88   do while (gf3_rc.eq.1)
      do while (mintime.ne.LARGENUMBER)
         !
         ! Read in data for file with minimum time:
         !
 30      gf3_rc = gft_read_full( fnames(mintime_i),steps(mintime_i),
     .                         shape,cnames,   rank,   times(mintime_i),
     .                              coords,   data           )
         if (trace2) write(*,*) '  Read gf3_rc :  ',gf3_rc
         !
         ! output to joined file:
         !
         if (trace) write(*,*) '  Output at:',mintime_i,mintime
 50      gf3_rc = gft_out_full(nameout, mintime,shape,cnames,
     .                                  rank, coords, data)
         if (trace2) write(*,*) '  Out gf3_rc :  ',gf3_rc
         !
         ! Read in next data set from this file:
         !
         steps(mintime_i) = steps(mintime_i) + 1
         gf3_rc = gft_read_shape( fnames(mintime_i),steps(mintime_i),
     .                    shape)
         call checkmem( shape(1),shape(2),shape(3),MAXPOINTS,MAXCOORDS)
         gf3_rc = gft_read_full(  fnames(mintime_i),steps(mintime_i),
     .                    shape, cnames,   rank,   times(mintime_i),
     .                              coords,   data           )
         if (trace2) write(*,*) '  Read2 gf3_rc :  ',gf3_rc
         !
         ! Continue with thie file or move on?
         !
         if (gf3_rc .ne. 1) then
            if (trace) write(*,*) '  Done with file: ',fnames(mintime_i)
            times(mintime_i) = LARGENUMBER
         else if (times(mintime_i).eq.mintime) then
            if (trace) write(*,*) '  Continue with file:',
     .                     fnames(mintime_i)
            goto 50
         end if
         !
         ! Find next file in chrono order:
         !
         call findmin(mintime,mintime_i,times,numfiles)
         if (trace) then
            write(*,*) ' Continuing  join files together:'
            write(*,*) '                      mintime = ',mintime
            write(*,*) '                    mintime_i = ',mintime_i
         write(*,*) '                    steps()   = ',steps(mintime_i)
         end if
      end do
		
      end
