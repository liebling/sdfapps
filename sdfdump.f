c     program intsdf
c
c     integrate grid function
c
      implicit none

      character*32  name, nameout, tcnames(3)
      character*128 sarg, cline, cnames
      integer       mxliv
      parameter   ( mxliv = 10000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, x,y,z, trank
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink, split,split_axis
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      parameter   ( nx =100, ny=100, nz=100)
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 40 000,  MAXPOINTS = 1 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time, 
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS)
      real*8        l2norm, maxdata, mindata,temp, integral, dx,dy,dz
      real*8        integral1, integral2
      logical       trace,           trace2
      parameter   ( trace = .false. )
c     parameter   ( trace = .true.  )
      parameter   ( trace2= .false. )
c     parameter   ( trace2= .true. )

c      ### usage statement
       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* Integrates grid function                      *'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       sdfdump  <sdf filename> <outputvector>'
          write(*,*) ''
          !write(*,*) ' NOTE: only works for 2-d data sets currently.'
          write(*,*) '                *****************'
          stop
       end if

c      ### read in arguments
      name   = sarg(1,'2d_Phi_3.sdf')
      cline  = sarg(2,'*-*')
      split  = i4arg(3,1)
      split_axis  = i4arg(4,1)

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'sdfdump: Reading in file ',name
         write(*,*) '*******************************'
      end if
c      ### set up output vector
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
          !write(*,*) 'sdfdump: using *-* as index vector'
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if


c     ### get rank
      if (trace2) then
          write(*,*) 'sdfdump:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'sdfdump: Unable to read ', name
         write(*,*) 'sdfdump: Quitting....'
         goto 800
      end if
      if (trace2) then
          write(*,*) 'sdfdump:  Finished reading in sdf file...'
          write(*,*) 'sdfdump:  rank = ', rank
      end if


      step = 0
      do while (.true.)
         step = step + 1
 		   if (trace2) then
            write(*,*) '..........step ', step
            write(*,*) '..........liv  ', liv 
            write(*,*) '..........iv(1)', iv(1) 
            if ( ivindx(iv,liv,step) .gt. 0 ) 
     .               write(*,*) '          step in index vector'
            write(*,*) '       reading in sdf'
         end if
         gf3_rc = gft_read_full( name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then
            if (   (rank.eq.1)
     .                .and.
     .         ((shape(1).gt.MAXPOINTS).or.(shape(1).gt.MAXCOORDS))
     .          ) then
                write(*,*) '1d data set too big for mem. allocated'
                stop
            else if ( (rank.eq.2)
     .                .and.
     .              (( (shape(1)*shape(2)).gt.MAXPOINTS)
     .                        .or.
     .               ( (shape(1)+shape(2)).gt.MAXCOORDS))
     .          ) then
                write(*,*) '2d data set too big for mem. allocated'
                write(*,*) 'Data set has ', shape(1)*shape(2), ' points'
                write(*,*) 'Data set has ', shape(1)+shape(2), ' coords'
                write(*,*) 'Data set has dimension ', shape(1),shape(2)
                write(*,*) 'Max points, coords', MAXPOINTS, MAXCOORDS
                stop
            else if ( (rank.eq.3)
     .                .and.
     .              ((shape(1)*shape(2)*shape(3).gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)+shape(2)+shape(3)).gt.MAXCOORDS))
     .          ) then
                write(*,*) '3d data set too big for mem. allocated'
                stop
            end if

            k          = INDEX(cnames, '|')
            tcnames(1) = cnames(:k-1)
            j          = k+1
            do i = 2, rank-1
               k          = INDEX(cnames(j:), '|') + j - 1
               tcnames(i) = cnames(j:k-1)
               j          = k+1
            end do
            tcnames(rank) = cnames(j:)

            if (trace2) then
               do i = 1, rank
                  write(*,*) tcnames(i), i
               end do
            end if 

            if (trace2) then
               write(*,*) '..........successfully read'
               write(*,*) '..........liv  ', liv 
               write(*,*) '..........iv(1)', iv(1) 
               write(*,*) '           step:',step
               write(*,*) '           time:',time
               write(*,*) '           rank:',rank
               write(*,*) '           shape    coord name:'
               do i = 1, rank
				       write(*,*) '                 ',shape(i),
     .                  '   ',tcnames(i)
               end do
            end if
            if ( ivindx(iv,liv,step) .gt. 0 ) then
		          if (trace2) 
     .               write(*,*) '..........step in index vector'

                if (rank.eq.2) then
c               ################################
c               ###  2 Dimensional sdf files ###
c               ################################
 10                format(1p,3E16.8)
                write(*,*) 'ZONE F=POINT, I=',shape(1),' , J=',shape(2)
                         do j = 1, shape(2)
                   do i = 1, shape(1)
                            write(*,10) coords(i), coords(shape(1)+j),
     .                           data( (j-1)*shape(1)+i )
                         end do
                   end do
                else if  (rank.eq.3) then
c               ################################
c               ###  3 Dimensional sdf files ###
c               ################################
 20                format(1p,4E16.8)
                   write(*,30) shape(1),shape(2),shape(3)
 30                format('ZONE F=POINT, I=',I4,', J=',I4,
     .               ' , K=',I4)
                   do k = 1, shape(3)
                   do j = 1, shape(2)
                   do i = 1, shape(1)
                     write(*,20) coords(i), coords(shape(1)+j),
     .                           coords(shape(2)+shape(1)+k),
     .               data( (k-1)*shape(2)*shape(1)+(j-1)*shape(1)+i )
                   end do
                   end do
                   end do
                else if  (rank.eq.1) then
c               ################################
c               ###  1 Dimensional sdf files ###
c               ################################
c                  agrees with "ser" to 10e-16
                   integral = 0.0d0
 40                format('Time =' F16.8)
                   write(*,40) time
                   do i = 1, shape(1)
                     write(*,20) coords(i), data(i)
                   end do
                   !write(*,*) time, integral
                end if
            else
              if (trace) write(*,*) 'sdfdump: not in index vector',step
            end if
 	   else 
             if (trace2) 
     .           write(*,*) "sdfdump: end of file ",
     .                       name
             goto 800
         end if
      end do
		
 800  continue
      end





