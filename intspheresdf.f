c     program intrhosdf
c
c     integrate grid function
c
      implicit none

      character*32  name, nameout, tcnames(3)
      character*128 sarg, cline, cnames
      integer       mxliv
      parameter   ( mxliv = 10000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, x,y,z, trank
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink, split
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 10 000,  MAXPOINTS = 3 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS)

      real*8        l2norm, maxdata, mindata,temp, integral, dx,dy,dz
      real*8        integralu, integrald, phi, theta, dphi, dtheta,dA
      real*8        psi, r8arg
      external           r8arg
      integer       istart, iend
      logical       zerotopi, zeroto2pi
      logical       double_equal
      external     double_equal
      real*8       cpi
      parameter  ( cpi  =  3.14159 26535 89793 23846 26433 83279)
      logical       trace,           trace2
      parameter   ( trace = .false. )
c     parameter   ( trace = .true.  )
      parameter   ( trace2= .false. )
c     parameter   ( trace2= .true. )

c      ### usage statement
       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* Integrates grid function  f(theta,phi)        *'
          write(*,*) '*     int f sin(theta) dphi dtheta              *'
          write(*,*) '*  where theta varies 0->2pi                    *'
          write(*,*) '*    and phi   varies 0-> pi                    *'
          write(*,*) '*                                               *'
          write(*,*) '* If applied to 1D data, assumes f(theta)       *'
          write(*,*) '*     [int f sin(theta) dtheta]*2pi             *'
          write(*,*) '* If the angle psi is given a nonzero default   *'
          write(*,*) '*   value, then the angle theta is transformed  *'
          write(*,*) '*   to theta+psi and only integrated over 0->pi *'
          write(*,*) '*                                               *'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '    intspheresdf  <sdf filename> <outputvector> '
          write(*,*) '          [ <angle psi=0> ]'
          write(*,*) ''
          write(*,*) '                *****************'
          write(*,*) '   Outputs three integrals, the one over the '
          write(*,*) '   upper hemisphere, lower hemisphere, and total'
          stop
       end if


c      ### read in arguments
      name   = sarg(1,'2d_Phi_3.sdf')
      cline  = sarg(2,'*-*')
      !split  = i4arg(3,1)
      split  = 1
      psi    = r8arg(3,0.d0)
      write(*,*) 'angle psi = ',psi

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'intspheresdf: Reading in file ',name
         write(*,*) 'intspheresdf: Integrating times ', cline
         write(*,*) 'intspheresdf: For 2d, split=',split
         write(*,*) '*******************************'
      end if
c      ### set up output vector
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
c         write(*,*) 'intspheresdf: using *-* as index vector'
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

c     if (trace2) call ivdump(iv,liv,'index vector',6)

c     ### get rank
      if (trace2) then
          write(*,*) 'intspheresdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'intspheresdf: Unable to read ', name
         write(*,*) 'intspheresdf: Quitting....'
         goto 800
      end if
      if (trace2) then
          write(*,*) 'intspheresdf:  Finished reading in sdf file...'
          write(*,*) 'intspheresdf:  rank = ', rank
      end if


      step = 0
      do while (.true.)
         step = step + 1
 		   if (trace2) then
            write(*,*) '..........step ', step
            write(*,*) '..........liv  ', liv 
            write(*,*) '..........iv(1)', iv(1) 
            if ( ivindx(iv,liv,step) .gt. 0 ) 
     .               write(*,*) '          step in index vector'
            write(*,*) '       reading in sdf'
         end if
         gf3_rc = gft_read_full( name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then
            if (   (rank.eq.1)
     .                .and.
     .              (( (shape(1)         ).gt.MAXPOINTS )
     .                        .or.
     .               ( (shape(1)         ).gt.MAXCOORDS) )
     .          ) then
                write(*,*) '1d data set too big for mem. allocated'
                write(*,*) 'Data set has ', shape(1)         , ' points'
                write(*,*) 'Data set has ', shape(1)         , ' coords'
                write(*,*) 'Data set has dimension ', shape(1)         
                write(*,*) 'Max points, coords', MAXPOINTS, MAXCOORDS
                stop
            else if ( (rank.eq.2)
     .                .and.
     .              (( (shape(1)*shape(2)).gt.MAXPOINTS )
     .                        .or.
     .               ( (shape(1)+shape(2)).gt.MAXCOORDS) )
     .          ) then
                write(*,*) '2d data set too big for mem. allocated'
                write(*,*) 'Data set has ', shape(1)*shape(2), ' points'
                write(*,*) 'Data set has ', shape(1)+shape(2), ' coords'
                write(*,*) 'Data set has dimension ', shape(1),shape(2)
                write(*,*) 'Max points, coords', MAXPOINTS, MAXCOORDS
                stop
            else if ( (rank.eq.3) ) then
                write(*,*) 'Expecting 2D data. Quitting....'
                stop
            end if
            k          = INDEX(cnames, '|')
            tcnames(1) = cnames(:k-1)
            j          = k+1
            do i = 2, rank-1
               k          = INDEX(cnames(j:), '|') + j - 1
               tcnames(i) = cnames(j:k-1)
               j          = k+1
            end do
            tcnames(rank) = cnames(j:)
            if (trace2) then
               do i = 1, rank
                  write(*,*) tcnames(i), i
               end do
            end if
            if (trace2) then
               write(*,*) '..........successfully read'
               write(*,*) '..........liv  ', liv 
               write(*,*) '..........iv(1)', iv(1) 
               write(*,*) '           step:',step
               write(*,*) '           time:',time
               write(*,*) '           rank:',rank
               write(*,*) '          split:',split
               write(*,*) '           shape    coord name:'
               do i = 1, rank
				       write(*,*) '                 ',shape(i),
     .                  '   ',tcnames(i)
               end do
            end if
            if ( ivindx(iv,liv,step) .gt. 0 ) then
		          if(trace2) write(*,*) '..........step in index vector'
                if (rank.eq.2) then
c               ################################
c               ###  2 Dimensional sdf files ###
c               ################################
                   integral = 0.0d0
                   integralu= 0.0d0
                   integrald= 0.0d0
                   if(.false.) then
                    !'*************************************************'
                    !'* Integrates grid function  f(theta,phi)        *'
                    !'*     int f sin(theta) dphi dtheta              *'
                    !'*  where theta varies 0->2pi                    *'
                    !'*    and phi   varies 0-> pi                    *'
                    !'*************************************************'
                      ! theta: 0->2pi (azimuth)
                      write(*,*) 'theta: ',shape(1),coords(1),
     .                                     coords(shape(1))
                      ! phi:   0->pi (polar angle)
                      write(*,*) 'phi:   ',shape(2),coords(shape(1)+1),
     .                             coords(shape(1)+shape(2))
                   end if
                   do j = 2, shape(2)
                      do i = 2, shape(1)
                         dtheta   =   coords(i)
     .                              - coords(i-1)
                         theta    = ( coords(i)+coords(i-1) )*0.5d0
                         dphi     =   coords( shape(1) + j  )
     .                              - coords( shape(1) + j-1)
                         phi      = ( coords(shape(1)+j)
     .                               +coords(shape(1)+j-1))*0.5d0
                         dA       = dphi*dtheta*dsin(phi)
                         !write(*,*) 'dA = ',dA
                         if (.false.) then
                            !write(*,*) '........just testing........'
                            ! This is just a test which should get 4pi:
                            !  (12.5663706)
                            integral = integral + 0.25d0*(
     .                            dsin(coords(shape(1)+j  ))
     .                          + dsin(coords(shape(1)+j-1))
     .                          + dsin(coords(shape(1)+j  ))
     .                          + dsin(coords(shape(1)+j-1))
     .                                        )*dphi*dtheta
                            if (phi.lt.0.5d0*cpi) then
                            integralu= integralu+ 0.25d0*(
     .                            dsin(coords(shape(1)+j  ))
     .                          + dsin(coords(shape(1)+j-1))
     .                          + dsin(coords(shape(1)+j  ))
     .                          + dsin(coords(shape(1)+j-1))
     .                                        )*dphi*dtheta
                            else
                            integrald= integrald+ 0.25d0*(
     .                            dsin(coords(shape(1)+j  ))
     .                          + dsin(coords(shape(1)+j-1))
     .                          + dsin(coords(shape(1)+j  ))
     .                          + dsin(coords(shape(1)+j-1))
     .                                        )*dphi*dtheta
                            end if
                         else
                            ! This is the proper integral:
                            integral = integral + 0.25d0*(
     .                            dsin(coords(shape(1)+j  ))
     .                                       *data( (j-1)*shape(1)+i   )
     .                          + dsin(coords(shape(1)+j-1))
     .                                       *data( (j-1)*shape(1)+i-1 )
     .                          + dsin(coords(shape(1)+j  ))
     .                                       *data( (j-2)*shape(1)+i   )
     .                          + dsin(coords(shape(1)+j-1))
     .                                       *data( (j-2)*shape(1)+i-1 )
     .                                        )*dphi*dtheta
                            if (phi.lt.0.5d0*cpi) then
                            integralu= integralu+ 0.25d0*(
     .                            dsin(coords(shape(1)+j  ))
     .                                       *data( (j-1)*shape(1)+i   )
     .                          + dsin(coords(shape(1)+j-1))
     .                                       *data( (j-1)*shape(1)+i-1 )
     .                          + dsin(coords(shape(1)+j  ))
     .                                       *data( (j-2)*shape(1)+i   )
     .                          + dsin(coords(shape(1)+j-1))
     .                                       *data( (j-2)*shape(1)+i-1 )
     .                                        )*dphi*dtheta
                            else
                            integrald= integrald+ 0.25d0*(
     .                            dsin(coords(shape(1)+j  ))
     .                                       *data( (j-1)*shape(1)+i   )
     .                          + dsin(coords(shape(1)+j-1))
     .                                       *data( (j-2)*shape(1)+i-1 )
     .                          + dsin(coords(shape(1)+j  ))
     .                                       *data( (j-1)*shape(1)+i   )
     .                          + dsin(coords(shape(1)+j-1))
     .                                       *data( (j-2)*shape(1)+i-1 )
     .                                        )*dphi*dtheta
                            end if
                         end if
                      end do
                   end do
                   write(*,90) time, integralu, integrald, integral
  90               format(G16.9,'  ',G16.9,'  ',G16.9,'  ',G16.9)
                elseif (rank.eq.1) then
c               ################################
c               ###  1 Dimensional sdf files ###
c               ###  NB: I suspect the naming convention used for 2D
c               ###      is opposite of that I amd using for 1D.    
c               ################################
                   integral = 0.0d0
                   integralu= 0.0d0
                   integrald= 0.0d0
                   !'*************************************************'
                   !'* Integrates grid function  f(theta)            *'
                   !'*     int f sin(theta) dtheta * 2pi             *'
                   !'*  where theta varies 0-> pi                    *'
                   !'*************************************************'
                   if (double_equal(psi,0.d0)) then
                      do i = 2, shape(1)
                         dtheta   =   coords(i) - coords(i-1)
                         theta    = ( coords(i)+coords(i-1) )*0.5d0
                         dphi     =   2.d0*cpi
                         zerotopi = double_equal(coords(i),0.d0) .and.
     .                              double_equal(coords(shape(1)),cpi)
                         zeroto2pi = double_equal(coords(i),0.d0) .and.
     .                         double_equal(coords(shape(1)),2.d0*cpi)
                         if (.false.) then
                            !write(*,*) '........just testing........'
                            ! This is just a test which should get 4pi:
                            !  (12.5663706)
                            integral = integral + 0.5d0*(
     .                            dsin(coords(  i))
     .                          + dsin(coords(i-1))
     .                                        )*dphi*dtheta
                            if (theta.lt.0.5d0*cpi) then
                            integralu= integralu+ 0.5d0*(
     .                            dsin(coords(  i))
     .                          + dsin(coords(i-1))
     .                                        )*dphi*dtheta
                            else
                            integrald= integrald+ 0.5d0*(
     .                            dsin(coords(  i))
     .                          + dsin(coords(i-1))
     .                                        )*dphi*dtheta
                            end if
                         else
                            ! This is the proper integral:
                            integral = integral + 0.5d0*(
     .                            dsin(coords(i  ))*data(i  )
     .                          + dsin(coords(i-1))*data(i-1)
     .                                        )*dphi*dtheta
                            if (theta.lt.0.5d0*cpi) then
                            integralu= integralu+ 0.5d0*(
     .                            dsin(coords(i  ))*data(i  )
     .                          + dsin(coords(i-1))*data(i-1)
     .                                        )*dphi*dtheta
                            else
                            integrald= integrald+ 0.5d0*(
     .                            dsin(coords(i  ))*data(i  )
     .                          + dsin(coords(i-1))*data(i-1)
     .                                        )*dphi*dtheta
                            end if
                         end if
                      end do
                   else
                      ! (1) Transform theta->theta+psi
                      ! (2) Only integrate for transformed theta in [0,pi]
                      i = 0
  55                  i = i + 1
                      theta = coords(i) + psi
                      if (theta.lt.0) then
                         if (i.lt.shape(1)) then
                            goto 55
                         else
                            ! nothing to integrate
                            istart = shape(1)+1
                         end if
                      else
                        ! begin here
                        istart = i
                      end if
                      write(*,*) 'istart: , theta:',istart,theta
                      i = i - 1
  56                  i = i + 1
                      theta = coords(i) + psi
                      if (theta.lt.cpi) then
                         if (i.lt.shape(1)) then
                            goto 56
                         else
                            ! integrate to the end
                            iend = shape(1)
                         end if
                      else
                        ! end here
                        iend = i
                      end if
                      write(*,*) 'iend:     theta:',iend,theta
                      write(*,*) 'istart/end: ',istart, iend
                      do i = istart, iend
                         dtheta   =   coords(i) - coords(i-1)
                         theta    = ( coords(i)+coords(i-1) )*0.5d0
                         theta    = theta + psi
                         dphi     =   2.d0*cpi
                         zerotopi = double_equal(coords(i),0.d0) .and.
     .                              double_equal(coords(shape(1)),cpi)
                         zeroto2pi = double_equal(coords(i),0.d0) .and.
     .                         double_equal(coords(shape(1)),2.d0*cpi)
                            integral = integral + 0.5d0*(
     .                            dsin(coords(i  )+psi)*data(i  )
     .                          + dsin(coords(i-1)+psi)*data(i-1)
     .                                        )*dphi*dtheta
                            if (theta.lt.0.5d0*cpi) then
                            integralu= integralu+ 0.5d0*(
     .                            dsin(coords(i  )+psi)*data(i  )
     .                          + dsin(coords(i-1)+psi)*data(i-1)
     .                                        )*dphi*dtheta
                            else
                            integrald= integrald+ 0.5d0*(
     .                            dsin(coords(i  )+psi)*data(i  )
     .                          + dsin(coords(i-1)+psi)*data(i-1)
     .                                        )*dphi*dtheta
                            end if
                      end do
                   end if
                   write(*,90) time, integralu, integrald, integral
                endif
            else
              if(trace)write(*,*)'intspheresdf:not in index vector',step
            end if
         else 
             if (trace2) 
     .           write(*,*) "intspheresdf: end of file ",
     .                       name
             goto 800
         end if
      end do
		
 800  continue
      end





