c     program procsdf
      implicit none

c     ## name of output file
      character*2   APPEND
		parameter   ( APPEND = '_p')

      character*32  name,   nameout, tcnames(3)
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 500 000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,gft_read_shape,
     .              iv(mxliv), liv, key, x,y,z, trank,vsrc,vsxynt,
     .              x0,y0,z0
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 50002,  MAXPOINTS = 34 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS)

      integer       numt
      real*8        mint, maxt, deltat, r8arg
      real*8        l2norm, maxdata, mindata,temp, 
     .                      maxdata_rho, maxdata_z,
     .                      mindata_rho, mindata_z
      logical       trace,           trace2
      parameter   ( trace = .false. )
c     parameter   ( trace = .true.  )
      parameter   ( trace2= .false. )
c     parameter   ( trace2= .true. )

c      ### usage statement
       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* copytsdf: copies a single data set to times   *'
          write(*,*) '*************************************************'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       copytsdf  <sdf filename>'
          write(*,*) '                   <min time> <max time> <numt> '
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) ''
          stop
       end if

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
      end do

c      ### read in arguments
      name   = sarg(1,'2d_Phi_3.sdf')
      mint   = r8arg(2,0.0)
      maxt   = r8arg(3,1.0)
      numt   = i4arg(4,10)
      if (numt<1) then
         write(*,*) 'copytsdf: numt must be at least 1: ',numt
         stop
      end if
      deltat = (maxt-mint)/(numt-1.0)
      stride = 1

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'procsdf: Reading in file ',name
         write(*,*) 'procsdf: Outputting times ', cline
         write(*,*) 'procsdf: x=',x0
         write(*,*) 'procsdf: y=',y0
         write(*,*) 'procsdf: z=',z0
         write(*,*) '*******************************'
      end if
c      ### set up output vector
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
c         write(*,*) 'procsdf: using *-* as index vector'
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

c     if (trace2) call ivdump(iv,liv,'index vector',6)


c     ### get rank
      if (trace2) then
          write(*,*) 'procsdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'procsdf: Unable to read ', name
         write(*,*) 'procsdf: gf3_rc = ', gf3_rc
         write(*,*) 'procsdf: Quitting....'
         goto 800
      end if
      if (trace2) then
          write(*,*) 'procsdf:  Finished reading in sdf file...'
          write(*,*) 'procsdf:  rank = ', rank
      end if

      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      if (trace2) write(*,*) '    junk = ', junk
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND
      if (trace2) write(*,*) ' outputing to: ', nameout

      step = 1
      if (trace2) then
            write(*,*) '..........step ', step
            write(*,*) '..........liv  ', liv 
            write(*,*) '..........iv(1)', iv(1) 
            if ( ivindx(iv,liv,step) .gt. 0 ) 
     .               write(*,*) '          step in index vector'
            write(*,*) '       reading in sdf'
      end if
         gf3_rc = gft_read_shape( name,     step,   shape)
         call checkmem(shape(1),shape(2),shape(3),MAXPOINTS,MAXCOORDS)
         gf3_rc = gft_read_full(  name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
       if (gf3_rc .eq. 1) then
          if (trace2) then
               write(*,*) 'copytsdf: successfully read'
          end if
          do i = 1, numt
                   time = mint + (i-1)*deltat
                   gf3_rc = gft_out_full(nameout, time, shape, cnames, 
     .                                     rank, coords, data)
                   if (gf3_rc .eq. 1) then
                            write(*,*) "procsdf: successful"
                   else
                           write(*,*) "procsdf: unsuccessful writing",
     .                             " file: ",nameout
                   end if
          end do
       else 
          if (trace2) then
               write(*,*) 'copytsdf: NOT successfully read'
          end if
       end if


 800   continue
       end
