c     program sphereharm
      implicit none

      character*32  name,   nameout, tcnames(3)
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 50000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,gft_read_shape,
     .              iv(mxliv), liv, key, x,y,z, trank,vsrc,vsxynt,
     .              x0,y0,z0
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 25 000 000 )
      real*8        idata(MAXPOINTS), coords(MAXCOORDS), time,
     .              rdata(MAXPOINTS),tcoords(MAXCOORDS),
     .              mdata(MAXPOINTS),jdata(MAXPOINTS)

      real*8        l2norm, maxdata, mindata,temp, 
     .                      maxdata_rho, maxdata_z,
     .                      mindata_rho, mindata_z
      logical       trace,           trace2
      !
      !
      !
      !
      character*32  nameoutr, nameouti
      integer       ntheta, nphi, index
      real*8        cpi, dtheta, dphi, rvalue, r8arg
      real*8        theta, phi, tmp
      parameter   ( cpi  =  3.14159265d0 )
      real*8                              rY00,
     *                             rY1m1, rY10,  rY1p1,
     *                      rY2m2, rY2m1, rY20,  rY2p1,  rY2p2,
     *              rY3m3,  rY3m2, rY3m1, rY30,  rY3p1,  rY3p2, rY3p3
      real*8                              iY00,
     *                             iY1m1, iY10,  iY1p1,
     *                      iY2m2, iY2m1, iY20,  iY2p1,  iY2p2,
     *              iY3m3,  iY3m2, iY3m1, iY30,  iY3p1,  iY3p2, iY3p3
      real*8                               a00,
     *                              a1m1,  a10,  a1p1,
     *                       a2m2,  a2m1,  a20,  a2p1,  a2p2,
     *               a3m3,   a3m2,  a3m1,  a30,  a3p1,  a3p2, a3p3
      !
      !
      !

      parameter   ( trace = .false. )
c     parameter   ( trace = .true.  )
      parameter   ( trace2= .false. )
c     parameter   ( trace2= .true. )

c      ### usage statement
       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* Generate 2D SDFs of linear combinations of    *'
          write(*,*) '*                spherical harmonics.           *'
          write(*,*) '*************************************************'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       sphereharm  <nphi> <ntheta> <r-value>'
          write(*,*) ''
          write(*,*) '                *****************'
          stop
       end if

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
      end do

c      ### read in arguments
      nphi   = i4arg(1,-1)
      ntheta = i4arg(2,-1)
      rvalue = r8arg(3,-1.d0)

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'sphereharm: nphi   = ',nphi
         write(*,*) 'sphereharm: ntheta = ',ntheta
         write(*,*) 'sphereharm: rvalue = ',rvalue
         write(*,*) '*******************************'
      end if
      nameoutr  = 'rap4'
      nameouti  = 'iap4'
      time      = 0.d0
      tshape(1) = nphi
      tshape(2) = ntheta
      tnames    = 'phi|theta'
      trank     = 2
      !
      dphi      = (2.0*cPi - 0.d0 ) / (nphi   - 1)
      dtheta    = (    cPi - 0.d0 ) / (ntheta - 1)
      !
      !
      ! Setup Coordinates:
      !
      do j = 1, nphi
         phi        = (j-1)*dphi
         tcoords(j) = phi
      end do
      do k = 1, ntheta
         theta             = (k-1)*dtheta
         tcoords(nphi+k)   = theta
      end do
      !
      ! Setup data:
      !
      do k = 1, ntheta
         theta = (k-1)*dtheta
         do j = 1, nphi
            phi   = (j-1)*dphi
            index = (k-1)*nphi + (j-1)
            !
            a00     = 0.d0
            a1m1    = 0.d0
            !
            a10     = 0.d0
            a1p1    = 0.d0
            !
            a2m2    = 1.d0
            a2m1    = 0.d0
            a20     = 1.d0
            a2p1    = 1.d0
            a2p2    = 0.d0
            !
            a3m3    = 0.d0
            a3m2    = 0.d0
            a3m1    = 0.d0
            a30     = 0.d0
            a3p1    = 0.d0
            a3p2    = 0.d0
            a3p3    = 0.d0
            !
            if (.false.) then
            ! "Regular" Spherical Harmonics:
            !
            !
            rY00   = 0.5d0 * 1.d0 / sqrt(cpi)
            iY00   = 0.0d0
!
            rY1m1  = 0.d0
            rY10   = 0.5d0 * sqrt(3.d0) / sqrt(cpi) * dcos(phi)
            iY10   = 0.d0
            rY1p1  = 0.d0
!
            rY2m2  = 0.d0
            rY2m1  = 0.d0
            rY20   = 0.25d0 * sqrt(5.d0/cpi) * (3.d0*dcos(phi)**2-1.d0)
            iY20   = 0.d0
            rY2p1  = 0.d0
            rY2p2  = 0.d0
!
            rY3m3  = 0.d0
            rY3m2  = 0.d0
            rY3m1  = 0.d0
            rY30   = 0.25d0 * sqrt(7.d0/cpi) *
     *                                (5.d0*dcos(phi)**3-3.d0*dcos(phi))
            iY30   = 0.d0
            rY3p1  = 0.d0
            rY3p2  = 0.d0
            rY3p3  = 0.d0
            !
            else
            !
            ! "Spin-Weight -2" Spherical Harmonics:
            !    (as seen in gr-qc/0610128 Eq. 42)
            !    (NB: switching phi and theta)
            !
            !if (.true.) then
               !! For testing whether the variables are switched:
               !!tmp   = theta
               !theta = phi
               !phi   = tmp
            !end if
            !
            rY00   = 0.0d0
            iY00   = 0.0d0
!
            rY1m1  = 0.d0
            rY10   = 0.d0
            iY10   = 0.d0
            rY1p1  = 0.d0
!
            rY2m2  = sqrt(5.d0/64.d0/cpi)*(1.d0-cos(theta))**2
     *                 *cos(-2.d0*phi)
            iY2m2  = sqrt(5.d0/64.d0/cpi)*(1.d0-cos(theta))**2
     *                 *sin(-2.d0*phi)
            rY2m1  = -sqrt(5.d0/16.d0/cpi)*sin(theta)*(1.d0-cos(theta))
     *                 *cos(-1.d0*phi)
            iY2m1  = -sqrt(5.d0/16.d0/cpi)*sin(theta)*(1.d0-cos(theta))
     *                 *sin(-1.d0*phi)
            rY20   = sqrt( 15.d0/32.d0/cpi) * sin(theta)**2
            iY20   = 0.d0
            rY2p1  = -sqrt(5.d0/16.d0/cpi)*sin(theta)*(1.d0+cos(theta))
     *                 *cos(phi)
            iY2p1  = -sqrt(5.d0/16.d0/cpi)*sin(theta)*(1.d0+cos(theta))
     *                 *sin(phi)
            rY2p2  = sqrt(5.d0/64.d0/cpi)*(1.d0+cos(theta))**2
     *                 *cos(2.d0*phi)
            iY2p2  = sqrt(5.d0/64.d0/cpi)*(1.d0+cos(theta))**2
     *                 *sin(2.d0*phi)
            rY2p2  = sqrt(5.d0/64.d0/cpi)*(1.d0+cos(theta))**2
     *                 *cos(2.d0*phi)
            iY2p2  = sqrt(5.d0/64.d0/cpi)*(1.d0+cos(theta))**2
     *                 *sin(2.d0*phi)
!
            rY3m3  = 0.d0
            rY3m2  = 0.d0
            rY3m1  = 0.d0
            rY30   = 0.d0
            iY30   = 0.d0
            rY3p1  = 0.d0
            rY3p2  = 0.d0
            rY3p3  = 0.d0
            end if
!
            rdata(index) = 
     *                     a00  * rY00
!
     *                   + a1m1 * rY1m1
     *                   + a10  * rY10
     *                   + a1p1 * rY1p1
!
     *                   + a2m2 * rY2m2
     *                   + a2m1 * rY2m1
     *                   + a20  * rY20
     *                   + a2p1 * rY2p1
     *                   + a2p2 * rY2p2
!
     *                   + a3m3 * rY3m3
     *                   + a3m2 * rY3m2
     *                   + a3m1 * rY3m1
     *                   + a30  * rY30
     *                   + a3p1 * rY3p1
     *                   + a3p2 * rY3p2
     *                   + a3p3 * rY3p3
!
            idata(index) = 
     *                     a00  * iY00
!
     *                   + a1m1 * iY1m1
     *                   + a10  * iY10
     *                   + a1p1 * iY1p1
!
     *                   + a2m2 * iY2m2
     *                   + a2m1 * iY2m1
     *                   + a20  * iY20
     *                   + a2p1 * iY2p1
     *                   + a2p2 * iY2p2
!
     *                   + a3m3 * iY3m3
     *                   + a3m2 * iY3m2
     *                   + a3m1 * iY3m1
     *                   + a30  * iY30
     *                   + a3p1 * iY3p1
     *                   + a3p2 * iY3p2
     *                   + a3p3 * iY3p3
!
            !
            ! Need to output a mass because "harmonic" normalizes w/r/t to it:
            !
            mdata(index) = 0.25d0 / cPi
            jdata(index) = 0.0
         end do
      end do
      !
      ! Output
      !
      gf3_rc = gft_out_full(nameoutr, time, tshape, tnames, 
     .                                     trank, tcoords, rdata)
      gf3_rc = gft_out_full(nameouti, time, tshape, tnames, 
     .                                     trank, tcoords, idata)
      gf3_rc = gft_out_full('aADM', time, tshape, tnames, 
     .                                     trank, tcoords, mdata)
      gf3_rc = gft_out_full('aJz',  time, tshape, tnames, 
     .                                     trank, tcoords, jdata)
      end





