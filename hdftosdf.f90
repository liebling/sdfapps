      PROGRAM H5_RDWT

      USE HDF5 ! This module contains all necessary modules
      IMPLICIT NONE
      CHARACTER(LEN=8), PARAMETER :: filename = "dsetf.h5" ! File name
      CHARACTER(LEN=4), PARAMETER :: dsetname = "Phi"     ! Dataset name
      INTEGER(HID_T) :: file_id       ! File identifier
      INTEGER(HID_T) :: dset_id       ! Dataset identifier
      INTEGER(HID_T) :: attr          ! Dataset identifier
      INTEGER     ::   error ! Error flag
      INTEGER     ::  i, j
      INTEGER, DIMENSION(4,6) :: dset_data, data_out ! Data buffers
      INTEGER(HSIZE_T), DIMENSION(3) :: data_dims, maxdimsr
      INTEGER(HID_T) :: dataspace     ! Dataspace identifier 
      INTEGER(HID_T) :: memspace      ! Memory dataspace identifier 
      INTEGER(HID_T) :: crp_list      ! Dataset creation property identifier 
      INTEGER(HSIZE_T), DIMENSION(1:3) :: dimsr
      real(kind=8), ALLOCATABLE   :: rdata(:,:,:)
      INTEGER :: rankr
      CHARACTER(LEN=4) , PARAMETER :: attribute = "time"
      CHARACTER(LEN=144)  :: mytime
      !INTEGER(HSIZE_T), DIMENSION(1:3) :: adims
      INTEGER(HSIZE_T), DIMENSION(10) :: adims
      !INTEGER(HSIZE_T)                 :: adims
      !
      INTEGER      :: gft_out_full,gf3_rc, gft_out_bbox
      real(kind=8) :: time, bbox(4)

      !
      ! Initialize the dset_data array.
      !
      DO i = 1, 4
      DO j = 1, 6
      dset_data(i,j) = (i-1)*6 + j
      END DO
      END DO

      !
      ! Initialize FORTRAN interface.
      !
      CALL h5open_f(error)
       !
       ! Open an existing file.
       !
       CALL h5fopen_f (filename, H5F_ACC_RDWR_F, file_id, error)

       !
       ! Open an existing dataset.
       !
       CALL h5dopen_f(file_id, dsetname, dset_id, error)
       CALL h5dopen_f(dset_id, attribute, attr, error)

       !
       !Get dataset's dataspace handle.
       !
       CALL h5dget_space_f(dset_id, dataspace, error)

       !
       !Get dataspace's rank.
       !
       CALL h5sget_simple_extent_ndims_f(dataspace, rankr, error)

       !
       !Get dataspace's dimensions.
       ! 
       CALL h5sget_simple_extent_dims_f(dataspace, dimsr, maxdimsr, error)

       !
       !Get creation property list.
       !
       CALL h5dget_create_plist_f(dset_id, crp_list, error)

       !call h5get_type_f(attr,dataspace, error)

       adims(1) = 3
       !call h5aget_name_f(attr,)
       !call h5aread_f(attr, dset_id, mytime, adims, error)
       !call h5aread_f()
       write(*,*)"Adims:  ",adims(1)
       !write(*,*)"Mytime(1): ",mytime(1)
       !write(*,*)"Mytime(2): ",mytime(2)
       !write(*,*)"Mytime(3): ",mytime(3)
       !
       ! Fill read buffer with zeroes
       !
       !rdata(1:dimsr(1),1:dimsr(2)) = 0
       if (rankr.eq.2) then
          allocate( rdata(1:dimsr(1),1:dimsr(2),1 ) )
       end if

       !
       !create memory dataspace
       !
       CALL h5screate_simple_f(rankr, dimsr, memspace, error)

       !
       !Read data 
       !
       !data_dims(1:2) = (/3,10/)
       CALL H5dread_f(dset_id, H5T_NATIVE_INTEGER, rdata, data_dims, &
              error, memspace, dataspace)

       WRITE(*,'(A,3I5)') "Dataset:" ,dimsr(1),dimsr(2), rankr
       WRITE(*,*)'data_dims',data_dims
       DO i = 1, dimsr(1)
            !WRITE(*,'(100(I0,1X))') rdata(i,1:dimsr(2))    
            WRITE(*,'(100(G10.5,1X))') rdata(i,1:dimsr(2),1)
       END DO

       !
       !Close the objects that were opened.
       !
       !CALL h5sclose_f(attr, error)
       CALL h5sclose_f(dataspace, error)
       CALL h5sclose_f(memspace, error)
       CALL h5pclose_f(crp_list, error)
       CALL h5dclose_f(dset_id, error)
       CALL h5fclose_f(file_id, error)

       !
       ! Write the dataset.
       !
       !data_dims(1) = 4
       !data_dims(2) = 6
       !CALL h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, dset_data, data_dims, error)

       !
       ! Read the dataset.
       !
       !CALL h5dread_f(dset_id, H5T_NATIVE_INTEGER, data_out, data_dims, error)

       !
       ! Close the dataset.
       !
       !CALL h5dclose_f(dset_id, error)

       !
       ! Close the file.
       !
       !CALL h5fclose_f(file_id, error)

       !
       ! Close FORTRAN interface.
       !
       !CALL h5close_f(error)

      time = 0.d0
      ! need to figure this out:
      bbox(1) = -1.d0
      bbox(2) = +1.d0
      bbox(3) = -1.d0
      bbox(4) = +1.d0
      !gf3_rc = gft_out_full(dsetname, time, dimsr, cnames, rank, coords, rdata)
      gf3_rc = gft_out_bbox(dsetname, time, dimsr, 2, bbox, rdata)


      END PROGRAM H5_RDWT

