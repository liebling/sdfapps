c     program intsdf
c
c     integrate grid function
c
      implicit none

      character*32  name, nameout, tcnames(3)
      character*128 sarg, cline, cnames
      integer       mxliv
      parameter   ( mxliv = 10 000 000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              gft_read_shape,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, x,y,z, trank
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink, split,split_axis
      ! For outputting "initial" integral
      integer       outinit
      real*8        integralinit
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      parameter   ( nx =130, ny=130, nz=130)
      integer       MAXCOORDS,         MAXPOINTS
c     parameter   ( MAXCOORDS = 10 000,  MAXPOINTS = 34 000 000 )
      parameter   ( MAXCOORDS = 100 000,  MAXPOINTS = 136 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time, 
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS)
      real*8        l2norm, maxdata, mindata,temp, integral, dx,dy,dz
      real*8        integral1, integral2
      real*8        h, rh, k1, k2, k3, k4, ftph, ftphh, ftphh_yphk1,ft
      real*8        datah, ftphh_yphk2, ftph_yk3
      real*8        interp_cubic
      external      interp_cubic
      logical       trace,           trace2
      parameter   ( trace = .false. )
      parameter   ( trace2= .false. )

       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* Integrates grid function                      *'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       intsdf  <sdf filename> <outputvector>',
     .                    '[<split index=1> <split axis=1>]'
          write(*,*) ''
          write(*,*) '   Can handle rank 1,2, or 3 data.'
          write(*,*) '   Can integrate over partial domains:'
          write(*,*) '      split index and axis determines which'
          write(*,*) '      axis gets split and where'
          write(*,*) '   Does not unify AMR data for a single integral.'
          write(*,*) '                *****************'
          stop
       end if

      !
      ! Read in arguments:
      !
      name        = sarg(1,'2d_Phi_3.sdf')
      cline       = sarg(2,'*-*')
      split       = i4arg(3,1)
      split_axis  = i4arg(4,1)
      outinit     = i4arg(5,0)

      !
      ! Initialize certain quantities:
      !
      shape(1)    = 1
      shape(2)    = 1
      shape(3)    = 1

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'intsdf: Reading in file ',name
         write(*,*) 'intsdf: Integrating times ', cline
         write(*,*) 'intsdf: For 2d, split=',split
         write(*,*) 'intsdf: For 2d, split_axis=',split_axis
         write(*,*) '*******************************'
      end if

      !
      ! Setup output vector:
      !
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
          if (trace) write(*,*) 'intsdf: using *-* as index vector'
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

      if (trace2) then
          write(*,*) 'intsdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'intsdf: Unable to read ', name
         write(*,*) 'intsdf: Quitting....'
         goto 800
      end if
      if (trace) then
          write(*,*) 'intsdf:  rank = ', rank
          write(*,*) 'intsdf:  Beginning loop....'
      end if


      !................................................
      !
      ! Loop over all data:
      !
      !................................................
      step = 0
      do while (.true.)
         step = step + 1
 		   if (trace2) then
            write(*,*) '..........step ', step
            write(*,*) '..........liv  ', liv 
            write(*,*) '..........iv(1)', iv(1) 
            if ( ivindx(iv,liv,step) .gt. 0 ) 
     .               write(*,*) '          step in index vector'
            write(*,*) '       reading in sdf'
         end if
         gf3_rc = gft_read_shape( name,     step,   shape )
         call checkmem(shape(1),shape(2),shape(3),
     .                    MAXPOINTS, MAXCOORDS)
         gf3_rc = gft_read_full( name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then

            k          = INDEX(cnames, '|')
            tcnames(1) = cnames(:k-1)
            j          = k+1
            do i = 2, rank-1
               k          = INDEX(cnames(j:), '|') + j - 1
               tcnames(i) = cnames(j:k-1)
               j          = k+1
            end do
            tcnames(rank) = cnames(j:)

            if (trace2) then
               do i = 1, rank
                  write(*,*) tcnames(i), i
               end do
            end if 

            if (split.eq.0) then
               split = nint(shape(split_axis)/2.0d0)
               if (trace) write(*,*) 'split = ', split
            end if

            if (trace2) then
               write(*,*) '..........successfully read'
               write(*,*) '..........liv  ', liv 
               write(*,*) '..........iv(1)', iv(1) 
               write(*,*) '           step:',step
               write(*,*) '           time:',time
               write(*,*) '           rank:',rank
               write(*,*) '           shape    coord name:'
               do i = 1, rank
				       write(*,*) '                 ',shape(i),
     .                  '   ',tcnames(i)
               end do
            end if
            if ( ivindx(iv,liv,step) .gt. 0 ) then
		          if (trace) write(*,*) '..........step ',step

                if (rank.eq.2) then
c               ################################
c               ###  2 Dimensional sdf files ###
c               ################################
                   integral = 0.0d0
                   do i = 2, shape(1)
                      do j = 2, shape(2)
                         dx       =   coords(i)
     .                              - coords(i-1)
                         dy       =   coords( shape(1) + j  )
     .                              - coords( shape(1) + j-1)
                         integral = integral
     .                              + 0.25d0*(
     .                                       data( (j-1)*shape(1)+i   )
     .                                     + data( (j-1)*shape(1)+i-1 )
     .                                     + data( (j-2)*shape(1)+i   )
     .                                     + data( (j-2)*shape(1)+i-1 )
     .                                        )*dx*dy
                      end do
                   end do
                   write(*,*) time, integral
c                  write(*,860) time, integral
c 861              format(2E18.5)
                else if  (rank.eq.3) then
c               ################################
c               ###  3 Dimensional sdf files ###
c               ################################
                   integral  = 0.0d0
                   integral1 = 0.0d0
                   integral2 = 0.0d0
                   do i = 2, shape(1)
                      do j = 2, shape(2)
                         do k = 2, shape(3)
                            dx       =   coords(i)
     .                                 - coords(i-1)
                            dy       =   coords( shape(1) + j  )
     .                                 - coords( shape(1) + j-1)
                            dz       =
     .                                coords( shape(2) + shape(1) + k  )
     .                              - coords( shape(2) + shape(1) + k-1)
                            integral = integral
     .                                 + 0.125d0*(
     .           data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .                                        )*dx*dy*dz
                         end do
                      end do
                   end do
                   if (split_axis.eq.1.and.split.ne.1) then
                   if (trace) write(*,*) 'Split X-axis at split=',split
                   do i = 2, split
                      do j = 2, shape(2)
                         do k = 2, shape(3)
                            dx       =   coords(i)
     .                                 - coords(i-1)
                            dy       =   coords( shape(1) + j  )
     .                                 - coords( shape(1) + j-1)
                            dz       =
     .                                coords( shape(2) + shape(1) + k  )
     .                              - coords( shape(2) + shape(1) + k-1)
                            integral1= integral1
     .                                 + 0.125d0*(
     .           data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .                                        )*dx*dy*dz
                         end do
                      end do
                   end do
                   do i = split+1, shape(1)
                      do j = 2, shape(2)
                         do k = 2, shape(3)
                            dx       =   coords(i)
     .                                 - coords(i-1)
                            dy       =   coords( shape(1) + j  )
     .                                 - coords( shape(1) + j-1)
                            dz       =
     .                                coords( shape(2) + shape(1) + k  )
     .                              - coords( shape(2) + shape(1) + k-1)
                            integral2= integral2
     .                                 + 0.125d0*(
     .           data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .                                        )*dx*dy*dz
                         end do
                      end do
                   end do
                   else if (split_axis.eq.2.and.split.ne.1) then
                   if (trace) write(*,*) 'Split Y-axis at split=',split
                   do i = 2, shape(1)
                      do j = 2, split
                         do k = 2, shape(3)
                            dx       =   coords(i)
     .                                 - coords(i-1)
                            dy       =   coords( shape(1) + j  )
     .                                 - coords( shape(1) + j-1)
                            dz       =
     .                                coords( shape(2) + shape(1) + k  )
     .                              - coords( shape(2) + shape(1) + k-1)
                            integral1= integral1
     .                                 + 0.125d0*(
     .           data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .                                        )*dx*dy*dz
                         end do
                      end do
                   end do
                   do i = 2, shape(1)
                      do j = split+1, shape(2)
                         do k = 2, shape(3)
                            dx       =   coords(i)
     .                                 - coords(i-1)
                            dy       =   coords( shape(1) + j  )
     .                                 - coords( shape(1) + j-1)
                            dz       =
     .                                coords( shape(2) + shape(1) + k  )
     .                              - coords( shape(2) + shape(1) + k-1)
                            integral2= integral2
     .                                 + 0.125d0*(
     .           data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .                                        )*dx*dy*dz
                         end do
                      end do
                   end do
                   else if (split_axis.eq.3.and.split.ne.1) then
                   if (trace) write(*,*) 'Split Z-axis at split=',split
                   do i = 2, shape(1)
                      do j = 2, shape(2)
                         do k = 2, split
                            dx       =   coords(i)
     .                                 - coords(i-1)
                            dy       =   coords( shape(1) + j  )
     .                                 - coords( shape(1) + j-1)
                            dz       =
     .                                coords( shape(2) + shape(1) + k  )
     .                              - coords( shape(2) + shape(1) + k-1)
                            integral1= integral1
     .                                 + 0.125d0*(
     .           data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .                                        )*dx*dy*dz
                         end do
                      end do
                   end do
                   do i = 2, shape(1)
                      do j = 2, shape(2)
                         do k = split+1, shape(3)
                            dx       =   coords(i)
     .                                 - coords(i-1)
                            dy       =   coords( shape(1) + j  )
     .                                 - coords( shape(1) + j-1)
                            dz       =
     .                                coords( shape(2) + shape(1) + k  )
     .                              - coords( shape(2) + shape(1) + k-1)
                            integral2= integral2
     .                                 + 0.125d0*(
     .           data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-1)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-1)*shape(1)+i-1 )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i   )
     .         + data( (k-2)*shape(2)*shape(1) + (j-2)*shape(1)+i-1 )
     .                                        )*dx*dy*dz
                         end do
                      end do
                   end do
                   else if (split.ne.1) then
                      write(*,*) 'split_axis incorrect', split_axis
                   end if
                   if (split.eq.1) then
                      write(*,*) time, integral
c                     write(*,860) time, integral
                   else
                      write(*,860) time, integral,integral1, integral2
                   end if
c 860              format(4G18.5)
c 860              format(4E18.5)
c 860              format(4F18.5)
  860            format(G19.11E3,1p,'  ',G19.11E3,1p,'  ',G19.11E3,1p,
     .                      '  ',G19.11E3)
                else if  (rank.eq.1) then
c               ################################
c               ###  1 Dimensional sdf files ###
c               ################################
                   if (.false.) then
                      ! this is just 2nd order accurate:
                      !agrees with "ser" to 10e-16
                      integral = 0.0d0
                      do i = 2, shape(1)
                         dx       = coords(i) - coords(i-1)
                         integral = integral
     .                       + 0.5d0*( data(i) + data(i-1) )*dx
                      end do
                   else
                      integral = 0.0d0
                      do j = 1, shape(1)-1
                          h   = coords(j+1)-coords(j)
                          rh  = 0.5d0 * (coords(j+1)  + coords(j))
                          if (j.gt.1.and.j.lt.shape(1)-1) then
                                datah =interp_cubic(data(j-1),data(j),
     .                                             data(j+1),data(j+2),
     .                               rh,coords(j-1), coords(j),
     .                               coords(j+1), coords(j+2)  )
                             else if (j.eq.1) then
                                datah =interp_cubic(data(j),data(j+1),
     .                                             data(j+2),data(j+3),
     .                               rh,coords(j), coords(j+1),
     .                               coords(j+2), coords(j+3)  )
                             else
                                datah =interp_cubic(data(j-2),data(j-1),
     .                                             data(j),data(j+1),
     .                               rh,coords(j-2), coords(j-1),
     .                               coords(j), coords(j+1)  )
                             end if
                             ! f(t, y):
                             ft          = data(j)
                             k1          = h*ft
                             ! f(t+h/2, y+k1/2):
                             ftphh_yphk1 = datah
                             k2          = h*ftphh_yphk1
                             ! f(t+h/2, y+k2/2):
                             ftphh_yphk2 = datah
                             k3          = h*ftphh_yphk2
                             ! f(t+h, y+k3):
                             ftph_yk3    = data(j+1)
                             k4          = h*ftph_yk3
                             !
                             integral = integral
     .                              +(1.d0/6.d0)*(k1+2.d0*k2+2.d0*k3+k4)
                      end do
                      !
                   end if    ! end: spatial loop
c                  write(*,*) time, integral
                   if (step.eq.1) integralinit = integral
                   if (outinit.eq.1) then
                      write(*,860) time, integral, integralinit
                   else
                      write(*,860) time, integral
                   end if
                end if
            else
              if (trace) write(*,*) 'intsdf: not in index vector',step
            end if
 	   else 
             if (trace2) 
     .           write(*,*) "intsdf: end of file ",
     .                       name
             goto 800
         end if
      end do
		
 800  continue
      end





