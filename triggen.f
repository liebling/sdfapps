      program main
      implicit none
      !From: 
      !http://www.fftw.org/fftw2_doc/fftw_3.html#SEC16
      ! Note also that we use the standard "in-order" output ordering--the k-th output corresponds to the frequency k/n (or k/T, where T is your total sampling period). For those who like to think in terms of positive and negative frequencies, this means that the positive frequencies are stored in the first half of the output and the negative frequencies are stored in backwards order in the second half of the output. (The frequency -k/n is the same as the frequency (n-k)/n.)
      include 'fftw3.f'

      integer     N, i, numpoints
      parameter ( N = 200000 )
      !parameter ( N = 10001 )
      !parameter ( N = 5001 )
      !parameter ( N = 1001 )
      !parameter ( N = 101 )
      real*8     PIE
      parameter (PIE=3.1415926535897932385d0)
      ! For Testing:
      real*8      omega,           phase
      real*8      minx,      maxx, dx
      real*8      tempx, tempy
      !parameter ( minx=5.d0, maxx  = 15.d0 )
            double precision in
             dimension in(N)
             double complex out
             dimension out(N/2 + 1)
             integer*8 plan
      real*8 power(N/2+1)
      real*8 f_out, omega_out, x_in, x_prev
      real*8 deltax, newdeltax
      real*8 r8arg
      character*128 name, sarg
      integer myfile, getu, i4arg, fct
      ! 
      ! Normalize:  such that fft of sin(omega*t) gives a power of 1
      ! 
      logical    normalize
      parameter (normalize = .true. )
      real*8     factor_norm
      ! Just for testing the code:
      logical    test
      parameter (test = .false. )
      logical    ltrace
      parameter (ltrace = .false. )
     

       if (iargc() .lt. 4) then
          write(*,*) '*************************************************'
          write(*,*) '*                                               *'
          write(*,*) '*  triggen:                                     *'
          write(*,*) '*            Generates trig functions.          *'
          write(*,*) '*      <mint> <maxt> <numt> <omega> [ <fct> ]   *'
          write(*,*) '*         where:                                *'
          write(*,*) '*           <mint> ---minimum value of t        *'
          write(*,*) '*           <maxt> ---maximum value of t        *'
          write(*,*) '*           <numt> ---number of points in t     *'
          write(*,*) '*           <omega>---ang.freq                  *'
          write(*,*) '*           <fct>  ---index to which function   *'
          write(*,*) '*                     one wants, where:         *'
          write(*,*) '*                       0--sin [default]        *'
          write(*,*) '*                       1--cos                  *'
          write(*,*) '*                       2--tan                  *'
          write(*,*) '*                                               *'
          write(*,*) '*            Outputs:                           *'
          write(*,*) '*                 <t> <trig output>             *'
          write(*,*) '*                                               *'
          write(*,*) '*************************************************'
          stop
       end if

      ! read in arguments
      minx      = r8arg(1,0.d0)
      maxx      = r8arg(2,1.d0)
      numpoints = i4arg(3,101)
         if (numpoints.gt.N) then
            write(*,*)'triggen: Not enough memory:',numpoints
            stop
         end if
      omega     = r8arg(4,1.d0)
      fct       = i4arg(5,0)

      phase     = 0.d0

          !minx = 0.d0
          !maxx = 8.d0
          !write(*,*)'triggen: min/max t: ',minx,maxx
          dx = (maxx-minx)/(1.d0*numpoints)
          do i = 1, numpoints
            !x_in  = 1.d0*(i-1) * (maxx-minx) / (1.d0*numpoints)
            x_in  = minx + (i-1)*dx
            if (fct.eq.0) then
               in(i) = 1.d0 * dsin( omega*x_in + phase )
            else if (fct.eq.1) then
               in(i) = 1.d0 * dcos( omega*x_in + phase )
            else if (fct.eq.0) then
               in(i) = 1.d0 * dtan( omega*x_in + phase )
            else
               write(*,*)'triggen: Unknown function:',fct
               stop
            end if
            write(*,*) x_in,in(i)
          end do

      end
