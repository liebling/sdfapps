c     program axispin
c
c     integrate grid function
c
      implicit none

      character*2   APPEND
      parameter   ( APPEND = '_p')

      character*32  name, nameout, tcnames
      character*128 sarg, cline, cnames
      integer       mxliv
      parameter   ( mxliv = 10000 )
      real*8        LARGENUMBER,   left, right
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, trank
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink, split_axis
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS,   junk, i_rho
      parameter   ( MAXCOORDS = 10 000,  MAXPOINTS = 3 000 000 )
      logical       linearinterp
      parameter   ( linearinterp = .false. )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time, 
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS),rho,
     .              threedout(MAXPOINTS), flip3
      real*8        l2norm, maxdata, mindata,temp, integral, dx,dy,dz
      real*8        integral1, integral2 
      logical       trace,           trace2
      real*8        xmax, ymax , x,y
c     parameter   ( trace = .false. )
      parameter   ( trace = .false.  )
c     parameter   ( trace2= .false. )
      parameter   ( trace2= .false. )


      do i = 1 , 3
         call sload(name,' ')
      end do

c      ### usage statement
       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* Makes 3d sdf from spinning axisymmetric data  *'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       axispin  <2d sdf filename> <outputvector>'
          write(*,*) ''
          write(*,*) '                *****************'
          stop
       end if

c      ### read in arguments
      name   = sarg(1,'2d_Phi_3.sdf')
      cline  = sarg(2,'*-*')

      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      if (trace2) write(*,*) '    junk = ', junk
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND
      if (trace2) write(*,*) ' outputing to: ', nameout

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'axispin: Reading in file ',name
         write(*,*) 'axispin: Integrating times ', cline
         write(*,*) 'axispin: Outputting to ', nameout
         write(*,*) '*******************************'
      end if
c      ### set up output vector
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
          write(*,*) 'axispin: using *-* as index vector'
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

c     if (trace2) call ivdump(iv,liv,'index vector',6)

c     ### get rank
      if (trace2) then
          write(*,*) 'axispin:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'axispin: Unable to read ', name
         write(*,*) 'axispin: Quitting....'
         goto 800
      end if
      if (rank.ne.2) then
         write(*,*) 'axispin: SDF file not 2d.'
         write(*,*) 'axispin: Quitting....'
         goto 800
      end if
      if (trace2) then
          write(*,*) 'axispin:  Finished reading in sdf file...'
          write(*,*) 'axispin:  rank = ', rank
      end if


      step = 0
      do while (.true.)
         step = step + 1
 		   if (trace2) then
            write(*,*) '..........step ', step
            write(*,*) '..........liv  ', liv 
            write(*,*) '..........iv(1)', iv(1) 
            if ( ivindx(iv,liv,step) .gt. 0 ) 
     .               write(*,*) '          step in index vector'
            write(*,*) '       reading in sdf'
         end if
         gf3_rc = gft_read_full( name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc.eq.1) then
            if ( (rank.eq.2)
     .                .and.
     .              (( (shape(1)*shape(2)).gt.MAXPOINTS)
     .                        .or.
     .               ( (shape(1)+shape(2)).gt.MAXCOORDS))
     .          ) then
                write(*,*) '2d data set too big for mem. allocated'
                write(*,*) 'Data set has ', shape(1)*shape(2), ' points'
                write(*,*) 'Data set has ', shape(1)+shape(2), ' coords'
                write(*,*) 'Data set has dimension ', shape(1),shape(2)
                write(*,*) 'Max points, coords', MAXPOINTS, MAXCOORDS
                stop
            end if

            k          = INDEX(cnames, '|')
c           tcnames(1) = cnames(:k-1)
            j          = k+1
            do i = 2, rank-1
               k          = INDEX(cnames(j:), '|') + j - 1
c              tcnames(i) = cnames(j:k-1)
               j          = k+1
            end do
c           tcnames(rank) = cnames(j:)

            if (trace2) then
               do i = 1, rank
c                 write(*,*) tcnames(i), i
               end do
            end if 

            if (trace2) then
               write(*,*) '..........successfully read'
               write(*,*) '..........liv  ', liv 
               write(*,*) '..........iv(1)', iv(1) 
               write(*,*) '           step:',step
               write(*,*) '           time:',time
               write(*,*) '           rank:',rank
               write(*,*) '           shape    coord name:'
               do i = 1, rank
				       write(*,*) '                 ',shape(i)
c    .                  ,'   ',tcnames(i)
               end do
            end if
            if ( ivindx(iv,liv,step) .gt. 0 ) then
		          if (trace2) 
     .               write(*,*) '..........step in index vector'
                   dx   = coords(2) - coords(1)
                   dy   = dx
c                  truncate.....
                   nx   = coords(shape(1))/sqrt(2.d0)/dx 
                   xmax = coords(nx+1)
                   nx   = 2*nx+1
                   ny   = nx
                   ymax = xmax
                   nz   = shape(2)
                   do i = 1, nx
                      x             = (i-1)*dx - xmax
                      y             = (i-1)*dy - ymax
                      tcoords(i)    = x
                      tcoords(nx+i) = y
                   end do
                   do k = 1, nz
                      tcoords(nx+ny+k) = coords(shape(1)+k)
                   end do
                   do i = 1, nx
                      do j = 1, ny
                         x             = (i-1)*dx - xmax
                         y             = (j-1)*dy - ymax
                         do k = 1, nz
                            rho        = sqrt(x**2+y**2)
                            i_rho      = rho / dx
                            i_rho      = i_rho + 1
                            if (      (rho.lt.coords(i_rho))
     .                          .and. (rho.ge.coords(i_rho+1)) ) then
                                write(*,*) 'something wrong, rho ',
     .                              'not bracketed.'
                            end if
                            if (linearinterp) then
                               left  = (coords(i_rho+1)-rho)
     .                                /(coords(i_rho+1)-coords(i_rho))
                               right = (rho-coords(i_rho  ))
     .                                /(coords(i_rho+1)-coords(i_rho))
c                              linearly interpolate....
                               if (i_rho.lt.shape(1)) then
                                  threedout((k-1)*ny*nx + (j-1)*nx + i)=
     .                                left *data((k-1)*shape(1)+i_rho)
     .                             +  right*data((k-1)*shape(1)+i_rho+1)
                               else if (i_rho.eq.shape(1)) then
                                  threedout((k-1)*ny*nx+(j-1)*nx+i)=
     .                                data((k-1)*shape(1)+i_rho)
                               else 
                                  write(*,*)'axispin: error irho>nrho'
                               end if
                            else 
c                              cubic interpolation....
                               if (rho.eq.coords(i_rho)) then
                                  threedout((k-1)*ny*nx+(j-1)*nx+i)=
     .                                data((k-1)*shape(1)+i_rho)
                               else if (i_rho.le.2) then
                                  threedout((k-1)*ny*nx + (j-1)*nx + i)=
     .                                flip3(rho,
     .                                   coords(1),
     .                                   coords(2),
     .                                   coords(3),
     .                                   coords(4),
     .                                   data((k-1)*shape(1)+1),
     .                                   data((k-1)*shape(1)+2),
     .                                   data((k-1)*shape(1)+3),
     .                                   data((k-1)*shape(1)+4)
     .                                   )
                               else if (i_rho.ge.(shape(1)-1) ) then
                                  threedout((k-1)*ny*nx + (j-1)*nx + i)=
     .                                flip3(rho,
     .                                   coords(shape(1)-3),
     .                                   coords(shape(1)-2),
     .                                   coords(shape(1)-1),
     .                                   coords(shape(1)  ),
     .                                  data((k-1)*shape(1)+shape(1)-3),
     .                                  data((k-1)*shape(1)+shape(1)-2),
     .                                  data((k-1)*shape(1)+shape(1)-1),
     .                                  data((k-1)*shape(1)+shape(1)  )
     .                                   )
                               else 
                                  threedout((k-1)*ny*nx+(j-1)*nx+i)=
     .                                flip3(rho,
     .                                   coords(i_rho-1),
     .                                   coords(i_rho  ),
     .                                   coords(i_rho+1),
     .                                   coords(i_rho+2),
     .                                   data((k-1)*shape(1)+i_rho-1),
     .                                   data((k-1)*shape(1)+i_rho  ),
     .                                   data((k-1)*shape(1)+i_rho+1),
     .                                   data((k-1)*shape(1)+i_rho+2)
     .                                   )
                               end if
                            end if    
                         end do
                      end do
                   end do
                   if (trace) then
                      write(*,*) 'axispin: xmax=',xmax
                      write(*,*) 'axispin: ymax=',ymax
                      write(*,*) 'axispin: rhomax=',coords(shape(1))
                      write(*,*) 'axispin: dx=',dx
                      write(*,*) 'axispin: dy=',dy
                      write(*,*) 'axispin: nx=',nx
                      write(*,*) 'axispin: ny=',ny
                   end if
                   if ( (nx*ny*nz).gt.MAXPOINTS ) then
                      write(*,*) 'axispin: Too many points'
                      write(*,*) 'axispin: nx*ny*nz=',nx*ny*nz
                      write(*,*) 'axispin: MAXPOINTS=',MAXPOINTS
                      stop
                   end if
                   tcnames   = "x|y|z"
                   trank     = 3
                   tshape(1) = nx
                   tshape(2) = ny
                   tshape(3) = nz
                   gf3_rc    = gft_out_full(nameout, time, tshape,
     .                      tcnames, trank, tcoords, threedout)
                   if (trace) then
                      if (gf3_rc .eq. 1) then
                         write(*,*) "         successful writing"
                      else
                        write(*,*) "axispin: unsuccessful writing",
     .                          " file: ",nameout
                      end if
                   end if
            else
              if (trace) write(*,*) 'axispin: not in index vector',step
            end if
 	   else 
             if (trace2) 
     .           write(*,*) "axispin: end of file ",
     .                       name
             goto 800
         end if
      end do
		
 800  continue
c     return
      end





      subroutine spin(
     .                  threedout,   twodin,
     .                  coords,
     .                  nx,           ny,
     .                  nrho,         nz      )
      return
      end
