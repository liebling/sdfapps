c     program deriv
      implicit none

      character*18   APPEND2, tmp_chr8, tmp2_chr8

      character*32  name,   nameout, tcnames(3)
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 100000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, trank,
     .              x0,y0,z0
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
      integer       myindex,indexjp2,indexjm2,indexjm1,indexjp1
		parameter   ( MAXTIMES = 1000,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 35 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,otime,
     .               deriv(MAXPOINTS),tcoords(MAXCOORDS),
     .              xval, yval, zval, radius, r8arg
      integer       MAXNTHETA, ntheta, direction
      parameter   ( MAXNTHETA = 6000)
      real*8        circle(MAXNTHETA), circledx(MAXNTHETA)
      real*8        fracx, fracy, dx, dy, minx,miny, maxx,maxy
      real*8        x,y, interp2d
      logical       double_equal, newtime
      external      r8arg, double_equal, interp2d
      real*8        cpi, dtheta, theta
      parameter   ( cpi  =  3.14159265d0 )


      real*8        FUZZ
      parameter   ( FUZZ = 1.0d-13)

      logical       first_dataset, last_dataset

      logical       trace,           trace2, outputascii
      parameter   ( trace = .false.  )
      parameter   ( trace2= .false. )
      parameter   ( outputascii= .true. )

c      ### usage statement
       if (iargc() .lt. 1) then
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       deriv  <sdf filename> <direction[=1]>'
          write(*,*) ''
          write(*,*) '  Notes: derivative of field in given direction'
          write(*,*) ''
          stop
       end if

      !
      ! Read in first data set, then for all others
      ! slice along the coordinate of the z-index chosen:
      !
      first_dataset = .true.
      last_dataset   = .false.

      xval = 0.d0
      yval = 0.d0

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
      end do

      !
      ! Read in arguments:
      !
      name   = sarg(1,'2d_Phi_3.sdf')
      direction = i4arg(2,1)
      stride = 1

      !
      ! Setup what will be appended to file name:
      !   (e.g. chi.sdf  ---> chi_i=11.sdf )
      !
      call sload(APPEND2,' ')
      APPEND2 = '_deriv'

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'deriv: Reading in file ',name
         write(*,*) 'deriv: Outputting times ', cline
         write(*,*) 'deriv: Outputting w/ : ',APPEND2
         write(*,*) 'deriv: radius=',radius
         write(*,*) '*******************************'
      end if

      !
      ! Setup output vector:
      !
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

      !
      ! Get rank:
      !
      if (trace2) then
          write(*,*) 'deriv:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step   = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'deriv: Unable to read ', name
         write(*,*) 'deriv: gf3_rc = ', gf3_rc
         write(*,*) 'deriv: Quitting....'
         goto 800
      end if
      !if (rank.eq.1) then
          !write(*,*) 'deriv:  Rank not equal to 2 or 3'
          !write(*,*) 'deriv:  Quitting...'
          !stop
      !end if

      !
      ! Create output file name
      ! based on input filename and user option:
      !
      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND2
      if (trace2) write(*,*) ' outputing to: ', nameout
      !if (.true.) write(*,*) ' outputing to: ', nameout

      !
      ! Step through all data sets:
      !
      otime = LARGENUMBER
      step = 0
 88   do while (.true.)
         step   = step + 1
         gf3_rc = gft_read_full( name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then
            !-----------------------------
            if (rank.eq.3) then
            !-----------------------------
            if ( ((shape(1)*shape(2)*shape(3).gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)+shape(2)+shape(3)).gt.MAXCOORDS))
     .          ) then
                write(*,*) '3d data set too big for mem. allocated'
                stop
            end if
            !
            !
            ! take derivative:
            call load_values1d(deriv,0.d0,shape(1)*shape(2)*shape(3))
            dx     = coords(2)-coords(1)
            do k = 3, shape(3)-2
            do j = 3, shape(2)-2
            do i = 3, shape(1)-2
               myindex  = shape(1)*shape(2)*(k-1)+shape(1)*(j  -1)+i
               if (direction.eq.2) then
                  indexjp2 = shape(1)*shape(2)*(k-1)+shape(1)*(j+2-1)+i
                  indexjp1 = shape(1)*shape(2)*(k-1)+shape(1)*(j+1-1)+i
                  indexjm2 = shape(1)*shape(2)*(k-1)+shape(1)*(j-2-1)+i
                  indexjm1 = shape(1)*shape(2)*(k-1)+shape(1)*(j-1-1)+i
!                 From had/src/hyper/derivs42.f90:
!   (-u(i,j+2,k) + 8.d0*u(i,j+1,k) - 8.d0*u(i,j-1,k) + u(i,j-2,k))/(12.d0*dy)
               else if (direction.eq.3) then
                  indexjp2 = shape(1)*shape(2)*(k+2-1)+shape(1)*(j-1)+i
                  indexjp1 = shape(1)*shape(2)*(k+1-1)+shape(1)*(j-1)+i
                  indexjm2 = shape(1)*shape(2)*(k-2-1)+shape(1)*(j-1)+i
                  indexjm1 = shape(1)*shape(2)*(k-1-1)+shape(1)*(j-1)+i
               else if (direction.eq.1) then
                  indexjp2 = shape(1)*shape(2)*(k-1)+shape(1)*(j-1)+i+2
                  indexjp1 = shape(1)*shape(2)*(k-1)+shape(1)*(j-1)+i+1
                  indexjm2 = shape(1)*shape(2)*(k-1)+shape(1)*(j-1)+i-2
                  indexjm1 = shape(1)*shape(2)*(k-1)+shape(1)*(j-1)+i-1
               end if
               deriv(myindex) = (-data(indexjp2)+8.d0*data(indexjp1)
     *                           -8.d0*data(indexjm1)+data(indexjm2)
     *                          ) /12.d0 / dx
            end do
            end do
            end do
                  !
                  !
                  !
            else if (rank.eq.2) then
               if(trace2)write(*,*)'deriv: 2d data:'
               if ( ((shape(1)*shape(2).gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)+shape(2)).gt.MAXCOORDS))
     .          ) then
                   write(*,*) '2d data set too big for mem. allocated'
                   stop
               end if
               !
               !
               ! take derivative:
               call load_values1d(deriv,0.d0,shape(1)*shape(2))
               dx     = coords(2)-coords(1)
               do j = 3, shape(2)-2
               do i = 3, shape(1)-2
                  myindex  = shape(1)*(j-1)+i
                  if (direction.eq.2) then
                     indexjp2 = shape(1)*(j+2-1)+i
                     indexjp1 = shape(1)*(j+1-1)+i
                     indexjm2 = shape(1)*(j-2-1)+i
                     indexjm1 = shape(1)*(j-1-1)+i
!                 From had/src/hyper/derivs42.f90:
!   (-u(i,j+2,k) + 8.d0*u(i,j+1,k) - 8.d0*u(i,j-1,k) + u(i,j-2,k))/(12.d0*dy)
                  else if (direction.eq.1) then
                     indexjp2 = shape(1)*(j-1)+i+2
                     indexjp1 = shape(1)*(j-1)+i+1
                     indexjm2 = shape(1)*(j-1)+i-2
                     indexjm1 = shape(1)*(j-1)+i-1
                  end if
                  deriv(myindex) = (-data(indexjp2)+8.d0*data(indexjp1)
     *                           -8.d0*data(indexjm1)+data(indexjm2)
     *                          ) /12.d0 / dx
               end do
               end do
            else if (rank.eq.1) then
               if(trace2)write(*,*)'deriv: 1d data:'
               if ( ((shape(1).gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)).gt.MAXCOORDS))
     .          ) then
                   write(*,*) '1d data set too big for mem. allocated'
                   stop
               end if
               !
               !
               ! take derivative:
               call load_values1d(deriv,0.d0,shape(1))
               dx     = coords(2)-coords(1)
               do i = 3, shape(1)-2
                     myindex  = i
                     indexjp2 = i+2
                     indexjp1 = i+1
                     indexjm2 = i-2
                     indexjm1 = i-1
                  deriv(myindex) = (-data(indexjp2)+8.d0*data(indexjp1)
     *                           -8.d0*data(indexjm1)+data(indexjm2)
     *                          ) /12.d0 / dx
               end do
            end if   ! if-then over rank
                  !
                  ! Outputting data:
                  !
                  if(trace2)write(*,*)'deriv: Outputting derivative'
                   gf3_rc = gft_out_full(nameout, time,shape,cnames,
     .                                     rank, coords, deriv)
                   if (trace2) then
		                if (gf3_rc .eq. 1) then
                         write(*,*) "         successful writing"
		                else
                         write(*,*) "         unsuccessful writing",
     .                                " file: ",nameout
                      end if
                   end if
                  !
 	      else 
             if (trace2) write(*,*) "deriv: end of file ", name
             goto 800
         end if       ! end if gf_rc>0
      end do
		
 800  continue
      end






