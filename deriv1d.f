      program uniform
      implicit none

      integer     maxN, N, i
      parameter ( maxN = 10 000 000 )
      real*8     PIE
      parameter (PIE=3.1415926535897932385d0)
      real*8      omega,           phase
      parameter ( omega = 77.d0,   phase = 10.d0 )
      real*8      minx,      maxx
      real*8      tempx, tempy
      real*8 x_in(MaxN), x_out(MaxN)
      real*8   in(MaxN),   out(MaxN)
      real*8 f_out, omega_out, x_prev
      real*8 deltax, newdeltax
      character*128 name, sarg
      integer myfile, getu, i4arg
     

       if (iargc() .lt. 1) then
          write(*,*) '*************************************************'
          write(*,*) '*                                               *'
          write(*,*) '*  deriv1d:                                     *'
          write(*,*) '*            Takes as input a data file in      *'
          write(*,*) '*            the form:                          *'
          write(*,*) '*                 <x_i> <data_i>                *'
          write(*,*) '*            and calculates the 1D derivative   *'
          write(*,*) '*            of it, asumming it is              *'
          write(*,*) '*            uniform in x_i.                    *'
          write(*,*) '*                                               *'
          write(*,*) '*            Outputs:                           *'
          write(*,*) '*                 <x_i> <derivative>            *'
          write(*,*) '*                                               *'
          write(*,*) '*  Usage:                                       *'
          write(*,*) '*    deriv1d  <filename>                        *'
          write(*,*) '*                                               *'
          write(*,*) '*************************************************'
          stop
       end if

      ! initialize to blanks
      call sload(name,' ')
      ! read in argument
      name   = sarg(1,'test.dat')
      N      = i4arg(2,1001)
      if (N.gt.maxN) then
         write(*,*)'uniform: Not enough memory'
         stop
      end if

      myfile = getu()
      open(myfile,file=name,form='formatted',status='old',err=800)
      i = 0
      minx = +9d99
      maxx = -9d99
  8   read(myfile,*,end=10) tempx, tempy
      i = i + 1
      x_in(i) = tempx
      in(i)   = tempy
      if (x_in(i).gt.maxx) maxx=x_in(i)
      if (x_in(i).lt.minx) minx=x_in(i)
      if (i.eq.2) then
         deltax = x_in(i)-x_prev
      else if (i.gt.2) then
         newdeltax = x_in(i)-x_prev
         if (abs(newdeltax-deltax).gt.1e-6) then
            write(*,*)'deriv1d: Input data not uniform in x'
            write(*,*)'deriv1d: at line: ',i
            write(*,*)'deriv1d: deltax, newdeltax = ',deltax,newdeltax
            stop
         end if
      end if
      if (i.eq.maxN) then
            write(*,*)'deriv1d: Too much data: ',maxN
            goto 10
      end if
      x_prev = x_in(i)
      goto 8
 10   close(myfile)

      N = i
      do i = 1, N-1
         x_out(i) = 0.5d0*(x_in(i)+x_in(i+1))
         out(i)   = (in(i+1)-in(i))/deltax
         write(*,*) x_out(i),out(i)
      end do

      stop
 800  continue
      write(*,*) 'deriv1d: file: ',name,' not found.'

      end

