c     program chopsdf
      implicit none

      character*18   APPEND2, tmp_chr8, tmp2_chr8

      character*32  name,   nameout, tcnames(3), centername
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 100000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, trank
      real*8        x0,y0,z0
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,h,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 1000,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 35 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,otime,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS),
     .              xval, yval, zval, radius
      integer       MAXNPHI, nphi, myindex
      parameter   ( MAXNPHI = 1200)
      integer       MAXNTHETA, ntheta
      parameter   ( MAXNTHETA = 600)
      real*8        circle(MAXNTHETA*MAXNPHI),
     .              circledx(MAXNTHETA*MAXNPHI)
      real*8        fracx, fracy, fracz, dx, dy,dz
      real*8        minx,miny, minz,maxx,maxy,maxz
      real*8        x,y,z
      logical       double_equal, newtime
      real*8        cpi, dphi, dtheta, theta, phi
      parameter   ( cpi  =  3.14159265d0 )
      real*8        interp2d, interp3d, r8arg
      external      r8arg, double_equal, interp2d, interp3d

      ! Variables for getting center:
      integer   centernum, ocenternum, rc
      real*8    centert, centerx, centery, centerz
      real*8   ocentert,ocenterx,ocentery,ocenterz

      real*8        FUZZ
      parameter   ( FUZZ = 1.0d-13)

      logical       first_dataset, last_dataset, usecenter

      logical       trace, trace2, trace3, outputascii
      parameter   ( trace = .false. )
      parameter   ( trace2= .false. )
      parameter   ( trace3= .false. )
      parameter   ( outputascii= .true. )

c      ### usage statement
       if (iargc() .lt. 1) then
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       interpcircle  <sdf filename> <r[=1]>', 
     *               ' <ntheta[=101]> <nphi[=201]>'
          write(*,*) '           <x0[=0.0]> <y0[=0.0]> <z0[=0.0]>'
          write(*,*) '           <filename>'
          write(*,*) ''
          write(*,*) '  Notes:-Takes 2d sdf data and interpolates '
          write(*,*) '         that data onto a circle, uniformly   '
          write(*,*) '         distributed in theta, with radius r'
          write(*,*) '  Outputs to a file with suffix _circle'
          write(*,*) ''
          write(*,*) '        -If    3d sdf data, then it interpolates'
          write(*,*) '         that data onto a sphere, uniformly   '
          write(*,*) '         distributed in theta,phi with radius r'
          write(*,*) '  Outputs to a file with suffix _sphere'
          write(*,*) ''
          write(*,*) '        -Computes relative to a center of (0,0)'
          write(*,*) '         or (0,0,0) unless another point is given'
          write(*,*) ''
          write(*,*) '        -If a filename is given, the center is '
          write(*,*) '        taken to be given by an entry in the file'
          write(*,*) '         which has the largest time equal or less'
          write(*,*) '       than that for the data to be interpolated.'
          write(*,*) '         The expected format for the file is:'
          write(*,*) '           <number> <time> <x0> <y0> <z0>'
          write(*,*) ''
          write(*,*) '  Be careful about angle conventions/definitions'
          stop
       end if

      !
      ! Read in first data set, then for all others
      ! slice along the coordinate of the z-index chosen:
      !
      first_dataset = .true.
      last_dataset  = .false.

      xval = 0.d0
      yval = 0.d0

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
         call sload(centername,' ')
      end do

      !
      ! Read in arguments:
      !
      name   = sarg(1,'2d_Phi_3.sdf')
      radius = r8arg(2,1.d0)
      ntheta = i4arg(3,101)
      if (ntheta.gt.MAXNTHETA) then
         write(*,*) 'interpcircle: Decrease ntheta:',ntheta
         write(*,*) 'interpcircle: or increase MAXNTHETA:',MAXNTHETA
         stop
      end if
      nphi   = i4arg(4,201)
      if (ntheta.gt.MAXNPHI) then
         write(*,*) 'interpcircle: Decrease nphi:',nphi
         write(*,*) 'interpcircle: or increase MAXNPHI:',MAXNPHI
         stop
      end if
      x0     = r8arg(5,0.d0)
      y0     = r8arg(6,0.d0)
      z0     = r8arg(7,0.d0)
      centername = sarg(8,' ')
      usecenter  = .false.
      if (centername.ne.' ') then
         usecenter = .true.
         open(unit=8,file=centername,form='formatted',
     .        status='old',iostat=rc)
               if (rc.ne.0) then
                  write(*,*)'interpcircle: unable to open center file'
                  stop
               end if
         centernum = 1
         centert   = -9.d50
         centerx   = 0.d0
         centery   = 0.d0
         centerz   = 0.d0
      end if
      stride = 1

      !
      ! Setup what will be appended to file name:
      !   (e.g. chi.sdf  ---> chi_i=11.sdf )
      !
      call sload(APPEND2,' ')
      APPEND2 = '_circle'

      if (.true.) then
         write(*,*) '*******************************'
         write(*,*) 'interpcircle: Reading in file ',name
         write(*,*) 'interpcircle: Outputting times ', cline
         !write(*,*) 'interpcircle: Outputting w/ : ',APPEND2
         write(*,*) 'interpcircle: radius=',radius
         write(*,*) 'interpcircle: usecenter=',usecenter
         if(.not.usecenter)then
            write(*,*) 'interpcircle: Center x/y/z0=',x0,y0,z0
         else
            write(*,*) 'interpcircle: Getting centers from file=',
     .                     centername
         end if
         write(*,*) '*******************************'
      end if

      !
      ! Setup output vector:
      !
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

      !
      ! Get rank:
      !
      if (trace2) then
          write(*,*) 'interpcircle:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step   = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'interpcircle: Unable to read ', name
         write(*,*) 'interpcircle: gf3_rc = ', gf3_rc
         write(*,*) 'interpcircle: Quitting....'
         goto 800
      end if
      if (rank.lt.2) then
          write(*,*) 'interpcircle:  Rank less than 2'
          write(*,*) 'interpcircle:  Quitting...'
          stop
      else if (rank.gt.3) then
          write(*,*) 'interpcircle:  Rank greater than 3'
          write(*,*) 'interpcircle:  Quitting...'
          stop
      end if

      !
      ! Create output file name
      ! based on input filename and user option:
      !
      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND2
      if (trace2) write(*,*) ' outputing to: ', nameout
      !if (.true.) write(*,*) ' outputing to: ', nameout

      !
      ! Step through all data sets:
      !
      otime = LARGENUMBER
      step = 0
 88   do while (.true.)
         step   = step + 1
         gf3_rc = gft_read_full( name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then
            !!
            if (rank.eq.2) then
            !!...............................................
            !!  Interpoloating to a circle:
            !!...............................................
            if ( ((shape(1)*shape(2)         .gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)+shape(2)         ).gt.MAXCOORDS))
     .          ) then
                write(*,*) '2d data set too big for mem. allocated'
                stop
            end if
            !
            !
            if (double_equal(time,otime)) then
               newtime = .false.
            else
               newtime = .true.
            end if
            if (newtime.and.usecenter) then
               if(trace)write(*,*)'interpcircle:Find appropriate center'
               if(trace)write(*,*)'interpcircle:old center:',x0,y0
  645          if (centert.gt.time) then
                 if(trace2)write(*,*)'interpcircle:..nothing to be done'
                  x0 = ocenterx
                  y0 = ocentery
               elseif (double_equal(centert,time)) then
                  if(trace2)write(*,*)'interpcircle:..times equal'
                  x0 =  centerx
                  y0 =  centery
               else
                  if(trace2)write(*,*)'interpcircle:..read next entry'
                  ocenternum = centernum
                  ocentert   = centert
                  ocenterx   = centerx
                  ocentery   = centery
                  read(8,*,iostat=rc) centernum,centert,centerx,
     .                                   centery
                  if(trace2)write(*,'("   entry: ",3G10.4)')
     .                             centert,centerx,centery
                  if (rc.ne.0) then
                     ! this effectively stops the loop
                     if(trace2)write(*,*)'interpcircle:no more entries'
                     centert = 9.9d50
                  end if
                  goto 645
               end if
               if(trace)write(*,*)'interpcircle:new center:',x0,y0
            end if
            if(trace2)write(*,*)'interpcircle: newtime:',newtime
            if (newtime) then
  700          if (.not.first_dataset) then
                  !
                  ! Outputting data:
                  !
                  if(trace2)write(*,*)'interpcircle: Outputting circle',
     .                otime
                   dtheta = 2.d0*cPi/(ntheta-1)
                   do i = 1, ntheta
                       tcoords( i ) = dtheta*(i-1)
                   end do
                   trank      = 1
                   tshape(1)  = ntheta
                   tnames     = 'theta'
                   if (.true.) then
                   gf3_rc = gft_out_full(nameout//'dx', otime,tshape,
     .                              tnames,trank, tcoords, circledx)
                   end if
                   gf3_rc = gft_out_full(nameout, otime,tshape,tnames,
     .                                     trank, tcoords, circle)
                   if (trace2) then
		                if (gf3_rc .eq. 1) then
                         write(*,*) "         successful writing"
		                else
                         write(*,*) "         unsuccessful writing",
     .                                " file: ",nameout
                      end if
                   end if
                  if (outputascii.or.trace2) then
                     do i = 1,ntheta
                        write(*,97) otime, dtheta*(i-1), circle(i)
                     end do
   97                format(3G16.9)
                  end if
                  if (last_dataset) goto 800
                  !
                  !
               else
                  first_dataset = .false.
               end if
               !
               !
               otime = time
               if(trace2)write(*,*)'interpcircle: Resetting circle'
               do i = 1, ntheta
                  circle(i)   = LARGENUMBER
                  circledx(i) = LARGENUMBER
               end do
               if(trace)write(*,*)'interpcircle: New time: ',time
            end if
            dtheta = 2.d0*cPi/(ntheta-1)
            minx   = coords(1)
            maxx   = coords(shape(1))
            dx     = coords(2)-coords(1)
            miny   = coords(shape(1)+1)
            maxy   = coords(shape(1)+shape(2))
            dy     = coords(shape(1)+2)-coords(shape(1)+1)
            if(trace2)write(*,*)'interpcircle:Extract from set:',time,dx
            if(trace2)write(*,*)'interpcircle: ',shape(1),shape(2),minx,
     *                  maxx, miny, maxy
            do k = 1, ntheta
               theta = dtheta*(k-1)
               !x     = radius*dcos(theta)
               !y     = radius*dsin(theta)
               x     = radius*dcos(theta) + x0
               y     = radius*dsin(theta) + y0
               i     = INT( (x-minx)/dx ) + 1
               j     = INT( (y-miny)/dy ) + 1
               if(trace2) write(*,99) ' =',k,theta,x,y,i,j
  99           format(A,I5,2P,3G10.4,2P,2I5)
               if ( i.ge.1 .and. i.le.shape(1) .and.
     .              j.ge.1 .and. j.le.shape(2)    ) then
                  fracx = ( x - coords(         i) )/dx
                  fracy = ( y - coords(shape(1)+j) )/dy
                  if(trace2) write(*,98) '  fracx/y: ',fracx,fracy,
     .                                          circledx(k),dx
  98              format(A,4G10.4)
                  if (circledx(k) .gt. dx) then
                     ! Only use this data if it has better resolution
                     ! than alread interpolated data:
                     circle(k)   = interp2d( data,fracx,fracy,
     .                                i,j,shape(1),shape(2))
                     circledx(k) = dx
                     if(trace2) write(*,*) ' storing: ',k,circle(k)
                  else
                     if(trace3) write(*,*) ' not storing: ',k
                  end if
               end if
            end do
            !!
            !!..........................................................
            else if (rank.eq.3) then
            !!..........................................................
            !!...............................................
            !!  Interpoloating to a sphere:
            !!...............................................
            !!
            APPEND2 = '_sphere'
            nameout(junk:)    = APPEND2
            if (trace3) write(*,*) ' outputing to: ', nameout
            if(trace3)write(*,*)'interpcircle: rank=3d:'
            if ( ((shape(1)*shape(2)*shape(3).gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)+shape(2)+shape(3)).gt.MAXCOORDS))
     .          ) then
                write(*,*) '3d data set too big for mem. allocated'
                stop
            end if
            !
            !
            if (double_equal(time,otime)) then
               newtime = .false.
            else
               newtime = .true.
            end if
            if (newtime.and.usecenter) then
               if(trace)write(*,*)'interpcircle:Find appropriate center'
               if(trace)write(*,*)'interpcircle:old center:',x0,y0,z0
  644          if (centert.gt.time) then
                  if(trace2)write(*,*)'interpcircle:nothing to be done'
                  x0 = ocenterx
                  y0 = ocentery
                  z0 = ocenterz
               elseif (double_equal(centert,time)) then
                  if(trace2)write(*,*)'interpcircle:..times equal'
                  x0 =  centerx
                  y0 =  centery
                  z0 =  centerz
               else
                  if(trace2)write(*,*)'interpcircle:..read next entry'
                  ocenternum = centernum
                  ocentert   = centert
                  ocenterx   = centerx
                  ocentery   = centery
                  ocenterz   = centerz
                  read(8,*,iostat=rc) centernum,centert,centerx,
     .                                   centery,centerz
                  if(trace2)write(*,'("   entry: ",4G10.4)')
     .                             centert,centerx,centery,centerz
                  if (rc.ne.0) then
                     ! this effectively stops the loop
                     if(trace2)write(*,*)'interpcircle:no more entries'
                     centert = 9.9d50
                  end if
                  goto 644
               end if
               if(trace)write(*,*)'interpcircle:new center:',x0,y0,z0
            end if
            if(trace3)write(*,*)'interpcircle: newtime:',newtime
            if (newtime) then
  701          if (.not.first_dataset) then
                  !
                  ! Outputting data:
                  !
                  if(trace)write(*,*)'interpcircle: -----------------'
                  if(trace)write(*,*)'interpcircle: Outputting sphere',
     .                            otime
                   dtheta = 1.d0*cPi/(ntheta-1)
                   dphi   = 2.d0*cPi/(nphi  -1)
                   do i = 1, ntheta
                       tcoords( i ) = dtheta*(i-1)
                   end do
                   do i = 1, nphi
                       tcoords( ntheta+i ) = dphi*(i-1)
                   end do
                   trank      = 2
                   tshape(1)  = ntheta
                   tshape(2)  = nphi
                   tnames     = 'theta|phi'
                   if (.true.) then
                      ! Switch axes to be consistent with HAD surface output
                      tshape(1) = nphi
                      tshape(2) = ntheta
                      tnames    = 'phi|theta'
                      call switchaxes2d(tdata,circle,tcoords,
     .                                            ntheta,nphi)
                   end if
                   if (trace2) then
                   gf3_rc = gft_out_full(nameout//'dx', otime,tshape,
     .                              tnames,trank, tcoords, circledx)
                   end if
                   gf3_rc = gft_out_full(nameout, otime,tshape,tnames,
     .                                     trank, tcoords, circle)
                   if (trace2) then
                      if (gf3_rc .eq. 1) then
                         write(*,*) "         successful writing",otime
                      else
                         write(*,*) "         unsuccessful writing",
     .                                " file: ",nameout
                      end if
                   end if
                  if (last_dataset) goto 800
                  !
                  !
               else
                  first_dataset = .false.
               end if
               !
               !
               otime = time
               if(trace2)then
                  write(*,*)'interpcircle: **************'
                  write(*,*)'interpcircle: **************'
                  write(*,*)'interpcircle: Resetting sphere'
                  write(*,*)'interpcircle: center x/y/z0:',x0,y0,z0
               end if
               do j = 1, nphi
                  do i = 1, ntheta
                     myindex           = (j-1)*ntheta + i
                     circle(myindex)   = LARGENUMBER
                     circledx(myindex) = LARGENUMBER
                  end do
               end do
               if(trace)write(*,*)'interpcircle: New time: ',time
               if(trace2)then
                  write(*,*)'interpcircle: New time: ',time
                  write(*,*)'interpcircle: **************'
                  write(*,*)'interpcircle: **************'
               end if
            end if
            dtheta = 1.d0*cPi/(ntheta-1)
            dphi   = 2.d0*cPi/(nphi  -1)
            minx   = coords(1)
            maxx   = coords(shape(1))
            dx     = coords(2)-coords(1)
            miny   = coords(shape(1)+1)
            maxy   = coords(shape(1)+shape(2))
            dy     = coords(shape(1)+2)-coords(shape(1)+1)
            minz   = coords(shape(1)+shape(2)+1)
            maxz   = coords(shape(1)+shape(2)+shape(3))
            dz     = coords(shape(1)+shape(2)+2)
     *              -coords(shape(1)+shape(2)+1)
            if(trace2)write(*,*)'interpcircle:Extract from set:',time,dx
            if(trace3)write(*,*)'interpcircle: ',shape(1),shape(2),minx,
     *                  maxx, miny, maxy
            do l = 1, nphi
               phi   = dphi*(l-1)
            do k = 1, ntheta
               theta = dtheta*(k-1)
               x     = radius*dcos(phi)*dsin(theta) + x0
               y     = radius*dsin(phi)*dsin(theta) + y0
               z     = radius*          dcos(theta) + z0
               i     = INT( (x-minx)/dx ) + 1
               j     = INT( (y-miny)/dy ) + 1
               h     = INT( (z-minz)/dz ) + 1
  76           format(A,3I5)
  77           format(A,3G12.3)
  78           format(A,I5,G12.3)
               if ( i.ge.1 .and. i.lt.shape(1) .and.
     .              j.ge.1 .and. j.lt.shape(2) .and.
     .              h.ge.1 .and. h.lt.shape(3)    ) then
                  fracx = ( x - coords(                  i) )/dx
                  fracy = ( y - coords(         shape(1)+j) )/dy
                  fracz = ( z - coords(shape(2)+shape(1)+h) )/dz
                  if(trace3) write(*,78) ' k/theta=',k,theta
                  if(trace3) write(*,78) ' l/phi  =',l,phi
                  if(trace3) write(*,77) 'dx/y/z  =',dx,dy,dz
                  if(trace3) write(*,77) ' x/y/z  =',x,y,z
                  if(trace3) write(*,76) ' i/j/h  =',i,j,h
                  if(trace3) write(*,77) ' nx/y/z =',shape(1),shape(2),
     .                                               shape(3)
                  if(trace3) write(*,77) ' fracx/y/z:',fracx,fracy,fracz
                  if(trace3) write(*,77) ' circle/dx:',circledx(k),dx
                  myindex = (l-1)*ntheta + k
                  if (circledx(myindex) .gt. dx) then
                     ! Only use this data if it has better resolution
                     ! than already interpolated data:
                     myindex           = (l-1)*ntheta + k
                     circle(myindex)   =interp3d(data,fracx,fracy,fracz,
     .                                 i,j,h,shape(1),shape(2),shape(3))
                     circledx(myindex) = dx
                     if(trace3) then
                        write(*,*) ' storing: ',myindex,circle(myindex)
                        write(*,*) 'ntheta/phi:',ntheta,nphi,ntheta*nphi
                        write(*,78) ' k/theta=',k,theta
                        write(*,78) ' l/phi  =',l,phi
                        write(*,77) 'dx/y/z  =',dx,dy,dz
                        write(*,77) ' x/y/z  =',x,y,z
                        write(*,76) ' i/j/h  =',i,j,h
                        write(*,77) ' nx/y/z =',shape(1),shape(2),
     .                                               shape(3)
                        write(*,77) ' fracx/y/z:',fracx,fracy,fracz
                        write(*,77) ' circle/dx:',circledx(myindex),dx
                        myindex = (k-1)*shape(2)*shape(1) 
     *                           +(j-1)*shape(1)
     *                           + i
                        write(*,77) ' data:',data(myindex)
                        myindex = (k-1)*shape(2)*shape(1) 
     *                           +(j-1)*shape(1)
     *                           + i+1
                        write(*,77) '.data:',data(myindex)
                     end if
                  else
                     if(trace3) write(*,*) ' not storing: ',k
                  end if
               end if
            end do
            end do
            !!
            end if  ! rank .eq. 3
            !!
         else 
             if (trace) write(*,*) "interpcircle: end of file ", name
             last_dataset = .true.
             if (rank.eq.2) then
                goto 700
             else if (rank.eq.3) then
                goto 701
             end if
         end if       ! end if gf_rc>0
      end do
		
 800  continue
      end


!.......................................................................
!.                                                                       
!. Switches axes of 2D data stored:                                      
!.      On input:   mydata(nx,ny)  and coords(nx+ny)                      
!.      On output:  mydata(ny,nx)  and coords(ny+nx)                     
!.                                                                       
!.......................................................................
      subroutine  switchaxes2d(temp,mydata,coords,nx,ny)
      implicit none
      integer  nx,ny
      real*8   temp(nx*ny),mydata(nx*ny),coords(nx+ny)
      integer  i,j
      integer  myindex1, myindex2

      ! switch coords:
      do j = 1, ny
         temp(j)    = coords(nx+j)
      end do
      do i = 1, nx
         temp(i+ny) = coords(i)
      end do
      ! ...copy coords back:
      do i = 1, ny+nx
         coords(i) = temp(i)
      end do

      ! switch data
      do j = 1, ny
         do i = 1, nx
            myindex1        = (j-1)*nx + i
            myindex2        = (i-1)*ny + j
            temp(myindex2)  = mydata(myindex1)
         end do
      end do
      ! ...copy data back:
      do i = 1, ny*nx
         mydata(i) = temp(i)
      end do

      return
      end       ! END: switchaxes2d




