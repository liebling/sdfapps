c     program chopsdf
      implicit none


!    Based on work by:
!    
!    @Article{Rosswog:2006rh,
!         author    = "Rosswog, Stephan",
!         title     = "{Fallback accretion in the aftermath of a compact binary
!                      merger}",
!         journal   = "Mon. Not. Roy. Astron. Soc. Lett.",
!         volume    = "376",
!         year      = "2007",
!         pages     = "L48-L51",
!         eprint    = "astro-ph/0611440",
!         archivePrefix = "arXiv",
!         doi       = "10.1111/j.1745-3933.2007.00284.x",
!         SLACcitation  = "%%CITATION = ASTRO-PH/0611440;%%"
!    }
!    

      character*18   APPEND2, tmp_chr8, tmp2_chr8, prefix

      character*32  name,   nameout, tcnames(3), centername,
     .              gft_name
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3), numproc,maxproc
      parameter   ( mxliv = 100000 )
      real*8        LARGENUMBER,           SMALLNUMBER
      parameter   ( LARGENUMBER = 1.0d68,  SMALLNUMBER = 1.0d-13)
      integer       gft_close_all, nummasked
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,
     .              iv(mxliv), liv, key, trank
      real*8        x0,y0,z0, B, tmpreal, tmpreal2, h, myh
      integer       mystringlength, lenpre
      external      mystringlength
      integer       numfloored, numcutoff, numdatasets, numbad
      integer       totalnumpoints
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 1000,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 6000,  MAXPOINTS = 1 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,otime,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS),
     .               rho(MAXPOINTS),
     .               D0(MAXPOINTS),
     .               vx(MAXPOINTS),
     .               vy(MAXPOINTS),
     .               vz(MAXPOINTS),
     .               vsq(MAXPOINTS),
     .               KE_i(MAXPOINTS),
     .               e_i(MAXPOINTS),
     .               a_i(MAXPOINTS),
     .               r_max(MAXPOINTS),
     .               J_i(MAXPOINTS),
     .               TE_i(MAXPOINTS),
     .               m_i(MAXPOINTS),
     .               absV(MAXPOINTS),
     .               r_i(MAXPOINTS),
     .               A(MAXPOINTS),
     .               C(MAXPOINTS),
     .               DD(MAXPOINTS),
     .               I_r_i(MAXPOINTS),
     .               I_r_max(MAXPOINTS),
     .               I_r_dis(MAXPOINTS),
     .               finesth(MAXPOINTS),
     .               vdotr(MAXPOINTS),
     .               tau(MAXPOINTS),
     .               unboundM(MAXPOINTS),
     .               boundM(MAXPOINTS),
     .               totalM(MAXPOINTS),
     .               mytmp_ri(MAXPOINTS),
     .               mytmp_rdis(MAXPOINTS),
     .               mytmp_rmax(MAXPOINTS),
     .               mytmp2_ri(MAXPOINTS),
     .               mytmp2_rdis(MAXPOINTS),
     .               mytmp2_rmax(MAXPOINTS),
     .              xval, yval, zval, radius
      integer       MAXNPHI, nphi, myindex
      parameter   ( MAXNPHI = 1200)
      integer       MAXNTHETA, ntheta
      parameter   ( MAXNTHETA = 600)
      real*8        circle(MAXNTHETA*MAXNPHI),
     .              circledx(MAXNTHETA*MAXNPHI)
      real*8        fracx, fracy, fracz, dx, dy,dz
      real*8        minx,miny, minz,maxx,maxy,maxz
      real*8        x,y,z
      logical       double_equal, newtime
      real*8        cpi, dphi, dtheta, theta, phi
      parameter   ( cpi  =  3.14159265d0 )
      real*8        interp2d, interp3d, r8arg
      external      r8arg, double_equal, interp2d, interp3d
      real*8        totalmass, unboundmass, boundmass, setmass
      logical       found_finesth, numproblem
      logical       first_dataset, last_dataset, usecenter, onfinest

      ! Parameters describing the black hole:
      !     BHMass       ---> mass of black hole, shows up in Potential Energy
      !     BHcent_x/y/z ---> shows up in radius of potential and ang. mome.
      real*8        BHMass,    BHcent_x, BHcent_y, BHcent_z, floor
      parameter   ( BHMass   = 7.5d0)
      ! For t=1806:
      parameter   ( BHcent_x = 10.0d0)
      parameter   ( BHcent_y =  0.0d0)
      ! For t=3006:
      !parameter   ( BHcent_x =  5.0d0)
      !parameter   ( BHcent_y =  3.0d0)
      parameter   ( BHcent_z =  0.0d0)
      ! Radius at which the "particle"'s energy is dissipated:
      !     (Rosswog chooses R_dis=10GM/c^2)
      !     Results appear quite sensitive to this value:
      !          5BHMass ---> no negative slope region at all
      !          6BHMass ---> dual peaked, very steep negative slope region
      !          7BHMass ---> slope of -0.56 (not a great fit though)
      !          8BHMass ---> slope of -1.4
      !          9BHMass ---> slope of -1.3
      !         10BHMass ---> slope of -1.3
      !         15BHMass ---> slope of -1.3
      real*8        R_dis
      !parameter   ( R_dis = 5d0*BHMass)
      !parameter   ( R_dis = 6d0*BHMass)
      !parameter   ( R_dis = 7d0*BHMass)
      !parameter   ( R_dis = 8d0*BHMass)
      !parameter   ( R_dis = 9d0*BHMass)
      parameter   ( R_dis = 10d0*BHMass)
      !parameter   ( R_dis = 15d0*BHMass)
      ! Ignore points with rho<floor:
      parameter   ( floor    = 1.d-7)
      ! Regularize the argument to arcsin? (else just throw point away)
      logical       reg_arg_arcsin
      parameter   ( reg_arg_arcsin = .true. )
      ! Regularize the argument to sqroot? (else just throw point away)
      logical       reg_arg_sqrt
      parameter   ( reg_arg_sqrt = .true. )
      ! Patrick ignores points within 1.5 R_isco of the BH:
      real*8        cutoffradius
      !parameter   ( cutoffradius = R_dis)
      parameter   ( cutoffradius = 3.d0*BHMass)
      ! Rescaled value of G for the run being processed:
      real*8        G_SCALEFACTOR
      parameter   ( G_SCALEFACTOR = 1000.d0)
      !
      ! Patrick does not take absolute values for the fallback time
      ! but rather chooses ordering to ensure positive fallback times:
      !
      logical       patrickstyle
      parameter   ( patrickstyle = .true. )
      !

      logical       trace,           trace2, outputascii, quiet
      parameter   ( trace = .false. )
      parameter   ( trace2= .false. )
      parameter   ( outputascii= .true. )
      !
      ! This needs to be done once first, before making histogram:
      !
      logical       makebboxesfile
      parameter   ( makebboxesfile = .false. )
      ! Do you want messages about numerical problems written out?
      parameter   ( quiet= .true. )
      ! HISTOGRAM parameters:
      ! Working in log space on the x-axis:
      integer       numbins
      parameter   ( numbins = 20 )
      !parameter   ( numbins = 10 )
      real*8        histo(2*numbins),  histo_max, histo_min, logtau,
     .              histoc(2*numbins), dhisto
      parameter   (                    histo_max = 5.d0)
      parameter   (                    histo_min =-2.d0)

      found_finesth = .false.
c      ### usage statement
       if (iargc() .lt. 1) then
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       fallback <max proc[=0]>'
          write(*,*) '                              '
          write(*,*) '       NB: --must be run twice to establish the'
          write(*,*) '             finest resolution at a given point'
          write(*,*) '             The first time you need to compile'
          write(*,*) '             makebboxesfile=.true.'             
          write(*,*) '        --the histogram is stored in *histo.sdf'
          write(*,*) '             where the asterisk is the largest'
          write(*,*) '             number associated with input data'
          stop
       else
          write(*,*)'fallback: Compiled in options:'
          write(*,*)'fallback: BHMass:      ',BHMass
          write(*,*)'fallback: BHcent_x/y/z:',BHcent_x,BHcent_y,BHcent_z
          write(*,*)'fallback: R_dis:       ',R_dis
          write(*,*)'fallback: floor:       ',floor
          write(*,*)'fallback: reg_arg_arcsin:',reg_arg_arcsin
          write(*,*)'fallback: reg_arg_sqrt:  ',reg_arg_sqrt
          write(*,*)'fallback: cutoffradius:  ',cutoffradius
          write(*,*)'fallback: patrickstyle:  ',patrickstyle
          write(*,*)'fallback: G_SCALEFACTOR: ',G_SCALEFACTOR
       end if

      maxproc = i4arg(1,0)

      if (.true.) then
         ! Setup histogram:
         dhisto = (histo_max-histo_min) / (numbins-1)
         do i = 1, numbins
            histo(2*i)    = 0.d0
            histo(2*i-1)  = histo(2*i)
            histoc(2*i)   = histo_min + dhisto*(i-1)
            histoc(2*i+1) = histoc(2*i)
            write(*,*) 'histo: ',i,histoc(2*i),dhisto
         end do
      end if

      rank        = 3
      numproc     = 0
      unboundmass = 0.d0
      boundmass   = 0.d0
      totalmass   = 0.d0
      numdatasets = 0
      numbad      = 0
      totalnumpoints = 0

 333  continue

      write(*,*) 'fallbacK: ---------------------'
      write(*,*) 'fallbacK: Dealing with processor: ',numproc
      call int2str(numproc,prefix)
      lenpre              = mystringlength(prefix)
      gft_name(1:lenpre)  = prefix(1:lenpre)
      gft_name(lenpre+1:) = 'rho0'

      !
      ! Step through all data sets:
      !
      step   = 0
      gf3_rc = 1
 88   do while (gf3_rc .eq. 1)
         ! Initialize some statistics for each data set:
         setmass    = 0.d0
         numfloored = 0
         numcutoff  = 0
         !
         step   = step + 1
         gft_name(lenpre+1:) = 'rho0'
         gf3_rc = gft_read_full( gft_name,  step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   rho           )
         if (gf3_rc .eq. 1) then
            numdatasets = numdatasets + 1
            write(*,*) '                Step: ',step
            if ( ((shape(1)*shape(2)*shape(3).gt.MAXPOINTS)
     .                        .or.
     .               ((shape(1)+shape(2)+shape(3)).gt.MAXCOORDS))
     .          ) then
                write(*,*) 'fallbacK: MAXPOINTS/COORDS:',MAXPOINTS,
     .                                                   MAXCOORDS
                write(*,*) 'fallbacK: numpoints: ',
     .                                  shape(1)*shape(2)*shape(3)
                write(*,*) 'fallbacK: numcoords: ',
     .                                  shape(1)+shape(2)+shape(3)
                write(*,*) '3d data set too big for mem. allocated'
                stop
            end if
            !
            !
            gft_name(lenpre+1:) = 'vx0'
            gf3_rc = gft_read_full(gft_name,  step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   vx           )
            gft_name(lenpre+1:) = 'vy0'
            gf3_rc = gft_read_full(gft_name,  step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   vy           )
            gft_name(lenpre+1:) = 'vz0'
            gf3_rc = gft_read_full(gft_name,  step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   vz           )
            gft_name(lenpre+1:) = 'D0'
            gf3_rc = gft_read_full(gft_name,  step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   D0           )
            !
            !
            h  = coords(2)-coords(1)
            nz = shape(3)
            ny = shape(2)
            nx = shape(1)
            !
            ! Output bounding box and resolution
            ! to facilitate masking:
            !
            if (makebboxesfile) then
            open(unit=8,file='.bboxes',form='formatted',status='old',
     .           POSITION='APPEND')
            write(8,123)coords(1),coords(nx),coords(nx+1),coords(nx+ny),
     .                 coords(nx+ny+1),coords(nx+ny+nz),h
 123        format(7G11.5)
            close(8)
            end if
            !
            do k = 1, nz
            do j = 1, ny
            do i = 1, nx
               myindex = (k-1)*ny*nx+(j-1)*nx+i
               !
               x               = coords(i)
               y               = coords(j+nx)
               z               = coords(k+ny+nx)
               x = x- BHcent_x
               y = y- BHcent_y
               z = z- BHcent_z
               !
               r_i(myindex)    = sqrt(x**2+y**2+z**2)
               radius          = r_i(myindex)
               !
               ! It appears all of this calculation is
               ! independent of the mass, so set to 1:
               !
               m_i(myindex)    = rho(myindex)*h**3
               m_i(myindex)    = 1.d0
               vsq(myindex)    =  vx(myindex)**2
     .                          + vy(myindex)**2
     .                          + vz(myindex)**2
               KE_i(myindex)   = 0.5d0*m_i(myindex)*vsq(myindex)
               if (radius .ne. 0.d0) then
                  absV(myindex)= BHMass*m_i(myindex)/radius
               else
                  absV(myindex)= 0.d0
               end if
               vdotr(myindex)  =   vx(myindex)*x
     .                           + vy(myindex)*y
     .                           + vz(myindex)*z
               TE_i(myindex)   = KE_i(myindex) - absV(myindex)
               J_i(myindex)    = m_i(myindex)*(  vx(myindex)*y
     .                                         - vy(myindex)*x )
                  A(myindex)= 0.d0
                  C(myindex)= 0.d0
                  DD(myindex)= 0.d0
                  a_i(myindex)= 0.d0
                  e_i(myindex)= 0.d0
                  r_max(myindex)= 0.d0
                  mytmp_ri(myindex) = 0.d0
                  mytmp2_ri(myindex)= 0.d0
                  mytmp_rdis(myindex) = 0.d0
                  mytmp2_rdis(myindex)= 0.d0
                  mytmp_rmax(myindex) = 0.d0
                  mytmp2_rmax(myindex)= 0.d0
                  I_r_dis(myindex)= 0.d0
                  I_r_max(myindex)= 0.d0
                  I_r_i(myindex)= 0.d0
                  tau(myindex)= 0.d0
                  totalM(myindex)   = 0.d0
                  unboundM(myindex) = 0.d0
                  boundM(myindex)   = 0.d0
               if (radius.lt.cutoffradius) then
                  ! Ignore points close to BH:
                  numcutoff  = numcutoff  + 1
                  goto 777
               end if
               if (rho(myindex).lt.floor) then
                  ! Ignore points with density near floor:
                  numfloored = numfloored + 1
                  goto 777
               end if
               if (TE_i(myindex).lt.0) then
                  if (.true.) then
                     !totalM(myindex)   = rho(myindex)
                     !boundM(myindex)   = rho(myindex)
                     totalM(myindex)   = D0(myindex)
                     boundM(myindex)   = D0(myindex)
                     unboundM(myindex) = 0.d0
                     boundmass         = boundmass + m_i(myindex)
                  end if
                  !.......................
                  ! B O U N D   M A S S 
                  !.......................
                  numproblem   = .false.
                  tmpreal      = 1.d0 + 2.d0*TE_i(myindex)
     .                                        *J_i(myindex)**2
     .                                        /m_i(myindex)**3
     .                                        /BHMass**2      
                  if (tmpreal .ge. 0) then
                     e_i(myindex)    = sqrt( tmpreal)
                  else
                     if(.not.quiet)write(*,*)'fallback: e_i:',
     .                              i,j,k,tmpreal,TE_i(myindex)
                     e_i(myindex)    = 0.d0
                     numproblem      = .true.
                  end if
                  a_i(myindex)= -BHMass*m_i(myindex)/2.d0/TE_i(myindex)
                  r_max(myindex)  = a_i(myindex)*(1.d0+e_i(myindex))
                  if (a_i(myindex) .lt. 0) then
                     if(.not.quiet)write(*,*)'fallback: a_i:',
     .                              i,j,k,a_i(myindex),TE_i(myindex)
                     a_i(myindex)    = 0.d0
                     r_max(myindex)  = radius
                     numproblem      = .true.
                  else if (a_i(myindex) .gt. 1000) then
                     ! Put a ceiling on a_i():
                     a_i(myindex)    = 1000.d0
                     r_max(myindex)  = a_i(myindex)*(1.d0+e_i(myindex))
                  end if
                  !
                  A(myindex)      = 2.d0*TE_i(myindex)/m_i(myindex)
                  B               = 2.d0*BHMass
                  C(myindex)      = -J_i(myindex)**2/m_i(myindex)**2
                  DD(myindex)     = 4.d0*A(myindex)*C(myindex)-B**2
                  !
                  !
                  if (DD(myindex).lt.-SMALLNUMBER) then
                     tmpreal      =   A(myindex)*r_i(myindex)**2
     .                           + B*r_i(myindex)
     .                           + C(myindex) 
                     tmpreal2     = (2.d0*A(myindex)*r_i(myindex)+B)
     .                              / sqrt(-DD(myindex))
                     if (tmpreal.lt.0)tmpreal=0.d0
                     if (tmpreal.ge.0) then
                        mytmp_ri(myindex)  = tmpreal
                        mytmp2_ri(myindex) = tmpreal2
                        if (tmpreal2.gt.1) then
                           tmpreal2 = 1.d0
                        elseif (tmpreal2.lt.-1) then
                           tmpreal2 =-1.d0
                        end if
                        if (abs(tmpreal2).le.1) then
                          I_r_i(myindex)  = sqrt( tmpreal ) / A(myindex)
     .                           +(B/2.d0/A(myindex)/sqrt(-A(myindex)))
     .                            *asin( tmpreal2 )
                        else
                          if(.not.quiet)write(*,*)'fallback:Iri w/rel2',
     .                                    i,j,k,tmpreal2
                          I_r_i(myindex) = -1.d0
                          numproblem     = .true.
                        end if
                     else
                        if(.not.quiet)write(*,*)'fallback:probw/ I_r_i',
     .                                    i,j,k,tmpreal
                        I_r_i(myindex)  = -1.d0
                        numproblem      = .true.
                     end if
                     !
                     tmpreal      =   A(myindex)*r_max(myindex)**2
     .                           + B*r_max(myindex)
     .                           + C(myindex) 
                     tmpreal2     = (2.d0*A(myindex)*r_max(myindex)+B)
     .                              / sqrt(-DD(myindex))
                     if (tmpreal.lt.0.and.reg_arg_sqrt)tmpreal=0.d0
                     if (tmpreal.ge.0) then
                        mytmp_rmax(myindex)   = tmpreal
                        mytmp2_rmax(myindex)  = tmpreal2
                        if (tmpreal2.gt.1.and.reg_arg_arcsin) then
                           tmpreal2 = 1.d0
                        elseif (tmpreal2.lt.-1.and.reg_arg_arcsin) then
                           tmpreal2 =-1.d0
                        end if
                        if (abs(tmpreal2).le.1) then
                        I_r_max(myindex)= sqrt( tmpreal ) / A(myindex)
     .                           +(B/2.d0/A(myindex)/sqrt(-A(myindex)))
     .                            *asin( tmpreal2 )
                        else
                          if(.not.quiet)write(*,*)'fallback:Irmax/rel2',
     .                                    i,j,k,tmpreal2
                          I_r_max(myindex)  = -1.d0
                          numproblem        = .true.
                        end if
                     else
                        if (vdotr(myindex).gt.0 .and. .not.quiet)
     .                       write(*,*)'fallback:probw/I_r_max',
     .                                             i,j,k,tmpreal
                        I_r_max(myindex)  = -1.d0
                        numproblem        = .true.
                     end if
                     !
                     tmpreal      =   A(myindex)*R_dis**2
     .                           + B*R_dis
     .                           + C(myindex) 
                     tmpreal2     = (2.d0*A(myindex)*R_dis         +B)
     .                              / sqrt(-DD(myindex))
                     if (tmpreal.lt.0)tmpreal=0.d0
                     if (tmpreal.ge.0) then
                        mytmp_rdis(myindex)  = tmpreal
                        mytmp2_rdis(myindex) = tmpreal2
                        if (tmpreal2.gt.1) then
                           tmpreal2 = 1.d0
                        elseif (tmpreal2.lt.-1) then
                           tmpreal2 =-1.d0
                        end if
                        if (abs(tmpreal2).le.1) then
                        I_r_dis(myindex)= sqrt( tmpreal ) / A(myindex)
     .                           +(B/2.d0/A(myindex)/sqrt(-A(myindex)))
     .                            *asin( tmpreal2 )
                        else
                          if(.not.quiet)write(*,*)'fallback:Irdis/rel2',
     .                                    i,j,k,tmpreal2
                          I_r_dis(myindex)  = -1.d0
                          numproblem        = .true.
                        end if
                     else
                        mytmp_rdis(myindex)  = -1.d0
                        if(.not.quiet)write(*,*)
     .                        'fallback:problemw/I_r_dis',i,j,k,tmpreal
                        I_r_dis(myindex)  = -1.d0
                        numproblem        = .true.
                     end if
                     !
                  else
                     if(.not.quiet)write(*,*)'fallback:DD is positive!',
     .                                     i,j,k,DD(myindex)
                  end if
                  !
                  if (vdotr(myindex).gt.0.and. .not.numproblem) then
                    if (patrickstyle) then
                    tau(myindex)=     I_r_max(myindex)-I_r_i(myindex) 
     .                              - I_r_dis(myindex)+I_r_max(myindex) 
                    else
                    tau(myindex)= abs(I_r_i(myindex)  -I_r_max(myindex))
     .                          + abs(I_r_max(myindex)-I_r_dis(myindex))
                    end if
                  elseif (.not. numproblem) then
                    if (patrickstyle) then
                    tau(myindex)=    (I_r_i(myindex)  -I_r_dis(myindex))
                    else
                    tau(myindex)= abs(I_r_i(myindex)  -I_r_dis(myindex))
                    end if
                  else
                     ! If there was a problem, assign some arbitrary,
                     ! but apparent visually, value:
                     tau(myindex)= -10.d0
                     !numbad      = numbad + 1    ! will get counted later
                  end if
               else
                  !.......................
                  ! U N B O U N D   M A S S 
                  !.......................
                  !if(.not.quiet)write(*,*)'fallback: mass unbound'
                  if (.true.) then
                     !totalM(myindex)   = rho(myindex)
                     !unboundM(myindex) = rho(myindex)
                     totalM(myindex)   = D0(myindex)
                     unboundM(myindex) = D0(myindex)
                     boundM(myindex)   = 0.d0
                     unboundmass       = unboundmass + m_i(myindex)
                  end if
                  A(myindex)= 0.d0
                  C(myindex)= 0.d0
                  DD(myindex)= 0.d0
                  a_i(myindex)= 0.d0
                  e_i(myindex)= 0.d0
                  r_max(myindex)= 0.d0
                  I_r_dis(myindex)= 0.d0
                  I_r_max(myindex)= 0.d0
                  I_r_i(myindex)= 0.d0
                  tau(myindex)= 0.d0
                  !
               end if
  777          continue
               !
            end do
            end do
            end do
            !
            ! Examine bounding box and resolution information
            ! to mask finer regions:
            !
            if (.true.) then
            if(trace)write(*,*)'---------------------------------------'
            if(trace)write(*,123)coords(1),coords(nx),coords(nx+1),
     .        coords(nx+ny), coords(nx+ny+1),coords(nx+ny+nz),h
               l = 0
               open(unit=8,file='.bboxes',form='formatted',status='old')
   9           read(8,123,END=10)minx,maxx,miny,maxy,minz,maxz,myh
               if(trace)write(*,123)minx,maxx,miny,maxy,minz,maxz,myh
               l = l + 1
               open(unit=8,file='.bboxes',form='formatted',status='old')
               if (myh.ge.h-SMALLNUMBER) then
                  if(trace )write(*,*)'fallback:ignoring coarsbox',myh,l
               else
                  if (minx.gt.coords(nx) .or. maxx.lt.coords(1)
     .                                      .or.
     .                miny.gt.coords(nx+ny).or.maxy.lt.coords(nx+1)
     .                                      .or.
     .                minz.gt.coords(nx+ny+nz).or.
     .                maxz.lt.coords(nx+ny+1)  ) then
                     if(trace )write(*,*)'fallback: no intersec ',myh,l
                  else
                     if(trace )write(*,*)'fallback: MASKING:    ',myh,l
                     nummasked = 0
                     do k = 1,nz
                     do j = 1,ny
                     do i = 1,nx
                        myindex = (k-1)*ny*nx+(j-1)*nx+i
                        !
                        x               = coords(i)
                        y               = coords(j+nx)
                        z               = coords(k+ny+nx)
                        !
                        if ( x .ge. minx .and. x .le. maxx .and.
     .                       y .ge. miny .and. y .le. maxy .and.
     .                       z .ge. minz .and. z .le. maxz       ) then
                           tau(myindex) = -1.d0
                           if(trace)write(*,*)'maskg point:',i,j,k,x,y,z
                           nummasked = nummasked + 1
                        end if
                        !
                     end do
                     end do
                     end do
                     if(trace)write(*,*)'fallback: nummasked=',nummasked
                  end if
               end if
            if(trace)write(*,*)'---------------------------------------'
               goto 9
   10          continue
               if(trace)write(*,*)'fallback: No more boxes. Done.'
               close(8)
            end if
            !
            ! At this point, we have:
            !    1) computed tau
            !    2) masked out regions covered by finer regions
            ! And we now have to histogram things:
            !
            if (.true.) then
               do k = 1,nz
               do j = 1,ny
               do i = 1,nx
                  myindex = (k-1)*ny*nx+(j-1)*nx+i
                  !
                  if (double_equal(tau(myindex),-1.d0)) then
                     if(trace)write(*,*)'fallback:Masked point'
                     ! do not count towards total mass
                  elseif (tau(myindex) .lt. 0.d0) then
                     numbad      = numbad + 1
                     if(.false.)write(*,*)'fallback:bad point',
     .                    tau(myindex), numbad
                  else
                     setmass   = setmass   + D0(myindex)*h**3
                     totalmass = totalmass + D0(myindex)*h**3
                     if ( tau(myindex) .gt. 0) then
                        if(trace)write(*,*)'fallback:Only unmasked pts'
                        ! Find out which bin tau falls in
                        ! and then add mass to that bin:
                        !logtau = log(tau(myindex))
                        logtau = log10(tau(myindex))
                        if(logtau.ge.histo_min .and.
     .                     logtau.le.histo_max)then
                           l            = (logtau-histo_min) /dhisto + 1
                           histo(2*l)   = histo(2*l) + D0(myindex)*h**3
                           histo(2*l-1) = histo(2*l)
                           if(trace)write(*,*)'hist',l,logtau,histo(2*l)
                        else
                           write(*,*)'fallback: Some material outside '
                           write(*,*)'fallback: bounds of your histo.'
                           write(*,*)'fallback: logtau = ',logtau
                           write(*,*)'fallback: histo_min = ',histo_min
                           write(*,*)'fallback: histo_max = ',histo_max
                        end if
                     end if
                  end if
                  !
               end do
               end do
               end do
             end if
            !
            if (.true.) then
               totalnumpoints = totalnumpoints + nx*ny*nz
               write(*,*)'                   numbad:',numbad,
     .                 100.d0*numbad/totalnumpoints,totalnumpoints
               write(*,*)'              numdatasets:',numdatasets
               write(*,*)'        numfloored/cutoff:',
     .                                      numfloored,numcutoff
               write(*,*)'        numfloored/cutoff:',
     .                                 100.d0*numfloored/nx/ny/nz,
     .                                 100.d0*numcutoff /nx/ny/nz
               write(*,*)'                     mass:',setmass,totalmass
            end if
            if (.false.) then
            if (trace) write(*,*) 'fallback: Outputting data:'
            gft_name(lenpre+1:) = 'tau'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, tau)
            gft_name(lenpre+1:) = 'I_rdis'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, I_r_dis)
            gft_name(lenpre+1:) = 'I_rmax'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, I_r_max)
            gft_name(lenpre+1:) = 'I_ri'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, I_r_i)
            gft_name(lenpre+1:) = 'a_i'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, a_i)
            gft_name(lenpre+1:) = 'e_i'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, e_i)
            gft_name(lenpre+1:) = 'TE_i'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, TE_i)
            gft_name(lenpre+1:) = 'absV'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, absV)
            gft_name(lenpre+1:) = 'J_i'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, J_i)
            gft_name(lenpre+1:) = 'm_i'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, m_i)
            gft_name(lenpre+1:) = 'r_i'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, r_i)
            gft_name(lenpre+1:) = 'r_max'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, r_max)
            gft_name(lenpre+1:) = 'A'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, A)
            gft_name(lenpre+1:) = 'C'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, C)
            gft_name(lenpre+1:) = 'DD'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, DD)
            gft_name(lenpre+1:) = 'KE_i'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, KE_i)
            gft_name(lenpre+1:) = 'vsq'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, vsq)
            gft_name(lenpre+1:) = 'vdotr'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, vdotr)
            gft_name(lenpre+1:) = 'finesth'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, finesth)
            gft_name(lenpre+1:) = 'boundM'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, boundM)
            gft_name(lenpre+1:) = 'unbndM'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, unboundM)
            gft_name(lenpre+1:) = 'totalM'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, totalM)
            gft_name(lenpre+1:) = 'mytmp_ri'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, mytmp_ri)
            gft_name(lenpre+1:) = 'mytmp_rmax'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, mytmp_rmax)
            gft_name(lenpre+1:) = 'mytmp_rdis'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, mytmp_rdis)
            gft_name(lenpre+1:) = 'mytmp2_ri'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, mytmp2_ri)
            gft_name(lenpre+1:) = 'mytmp2_rmax'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, mytmp2_rmax)
            gft_name(lenpre+1:) = 'mytmp2_rdis'
            gf3_rc = gft_out_full(gft_name, time, shape, cnames,
     .                                     rank, coords, mytmp2_rdis)
            end if
            !
            gft_name(lenpre+1:) = 'histo'
            gf3_rc = gft_out_full(gft_name, time, 2*numbins, 'logtau',
     .                                     1, histoc, histo)
            !
            !
            end if
      end do

      if (numproc .lt. maxproc) then
         gf3_rc  = gft_close_all()
         numproc = numproc + 1
         goto 333
      end if

!     write(*,*) 'fallback: Mass stats:'
      write(*,*) 'fallback: total   mass: ',totalmass
      write(*,*) 'fallback: total   numdatasets: ',numdatasets
!     write(*,*) 'fallback: unbound mass: ',unboundmass,
!    .                                    100.d0*unboundmass/totalmass
!     write(*,*) 'fallback:   bound mass: ',  boundmass,
!    .                                    100.d0*  boundmass/totalmass
 800  continue
      end






