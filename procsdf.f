c     program procsdf
      implicit none

c     ## name of output file
      character*2   APPEND
		parameter   ( APPEND = '_p')

      character*32  name,   nameout, tcnames(3)
      character*128 sarg,   cline, cnames, tnames
      integer       mxliv,  junk, tcnames_len(3)
      parameter   ( mxliv = 500 000 )
      real*8        LARGENUMBER
      parameter   ( LARGENUMBER = 1.0d68)
      integer       shape(3),gft_out_full,tshape(3),gft_read_rank,
     .              gf3_rc,gft_read_full, rank, nx, ny, nz,
     .              j, MAXTIMES, stride, irdivn, ivindx,gft_read_shape,
     .              iv(mxliv), liv, key, x,y,z, trank,vsrc,vsxynt,
     .              x0,y0,z0
      integer       step, i4arg, iargc, MAX_POS_RANK, i,k,l,
     .              maxi,maxj,maxk, mini,minj,mink
		parameter   ( MAXTIMES = 100,  MAX_POS_RANK = 3, key = -100 )
      integer       MAXCOORDS,         MAXPOINTS
      parameter   ( MAXCOORDS = 50002,  MAXPOINTS = 34 000 000 )
      real*8         data(MAXPOINTS), coords(MAXCOORDS), time,
     .              tdata(MAXPOINTS),tcoords(MAXCOORDS)

      real*8        l2norm, maxdata, mindata,temp, 
     .                      maxdata_rho, maxdata_z,
     .                      mindata_rho, mindata_z
      logical       trace,           trace2
      parameter   ( trace = .false. )
c     parameter   ( trace = .true.  )
      parameter   ( trace2= .false. )
c     parameter   ( trace2= .true. )

c      ### usage statement
       if (iargc() .lt. 2) then
          write(*,*) '*************************************************'
          write(*,*) '* Current caveats: slices any data.             *'
          write(*,*) '*                only cuts 2d data.             *'
          write(*,*) '*************************************************'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       procsdf  <sdf filename> <outputvector>'
          write(*,*) '               ',
     .                         '[<1 index=-1><2 index=-1><3 index=-1>]'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Output:                                       *'
          write(*,*) '*************************************************'
          write(*,*) '       sdf file <sdf filename>_p.sdf'
          write(*,*) ''
          write(*,*) '*************************************************'
          write(*,*) '* Sample usage:                                 *'
          write(*,*) '*************************************************'
          write(*,*) ''
          write(*,*) '   procsdf 2d_Phi_2 . 1' 
          write(*,*) ''
          write(*,*) '       creates 1d sdf from 2d with first',
     .                      ' coordinate constant'
          write(*,*) ''
          write(*,*) '                *****************'
          write(*,*) '   procsdf 2d_Phi_2 . 0 0'
          write(*,*) ''
          write(*,*) '       outputs the l2 norm of each 2d slice'
          write(*,*) ''
          write(*,*) '                *****************'
          write(*,*) '   procsdf 2d_Phi_2 . 0 '
          write(*,*) ''
          write(*,*) '       creates 1d sdf file with first coordinate'
          write(*,*) '       fixed at index value one half its max'
          write(*,*) ''
          write(*,*) '                *****************'
          write(*,*) '   procsdf 2d_Phi_2 . 1 1'
          write(*,*) ''
          write(*,*) '       outputs the value of the grid ',
     .                      ' function at the point Phi(1,1)'
          write(*,*) '                *****************'
          write(*,*) '   procsdf 3d_Phi_2  1-10/2'
          write(*,*) ''
          write(*,*) '       creates 3D sdf file with only time',
     .                      ' slices 2,4,6,8,10'
          write(*,*) ''
          write(*,*) '                *****************'
          write(*,*) '   procsdf 3d_Phi_2  . 0 0 0'
          write(*,*) ''
          write(*,*) '       outputs l2norm, min and max of data ',
     .                      ' sets for all time slices'
          write(*,*) ''
          write(*,*) '                *****************'
          stop
       end if

      do i = 1 , 3
         call sload(name,' ')
         call sload(cnames,' ')
         call sload(tcnames(i),' ')
         call sload(tnames,' ')
      end do

c      ### read in arguments
      name   = sarg(1,'2d_Phi_3.sdf')
      cline  = sarg(2,'*-*')
      x0     = i4arg(3,-1)
      y0     = i4arg(4,-1)
      z0     = i4arg(5,-1)
      stride = 1

      if (trace) then
         write(*,*) '*******************************'
         write(*,*) 'procsdf: Reading in file ',name
         write(*,*) 'procsdf: Outputting times ', cline
         write(*,*) 'procsdf: x=',x0
         write(*,*) 'procsdf: y=',y0
         write(*,*) 'procsdf: z=',z0
         write(*,*) '*******************************'
      end if
c      ### set up output vector
      if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) then
c         write(*,*) 'procsdf: using *-* as index vector'
          cline = '*-*'
          if( irdivn(iv,liv,mxliv,1,mxliv,cline) .lt. -1 ) goto 800
      end if

c     if (trace2) call ivdump(iv,liv,'index vector',6)


c     ### get rank
      if (trace2) then
          write(*,*) 'procsdf:  Reading in sdf file...'
          write(*,*) '          name: ', name
      end if
      step = 1
      gf3_rc = gft_read_rank(name,step,rank)
      if (gf3_rc.ne.1) then
         write(*,*) 'procsdf: Unable to read ', name
         write(*,*) 'procsdf: gf3_rc = ', gf3_rc
         write(*,*) 'procsdf: Quitting....'
         goto 800
      end if
      if (trace2) then
          write(*,*) 'procsdf:  Finished reading in sdf file...'
          write(*,*) 'procsdf:  rank = ', rank
      end if

      junk = index(name,'.sdf')
      if (junk.eq.0) then
         junk = index(name,' ')
      end if
      if (trace2) write(*,*) '    junk = ', junk
      nameout(1:junk-1) = name(1:junk)
      nameout(junk:)    = APPEND
      if (trace2) write(*,*) ' outputing to: ', nameout

      step = 0
      do while (.true.)
         step = step + 1
 		   if (trace2) then
            write(*,*) '..........step ', step
            write(*,*) '..........liv  ', liv 
            write(*,*) '..........iv(1)', iv(1) 
            if ( ivindx(iv,liv,step) .gt. 0 ) 
     .               write(*,*) '          step in index vector'
            write(*,*) '       reading in sdf'
         end if
         gf3_rc = gft_read_shape( name,     step,   shape)
         ! important for call to checkmem:
         if (rank.lt.3) shape(3) = 1
         if (rank.lt.2) shape(2) = 1
         call checkmem(shape(1),shape(2),shape(3),MAXPOINTS,MAXCOORDS)
         gf3_rc = gft_read_full(  name,     step,   shape,
     .                           cnames,   rank,   time,
     .                           coords,   data           )
		   if (gf3_rc .eq. 1) then

            k              = INDEX(cnames, '|')
            tcnames(1)     = cnames(:k-1)
            tcnames_len(1) = k-1
            j          = k+1
            do i = 2, rank-1
               k              = INDEX(cnames(j:), '|') + j - 1
               tcnames(i)     = cnames(j:k-1)
               tcnames_len(i) = k-j 
               j              = k+1
            end do
            tcnames(rank)     = cnames(j:)
            tcnames_len(rank) = INDEX(tcnames(rank),' ') - 1

            if (trace2) then
               do i = 1, rank
                  write(*,*) tcnames(i), tcnames_len(i),i
               end do
            end if

            if (trace2) then
               write(*,*) '..........successfully read'
               write(*,*) '..........liv  ', liv 
               write(*,*) '..........iv(1)', iv(1) 
               write(*,*) '           step:',step
               write(*,*) '           time:',time
               write(*,*) '           rank:',rank
               write(*,*) '           shape    coord name:'
               do i = 1, rank
				       write(*,*) '                 ',shape(i),
     .                  '   ',tcnames(i)
               end do
            end if
            if ( ivindx(iv,liv,step) .gt. 0 ) then
      x = x0
      y = y0
      z = z0
      if (trace2.and.rank.eq.3) write(*,*)'procsdf: x y z ',x,y,z
      if ((rank.eq.2).and.((x0.ne.0).or.(y0.ne.0))) then
          if (x0.eq.0) then
              x = nint(shape(1)/2.d0)
          else if (y0.eq.0) then
              y = nint(shape(2)/2.d0)
          end if
          if (trace2) write(*,*)'procsdf: x and y ',x,y
      else if 
     . ((rank.eq.3).and.((x0.ne.0).or.(y0.ne.0).or.(z0.ne.0))) then
          if (x0.eq.0) then
              x = nint(shape(1)/2.0d0)
          end if
          if (y0.eq.0) then
              y = nint(shape(2)/2.0d0)
          end if
          if (z0.eq.0) then
              z = nint(shape(3)/2.0d0)
          end if
          if (trace2) write(*,*)'procsdf: x and y and z ',x,y,z
      end if

		          if (trace2) 
     .               write(*,*) '..........step in index vector'
c               ################################
c               ###  2 Dimensional sdf files ###
c               ################################
                if (rank.eq.2) then
c
c                  Calculate L2 norm of 2d time slice
c
                   if ((x.eq.0).and.(y.eq.0)) then
                      if (trace) write(*,*) 
     .                   'procsdf:   calculating l2 norm of slice'
                      maxdata = -LARGENUMBER
                      mindata =  LARGENUMBER
                      l2norm = 0.0d0
                      do i = 1, shape(1)
                         do j = 1, shape(2)
                            l2norm = 
     .                          l2norm + data((j-1)*shape(1)+i)**2
                            if (data((j-1)*shape(1)+i).gt.maxdata) then
                                maxdata     = data((j-1)*shape(1)+i)
                                maxdata_z   = coords(shape(1)+j)
                                maxdata_rho = coords( i)
                            end if
                            if (data((j-1)*shape(1)+i).lt.mindata) then
                                mindata = data((j-1)*shape(1)+i)
                                mindata_z   = coords(shape(1)+j)
                                mindata_rho = coords( i)
                            end if
                         end do
                      end do
                      if (shape(1)*shape(2).ne.0)
     .                l2norm = sqrt( l2norm / (1.0d0*shape(1)*shape(2)))
                      write(*,225) time, l2norm,
     .                           maxdata, maxdata_z, maxdata_rho,
     .                           mindata, mindata_z, mindata_rho
!225                  format(1p,8E12.4)
!225                  format(1p,G19.12,7E12.4)
 225                  format(1p,G20.13,7E12.4)
c
c                  Output un-doctored time slice
c
                   else
     .             if ((x.lt.0).and.(y.lt.0)) then
                      if (trace) write(*,*) 
     .                   'procsdf:   outputting sdf file: ', nameout

			             gf3_rc = gft_out_full(nameout, time, shape, cnames, 
     .                                     rank, coords, data)
                      if (trace) then
		                   if (gf3_rc .eq. 1) then
                            write(*,*) "procsdf: successful"
		                   else
                           write(*,*) "procsdf: unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output 1d slice y = constant
c
                   else if (x.lt.1) then
                      if (trace) 
     .                write(*,*) 'procsdf: I  have constant 2st coord',y

                      do i = 1,shape(1)
                         tdata(i)   = data( (y-1)*shape(1)+i )
                         tcoords(i) = coords(i)
                      end do
                      trank      = rank - 1
                      tshape(1)  = shape(1)
                      tnames     = tcnames(1)
                    gf3_rc = gft_out_full(nameout, time, tshape, tnames,
     .                                     trank, tcoords, tdata)
                      vsrc=vsxynt(nameout,time,tcoords,tdata,tshape(1))
                      if (trace) then
		                   if (gf3_rc .eq. 1) then
                            write(*,*) "procsdf: successful"
		                   else
                           write(*,*) "procsdf: unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output 1d slice x = constant
c
                   else if (y.lt.1) then
                      if (trace) 
     .                write(*,*) 'procsdf: I  have constant 1st coord',x
                      do j = 1,shape(2)
                         tdata(j)   = data( (j-1)*shape(1)+x )
                         tcoords(j) = coords(shape(1)+j)
                      end do
c                     do j = 1, shape(1)+shape(2)
c                        write(*,*) j, coords(j)
c                     end do
c                     write(*,*) coords(shape(1)+1),
c    .                           coords(shape(1)+shape(2)),shape(1)
c                     write(*,*) 'y = ',tcoords(1),tcoords(shape(2)),
c    .                           shape(2)
                      if (trace) write(*,*) tcoords(shape(2))
                      trank      = rank - 1
                      tshape(1)  = shape(2)
                      tnames     = tcnames(2)
                    gf3_rc = gft_out_full(nameout, time, tshape, tnames,
     .                                     trank, tcoords, tdata)
                      vsrc=vsxynt(nameout,time,tcoords,tdata,tshape(1))
                      if (trace) then
		                   if (gf3_rc .eq. 1) then
                            write(*,*) "procsdf: successful"
		                   else
                           write(*,*) "procsdf: unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output ASCII x = constant y = constant
c
                   else 
                      if (trace) write(*,*) 'procsdf: outputting ASCII'
                      if ( (x.gt.shape(1)).or.(y.gt.shape(2))) then
                         write(*,*) 'procsdf: Error. Point (x,y) ', x,y,
     .                     ' outside data bounds (nx,ny)',
     .                     shape(1), shape(2)
                      else
                         write(*,100)  time,
     .                           data( (y-1)*shape(1) + x )
  100                    format(2G20.9)
                      end if
                   end if

c               ################################
c               ###  3 Dimensional sdf files ###
c               ################################
                else if (rank.eq.3) then
c
c                  Calculate L2 norm of 3D time slice
c
                   if ( (x.eq.0).and.(y.eq.0).and.(z.eq.0) ) then
                      if (trace2) write(*,*)
     .                   '           getting statitics'
                      l2norm  =  0.0d0
                      maxdata = -LARGENUMBER
                      mindata =  LARGENUMBER
                      do i = 1, shape(1)
                         do j = 1, shape(2)
                            do k = 1, shape(3)
                               temp =
     .               data((k-1)*shape(2)*shape(1) +(j-1)*shape(1)+i)
                               l2norm = l2norm + temp**2
                               if (temp.gt.maxdata) then
                                   maxi    = i
                                   maxj    = j
                                   maxk    = k
                                   maxdata = temp
                                end if
                                if (temp.lt.mindata) then
                                   mini    = i
                                   minj    = j
                                   mink    = k
                                   mindata = temp
                                end if
                            end do
                         end do
                      end do
                      if ((shape(1)*shape(2)*shape(3)).ne.0)
     .                              l2norm = sqrt( l2norm 
     .                            / (1.0d0*shape(1)*shape(2)*shape(3)))
                      write(*,226) time, l2norm,
     .            maxdata, coords(maxi),
     .            coords(shape(1)+maxj), coords(shape(1)+shape(2)+maxk),
     .            mindata, coords(mini),
     .            coords(shape(1)+minj), coords(shape(1)+shape(2)+mink)
 226                  format(1p,10E12.4)
c
c                  Output un-doctored 3D time slice
c
                   else if ( (x.lt.0).and.(y.lt.0).and.(z.lt.0) ) then
                      if (trace) write(*,*)
     .                   '           outputting 3D sdf file: ', nameout
                      gf3_rc = gft_out_full(nameout, time, shape, 
     .                                     cnames, rank, coords, data)
                      if (trace) then
                         if (gf3_rc .eq. 1) then
                            write(*,*) "         successful writing"
                         else
                           write(*,*) "procsdf: unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output (z = constant) 2D slice
c
                   else if ( (x.lt.0).and.(y.lt.0).and.(z.gt.0) ) then
                      if (trace) 
     .                write(*,*) '         I  have constant 3rd coord',z
                      do j = 1, shape(2)
                         do i = 1, shape(1)
                           tdata( i + (j-1)*shape(1) )   = 
     .                data((z-1)*shape(1)*shape(2)+(j-1)*shape(1)+i)
                         end do
                      end do
                      do i = 1, shape(1)
                           tcoords(i) = coords(i)
                      end do
                      do j = 1, shape(2)
                           tcoords( shape(1) + j ) = 
     .                                    coords( shape(1) + j)
                      end do
                      trank      = 2
                      tshape(1)  = shape(1)
                      tshape(2)  = shape(2)
                      tnames     = tcnames(1)(:tcnames_len(1))
     .                             //'|'//
     .                             tcnames(2)(:tcnames_len(2))
                      gf3_rc = gft_out_full(nameout, time,tshape,tnames,
     .                                     trank, tcoords, tdata)

                      if (trace) then
		                   if (gf3_rc .eq. 1) then
                            write(*,*) "         successful writing"
		                   else
                           write(*,*) "         unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output (y = constant) 2D slice
c
                   else if ( (x.lt.0).and.(z.lt.0).and.(y.gt.0) ) then
                      if (trace) 
     .                write(*,*) '         I  have constant 2rd coord',y
                      do k = 1, shape(3)
                         do i = 1, shape(1)
                           tdata( i + (k-1)*shape(1) )   = 
     .                    data((k-1)*shape(1)*shape(2)+(y-1)*shape(1)+i)
                         end do
                      end do
                      do i = 1, shape(1)
                           tcoords(i) = coords(i)
                      end do
                      do k = 1, shape(3)
                           tcoords( shape(1) + k ) = 
     .                                    coords( shape(1)+shape(2)+k )
                      end do
                      trank      = 2
                      tshape(1)  = shape(1)
                      tshape(2)  = shape(3)
                      tnames     = tcnames(1)(:tcnames_len(1))
     .                             //'|'//
     .                             tcnames(3)(:tcnames_len(3))
                    gf3_rc = gft_out_full(nameout, time, tshape, tnames,
     .                                     trank, tcoords, tdata)
                      if (trace) then
		                   if (gf3_rc .eq. 1) then
                            write(*,*) "         successful"
		                   else
                           write(*,*) "         unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output (x = constant) 2D slice
c
                   else if ( (y.lt.0).and.(z.lt.0) ) then
                      if (trace) 
     .                write(*,*) '         I  have constant 1rd coord',x
                      do k = 1, shape(3)
                         do j = 1, shape(2)
                           tdata( (k-1)*shape(2) + j)   = 
     .                    data((k-1)*shape(1)*shape(2)+(j-1)*shape(1)+x)
                         end do
                      end do
                      do i = 1, shape(1)
                           tcoords(i) = coords(i)
                      end do
                      do k = 1, shape(3)
                           tcoords( shape(1) + k ) = 
     .                                    coords( shape(1)+shape(2)+k)
                      end do
                      trank      = 2
                      tshape(1)  = shape(2)
                      tshape(2)  = shape(3)
                      tnames     = tcnames(2)(:tcnames_len(2))
     .                             //'|'//
     .                             tcnames(3)(:tcnames_len(3))
                    gf3_rc = gft_out_full(nameout, time, tshape, tnames,
     .                                     trank, tcoords, tdata)
                      if (trace) then
		                   if (gf3_rc .eq. 1) then
                            write(*,*) "         successful"
		                   else
                           write(*,*) "         unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output (x = constant, y = constant) 1D slice
c
                   else if ( (z.lt.0) ) then
                      if (trace) 
     .                   write(*,*) '         I  have constant 1st and',
     .                           ' 2nd coord',x,y
                      do k = 1, shape(3)
                           tdata( k )   = 
     .                    data((k-1)*shape(1)*shape(2)+(y-1)*shape(1)+x)
                      end do
                      do k = 1, shape(3)
                           tcoords( k ) = 
     .                                coords( shape(1)+shape(2)+k )
                      end do
                      trank      = 1
                      tshape(1)  = shape(3)
                      tnames     = tcnames(3)
 		             gf3_rc = gft_out_full(nameout, time, tshape, tnames, 
     .                                     trank, tcoords, tdata)
                      if (trace) then
 	                   if (gf3_rc .eq. 1) then
                            write(*,*) "         successful writing"
 	                   else
                           write(*,*) "         unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output (x = constant, z = constant) 1D slice
c
                   else if ( (y.lt.0) ) then
                      if (trace) 
     .                   write(*,*) '         I  have constant 1st and',
     .                           ' 3rd coord',x,z
                      do j = 1, shape(2)
                           tdata( j )   = 
     .                    data((z-1)*shape(1)*shape(2)+(j-1)*shape(1)+x)
                      end do
                      do j = 1, shape(2)
                           tcoords( j ) = 
     .                                coords( shape(1)+j )
                      end do
                      trank      = 1
                      tshape(1)  = shape(2)
                      tnames     = tcnames(2)
 		             gf3_rc = gft_out_full(nameout, time, tshape, tnames, 
     .                                     trank, tcoords, tdata)
                      if (trace) then
 	                   if (gf3_rc .eq. 1) then
                            write(*,*) "         successful writing"
 	                   else
                           write(*,*) "         unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output (y = constant, z = constant) 1D slice
c
                   else if ( (x.lt.0) ) then
                      if (trace) 
     .                   write(*,*) '         I  have constant 2nd and',
     .                           ' 3rd coord',y,z
                      do i = 1, shape(1)
                           tdata( i )   = 
     .                    data((z-1)*shape(1)*shape(2)+(y-1)*shape(1)+i)
                      end do
                      do i = 1, shape(1)
                           tcoords( i ) = 
     .                                coords( i )
                      end do
                      trank      = 1
                      tshape(1)  = shape(1)
                      tnames     = tcnames(1)
 		             gf3_rc = gft_out_full(nameout, time, tshape, tnames, 
     .                                     trank, tcoords, tdata)
                      if (trace) then
 	                   if (gf3_rc .eq. 1) then
                            write(*,*) "         successful writing"
 	                   else
                           write(*,*) "         unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
c
c                  Output ASCII point 
c
                   else 
                      if ( (x.gt.shape(1))
     .                         .or.
     .                     (y.gt.shape(2))
     .                         .or.
     .                     (z.gt.shape(3)) ) then
                         write(*,*) 'procsdf: Error. Point (x,y,z) ', x,y,z,
     .                     ' outside data bounds (nx,ny,nz)',
     .                     shape(1), shape(2), shape(3)
                      else
                         if (trace) 
     .                      write(*,*) '         Outputting ascii',
     .                              ' data points',x,y,z
c                        write(*,200)  coords(x),  coords(shape(1)+y), 
c    .                               coords(shape(1) + shape(2) + z),
c    .                 data( (z-1)*shape(1)*shape(2)+(y-1)*shape(1)+x),
c    .                       x,y,z
                         write(*,220)  time,
     .                 data( (z-1)*shape(1)*shape(2)+(y-1)*shape(1)+x)
  200                    format(4G16.4,3I3)
  220                    format(2G16.4)
                      end if
                   end if
c               ################################
c               ###  1 Dimensional sdf files ###
c               ################################
                else if (rank.eq.1) then

                   if (x.eq.0) then
                      if (trace) write(*,*) 
     .                   'procsdf:   calculating l2 norm of slice'
                      maxdata = -LARGENUMBER
                      mindata =  LARGENUMBER
                      l2norm  = 0.0d0
                      do i = 1, shape(1)
                            l2norm = 
     .                          l2norm + data(i)**2
                            if (data(i).lt.mindata) then
                               mindata     = data(i)
                               mindata_rho = coords(i)
                            end if
                            if (data(i).gt.maxdata) then
                               maxdata     = data(i)
                               maxdata_rho = coords(i)
                            end if
                      end do
                      if (shape(1).ne.0)
     .                l2norm = sqrt( l2norm / (1.0d0*shape(1)))
                      write(*,227) time, l2norm, maxdata, maxdata_rho,
     .                                         mindata, mindata_rho
 227                  format(1p,G14.6,6E15.6)
                   else if  (x.lt.0) then
                      if (trace) write(*,*) 
     .                   'procsdf:   1D outputting sdf file: ', nameout

 		             gf3_rc = gft_out_full(nameout, time, shape, cnames, 
     .                                     rank, coords, data)
                      if (trace) then
 	                   if (gf3_rc .eq. 1) then
                            write(*,*) "procsdf: successful"
 	                   else
                           write(*,*) "procsdf: unsuccessful writing",
     .                             " file: ",nameout
                         end if
                      end if
                   else if ( (x.gt.0).and.(x.le.shape(1)) ) then
                      write(*,*) time, data(x)
                   end if
                else
                    write(*,*) 'procsdf: cannot handle rank',rank
                end if  
            else
              if (trace) write(*,*) 'procsdf: not in index vector',step
            end if
 	   else 
             if (trace2) 
     .           write(*,*) "procsdf: end of file ",
     .                       name
             goto 800
         end if
      end do
		
 800  continue
      end



